<?php
ob_start();
?>
<style type="text/css">
    .header p {
        margin: 0;
        color: #043bff;
        padding: 5px;
    }

    .footer p {
        margin: 0;
        font-size: 10px;
        color: #0f9934;
    }

    .footer hr {
        color: #099909;
    }

    h1 {
        text-transform: uppercase;
        font-size: 18px;
        text-align: center;
        color: #444;
        margin: 40px;
    }

    table {
        width: 100%;
    }

    table thead th {
        width: 5%;
        background: #000;
        color: #FFF;
        padding: 5px;
        text-align: center;
    }

    table thead th.large {
        width: 40%;
        text-align: left;
    }

    table tbody tr td {
        padding: 8px 5px;
        border: 1px solid #999;
        text-align: center;
    }

    img {
        width: 150px;
        height: 100px;

    }

    .fiche {
        border: 1px solid black;
        margin-bottom: 10px;
        padding: -35px;
    }

    .content {
        margin-top: 30px;
        margin-bottom: -50px;
    }

    .feuille {
        background-color: #ca9211;
        width: 100%;
        padding: -35px;
    }

    p {
        padding-bottom: -5px;
    }

    .signature {
        text-align: center;
        padding: 110px 290px 90px;
    }

    .recu1 {
        height: 330px;
        width: 100%;
        border: 1px solid black;
        border-radius: 10px;
        margin-left: 10px;
        margin-top: 20px;
    }

    .logo {
        height: 100px;
        width: 100px;
        margin-top: 10px;
        margin-left: 10px;
    }

    .titre_recu {
        font-size: 20px;
        margin-left: 170px;
        margin-top: -65px;
        font-weight: bold;
        /* font-style: normal; */
    }

    .sous_titre {
        font-size: 20px;
        margin-left: 250px;
        margin-top: -80px;
        font-weight: bold;
    }

    .info {
        margin-top: -70px;
        text-align: center;
    }

    .montant {
        border-left: 1px solid black;
        height: 50px;
        width: 190px;
        margin-left: 20px;
        margin-top: -40px;
        text-align: left;
    }

    .trait {
        border-bottom: 1px dashed black;
        margin-top: 4px;
        width: 550px;
        margin-left: 70px;
    }

    .client {
        margin-top: 120px;
        margin-left: 50px;
        margin-top: 100px;
    }

    .gestionnaire {
        margin-top: 120px;
        margin-left: 480px;
        margin-top: 100px;
    }

    .client1 {
        margin-top: 120px;
        margin-left: 50px;
        margin-top: 350px;
    }

    .gestionnaire1 {
        margin-top: 120px;
        margin-left: 480px;
        margin-top: 350px;
    }

    /* strong{
            text-decoration:underline;
        } */
</style>
<page footer="date;pagination" backtop="120px" backbottom="100px">
    <page_header>
        <div style="width: 90%; margin-left: 20px;" class="header">
            <?php
            require_once("../../_config/config.php");
            require_once("../../_config/db.php");
            require_once("../../_classes/Historiques.php");
            require_once("../../_classes/Clients.php");
            require_once("../../_classes/Inscriptions.php");
            $n = time() . '_' . uniqid();
            ?>
            <div class="recu1">
                <div id="image">
                    <img class="logo" src="../../assets/img/brand/favicon.jpg" alt="">
                    <span class="titre_recu">TINKI GYM</span><br>
                    <span class="sous_titre">Aérobic-Yoga-Zumba</span><br>
                    <span class="info">
                        <span style="margin-left: 160px;margin-top:-2px;">Tel:+224 629 47 51 51 Email:info@asf-fitness.com</span>
                        <div class="montant">

                            <?php
                            if (isset($_GET['id']) and !empty($_GET['id']) and $_GET['id'] > 0) :
                                extract($_GET);
                                $num = \_classes\Clients::numRecu();
                                $fmt = new NumberFormatter('fr', NumberFormatter::SPELLOUT);
                                setlocale(LC_TIME, 'fr');
                                $annee = date_format(date_create($print->dates), 'Y');
                                $mois = date_format(date_create($print->dates), 'm');
                                $jour = date_format(date_create($print->dates), 'd');
                                $getRecuInscription = \_classes\Inscriptions::getInscriptionById($id);

                                foreach ($getRecuInscription as $get) : ?>
                                    <span>&nbsp;&nbsp; Recu N°: <?= "FN0" . $num; ?></span><br>
                                    <span>&nbsp;&nbsp;&nbsp;Montant : <?= number_format($get->montant, '0', '.', ',') . ' GNF'; ?></span><br>
                                    <span>&nbsp;&nbsp;&nbsp;Date : Le <?= $jour . "/" . $mois . "/" . $annee ?> </span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <span style="margin-top: 6px"><span style="font-weight:bold;">Site internet</span> :www.asf-fitness.com </span><br>
                        <span style="margin-top: 10px">Ratoma/Plaza diamant</span>
                    </span>
                </div>
                <br>
                <div class="trait"></div>
                <?php
                if (isset($_GET['id']) and !empty($_GET['id']) and $_GET['id'] > 0) :
                    extract($_GET);
                    $fmt = new NumberFormatter('fr', NumberFormatter::SPELLOUT);
                    setlocale(LC_TIME, 'fr');
                    $getRecuInscription = \_classes\Inscriptions::getInscriptionById($id);
                    foreach ($getRecuInscription as $get) : ?>
                        <span style="margin-left:15px;font-size:16px;"> <strong>Récu de </strong> </span> : <span style="font-size: 15px"><?= ucfirst($get->nomClients . " " . $get->prenomClients) ?></span>
                        <span style="margin-left:15px;font-size:16px;"><strong>Téléphone</strong></span> : <span style="font-size: 15px"><?= ucfirst($get->telephoneClients) ?></span>
                        <?php if (isset($get->matriculeClients) and !empty($get->matriculeClients)) : ?>
                            <!-- <span style="margin-left:15px;font-size:16px;"> <strong>Email:</strong></span> <span style="font-size: 15px"><?php echo strtolower($get->emailClients); ?></span> -->
                        <?php endif ?>
                        <div style="margin-left:15px;" class="somme">
                            <strong style="font-size: 16px">La somme de : </strong><?= " " . $fmt->format($get->fraisClients) . " Francs guinéens" ?>
                        </div><br>
                        <!-- <span style="margin-left:15px;font-size:16px;">Sport inscrit : </span> -->
                        <!-- <span style="font-size: 15px">
                            <?php
                            $sport = $get->sportClients;
                            if (isset($sport) and !empty($sport)) {
                                if ($sport == "Misculation") {
                                    echo "Musculation + Aérobic + Yoga";
                                }
                                if ($sport == "Judo") {
                                    echo "Judo";
                                }
                                if ($sport == "King Boxe") {
                                    echo "King Boxe";
                                }
                                if ($sport == "MMA") {
                                    echo "MMA";
                                }
                            }
                            ?>
                        </span> -->
                        <span style="margin-left:220px;font-size:16px;">
                            Durée :
                            <?php $dure = $get->dure;
                            if ($dure == "s1") {
                                echo "1 Jour";
                            }
                            if ($dure == "s2") {
                                echo "2 Jours";
                            }
                            if ($dure == "s3") {
                                echo "3 Jours";
                            }
                            if ($dure == "s4") {
                                echo "4 Jours";
                            }
                            if ($dure == "s5") {
                                echo "5 Jours";
                            }
                            if ($dure == "s6") {
                                echo "6 Jours";
                            }
                            if ($dure == "s7") {
                                echo "7 Jours";
                            }
                            if ($dure == "s14") {
                                echo "14 Jours";
                            }
                            if ($dure == "s21") {
                                echo "21 Jours";
                            }
                            if ($dure == "1") {
                                echo "1 Mois";
                            }
                            if ($dure == "2") {
                                echo "2 Mois";
                            }
                            if ($dure == "3") {
                                echo "3 Mois";
                            }
                            if ($dure == "4") {
                                echo "4 Mois";
                            }
                            if ($dure == "5") {
                                echo "5 Mois";
                            }
                            if ($dure == "5") {
                                echo "6 Mois";
                            }
                            if ($dure == "7") {
                                echo "7 Mois";
                            }
                            if ($dure == "8") {
                                echo "8 Mois";
                            }
                            if ($dure == "9") {
                                echo "9 Mois";
                            }
                            if ($dure == "s21") {
                                echo "10 Mois";
                            }
                            if ($dure == "11") {
                                echo "11 Mois";
                            }
                            if ($dure == "12") {
                                echo "12 Mois";
                            }
                            ?>
                        </span>

                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <br>
            <div class="recu1">
                <div id="image">
                    <img class="logo" src="../../assets/img/brand/favicon.jpg" alt="">
                    <span class="titre_recu">ASF FITNESS</span><br>
                    <span class="sous_titre">Aérobic-Yoga-Zumba</span><br>
                    <span class="info">
                        <span style="margin-left: 160px;margin-top:-2px;">Tel:+224 629 47 51 51 Email:info@asf-fitness.com</span>
                        <div class="montant">

                            <?php
                            if (isset($_GET['id']) and !empty($_GET['id']) and $_GET['id'] > 0) :
                                extract($_GET);
                                $num = \_classes\Clients::numRecu();
                                $fmt = new NumberFormatter('fr', NumberFormatter::SPELLOUT);
                                setlocale(LC_TIME, 'fr');
                                $annee = date_format(date_create($print->dates), 'Y');
                                $mois = date_format(date_create($print->dates), 'm');
                                $jour = date_format(date_create($print->dates), 'd');
                                $getRecuInscription = \_classes\Inscriptions::getInscriptionById($id);

                                foreach ($getRecuInscription as $get) : ?>
                                    <span>&nbsp;&nbsp;Recu N°: <?= "FN0" . $num; ?></span><br>
                                    <span>&nbsp;&nbsp;&nbsp;Montant : <?= number_format($get->montant, '0', '.', ',') . ' GNF'; ?></span><br>
                                    <span>&nbsp;&nbsp;&nbsp;Date : Le <?= $jour . "/" . $mois . "/" . $annee ?> </span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <span style="margin-top: 6px"><span style="font-weight:bold;">Site internet</span> :www.asf-fitness.com </span><br>
                        <span style="margin-top: 10px">Ratoma/Plaza diamant</span>

                    </span>
                </div>
                <br>
                <div class="trait"></div>
                <?php
                if (isset($_GET['id']) and !empty($_GET['id']) and $_GET['id'] > 0) :
                    extract($_GET);
                    $fmt = new NumberFormatter('fr', NumberFormatter::SPELLOUT);
                    setlocale(LC_TIME, 'fr');
                    $getRecuInscription = \_classes\Inscriptions::getInscriptionById($id);
                    foreach ($getRecuInscription as $get) : ?>
                        <span style="margin-left:15px;font-size:16px;"> <strong>Récu de </strong> </span> : <span style="font-size: 15px"><?= ucfirst($get->nomClients . " " . $get->prenomClients) ?></span>
                        <span style="margin-left:15px;font-size:16px;"><strong>Téléphone</strong></span> : <span style="font-size: 15px"><?= ucfirst($get->telephoneClients) ?></span>

                        <?php if (isset($get->matriculeClients) and !empty($get->matriculeClients)) : ?>
                            <!-- <span style="margin-left:15px;font-size:16px;"> <strong>Email:</strong></span> <span style="font-size: 15px"><?php echo strtolower($get->emailClients); ?></span> -->
                        <?php endif ?>

                        <div style="margin-left:15px;" class="somme">
                            <strong style="font-size: 16px">La somme de : </strong><?= " " . $fmt->format($get->fraisClients) . " Francs guinéens" ?>
                        </div><br>
                        <!-- <span style="margin-left:15px;font-size:16px;">Sport inscrit : </span> -->
                        <!-- <span style="font-size: 15px">
                            <?php
                            $sport = $get->sportClients;
                            if (isset($sport) and !empty($sport)) {
                                if ($sport == "Misculation") {
                                    echo "Musculation + Aérobic + Yoga";
                                }
                                if ($sport == "Judo") {
                                    echo "Judo";
                                }
                                if ($sport == "King Boxe") {
                                    echo "King Boxe";
                                }
                                if ($sport == "MMA") {
                                    echo "MMA";
                                }
                            }
                            ?>
                        </span> -->
                        <span style="margin-left:220px;font-size:16px;">
                            Durée :
                            <?php $dure = $get->dure;
                            if ($dure == "s1") {
                                echo "1 Jour";
                            }
                            if ($dure == "s2") {
                                echo "2 Jours";
                            }
                            if ($dure == "s3") {
                                echo "3 Jours";
                            }
                            if ($dure == "s4") {
                                echo "4 Jours";
                            }
                            if ($dure == "s5") {
                                echo "5 Jours";
                            }
                            if ($dure == "s6") {
                                echo "6 Jours";
                            }
                            if ($dure == "s7") {
                                echo "7 Jours";
                            }
                            if ($dure == "s14") {
                                echo "14 Jours";
                            }
                            if ($dure == "s21") {
                                echo "21 Jours";
                            }
                            if ($dure == "1") {
                                echo "1 Mois";
                            }
                            if ($dure == "2") {
                                echo "2 Mois";
                            }
                            if ($dure == "3") {
                                echo "3 Mois";
                            }
                            if ($dure == "4") {
                                echo "4 Mois";
                            }
                            if ($dure == "5") {
                                echo "5 Mois";
                            }
                            if ($dure == "5") {
                                echo "6 Mois";
                            }
                            if ($dure == "7") {
                                echo "7 Mois";
                            }
                            if ($dure == "8") {
                                echo "8 Mois";
                            }
                            if ($dure == "9") {
                                echo "9 Mois";
                            }
                            if ($dure == "s21") {
                                echo "10 Mois";
                            }
                            if ($dure == "11") {
                                echo "11 Mois";
                            }
                            if ($dure == "12") {
                                echo "12 Mois";
                            }
                            ?>
                        </span>

                    <?php endforeach; ?>
                <?php endif; ?>
            </div>


        </div>
    </page_header>

    <page_footer>
        <div class="footer">
            <p style="text-align: center">Formations - Assistances - Conseils - Sports - Récrutements - Interims <br> Tel : (+224) 656 21 26 26 /Ratoma/Plaza diamant </p>
        </div>
    </page_footer>
    <div style="width: 90%; margin-left: 20px;" class="content">


    </div>
    <div class="">
        <span class="client">Le Client</span><span class="gestionnaire">Le gestionnaire</span>
    </div>
    <div class="">
        <span class="client1">Le Client</span><span class="gestionnaire1">Le gestionnaire</span>
    </div>
</page>
<?php
$content = ob_get_clean();
require_once(dirname(__FILE__) . '/html2pdf/html2pdf.class.php');
try {
    $pdf = new HTML2PDF('P', 'A4', 'fr');
    $pdf->pdf->SetDisplayMode('fullpage');
    $pdf->pdf->SetTitle('FORMULAIRE_INSCRIPTION');
    $pdf->pdf->SetAuthor('FITNESSONE');
    $pdf->pdf->SetProtection(array('print'));
    $pdf->writeHTML($content);
    ob_end_clean();
    $pdf->Output('FORMULAIRE_INSCRIPTION_' . $n . '.pdf');
} catch (HTML2PDF_exception $e) {
    echo $e->getMessage();
    exit;
}
