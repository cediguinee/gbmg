<?php
use _classes\Ventes;
 require_once ("../../_config/config.php");
 require_once ("../../_config/db.php");
 require_once ("../../_classes/Ventes.php");
 require_once ("../../_classes/Clients.php");
 require_once ("../../_functions/fonctions.php");
 $n = time().'_'.uniqid();
     if(isset($_GET) AND !empty($_GET)){
         extract($_GET);
         $prints=Ventes::getfacture($id);
         $infos=Ventes::getinfoclient($id);
        //  debug($infos);
        //  die();
         $fmt = new NumberFormatter('fr', NumberFormatter::SPELLOUT);
         setlocale(LC_TIME, 'fr');
         date_format(date_create($print->dates), 'Y-m-d');
         $annee = strftime('%Y');
         $mois =ucfirst(strftime('%B'));
         $jour = strftime('%V');
     }
ob_start();
?>
<style type="text/css">
    .header {
        display: inline-block;
        height: 100px;
        /* background: #999 url(logo.png) 5px 5px no-repeat; */
        padding: 10px 10px 10px 10px;
    }

    .logo {
        width: 120px;
        height: 85px;
        /* background: url(logo1.png) 5px 5px no-repeat; */
        padding-bottom: -2px;
        padding-right: -20px;
    }

    .liste ul li {
        list-style: none;
        padding: 0;
        margin: 0;
    }

    .header p {
        margin: 0;
        color: #FFF;
    }


    .phead p {
        margin: 0;
        color: black;
    }

    .footer p {
        margin: 0;
        font-size: 10px;
        color: #999;
    }

    .footer hr {
        color: #999;
    }

    h4 {
        text-align: right;
    }

    h1 {
        text-transform: uppercase;
        font-size: 18px;
        text-align: center;
        color: #444;
        margin: 40px;
    }

    .client {
        margin-left: 400px;
        padding: 10px;
        border: 1px dotted #999;
    }

    .client p {
        margin: 0;
    }

    table {
        width: 100%;
    }

    table thead th {
        width: 11%;
        background: #fb8b4c;
        color: #FFF;
        padding: 5px;
        text-align: center;
    }

    /* ################################# */
    table thead th.designation {
        width: 240px;
        text-align: left;
        /* max-width: 45%; */
    }

    table thead th.quantite {
        width: 85px;
        text-align: left;
    }

    table thead th.prixunitaire {
        width: 120px;
        text-align: left;
    }

    table thead th.prixtotal {
        width: 140px;
        text-align: left;
    }

    /* ############################### */
    table tbody tr td {
        padding: 8px 5px;
        border: 1px solid #999;
        text-align: center;
    }

    table tbody tr td.large {
        text-align: left;
    }

    table tr.total {
        background: #000;
        color: #FFF;
    }

    ul {
        padding: 0;
    }

    table thead td.liste {
        width: 15px;
        text-align: left;

    }

    .infovehicule {
        border-right: 4px solid white;
        text-align: center;
        background-color: #fb8b4c;
        color: white;
    }

    .trait {
        border-top: 1px solid, black;
    }

    a {
        color: black;
        text-decoration: none;
    }

    .ha {
        margin-top: -100px;
    }
.intitule{
    border-top: 1px solid white;
    border-left: 1px solid white;
    border-right: 1px solid white;
    border-bottom: 1px solid black;
    height: 10px;
    width: 100px;
    margin-left: 10px;
    font-size: 20px;
    }
.nom{
    border-top: 1px solid white;
    border-left: 1px solid white;
    border-right: 1px solid white;
    border-bottom: 1px dashed black;
    font-size: 17px;
    text
}
</style>
<page footer="date;pagination" backtop="120px" backbottom="200px">
    <div class="ha">
       
        <table border="" style="border-collapse:collapse">
        
            <tr>
                <td class="logo">
                    <img class="logo" src="../../assets/img/brand/favicon.png" alt=""> 
                </td>
                <td class="phead">
                    
                </td>
                <td style="width: 620px;margin-left:150px;" class="liste">
                    <div style="margin-left:-100px;text-align:center">
                    <h3 style="color:#fb8b4c;margin-left:200px;border:1px solid black;font-size:30px;">ASF FITNESS</h3>
                    <span class="sous_titre">Aerobic-Yoga-Zumba</span><br>
                    <span>Tel: +224 656 21 26 26</span><br> <span> Email:info@asf-fitness.com</span> <br>
                    <span> <?php  ?>  </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="trait"></td>
            </tr>
            <tr>
                <td colspan="4" style="border:1px solid white;text-align:center"><strong>FACTURE DEFINITIVE</strong></td>
            </tr>
            <tr>
                <td style="text-align: left; border:1px solid white" colspan="2">
                <?php foreach($infos as $info) : ?> 
                    <strong><?='REF:'.$info->reference?></strong>
                <?php endforeach; ?> 
                </td><br><br>
                <td style="text-align: right; border:1px solid white" colspan="1">

                <?php foreach ($infos as $info) : ?>
                    <?php
                    date_format(date_create($info->dates), 'Y-m-d');
                    $annee = date_format(date_create($info->dates), 'Y');
                    $mois = date_format(date_create($info->dates), 'm');
                    $jour = date_format(date_create($info->dates), 'd');

                    ?>
                <?php endforeach;?>
               
                <strong>Conakry le <?= $jour . "/" . strtoupper($mois . "/" . $annee); ?></strong>
                </td><br>
            </tr><br>
            <tr>
                <td class="infovehicule" style="text-align: center" colspan="4">INFORMATION DU CLIENT</td><br>
            </tr>
            <tr>
                <td class="intitule">Nom: </td>
                <td class=""></td>
                <?php foreach($infos as $info) :?>
                    <td class="nom"><?=$info->nom ?> </td> 
                <?php endforeach; ?> 
                
            </tr>
            <tr>
                <td>.</td>
            </tr>
            <tr>
                <td class="intitule">Prénom: </td>
                <td class=""></td>
                <?php foreach($infos as $info) :?>
                    <td class="nom"><?=$info->prenom ?> </td> 
                <?php endforeach; ?> 
            </tr>
            <tr>
                <td>.</td>
            </tr>
            <tr>
                <td class="intitule">Téléphone: </td>
                <td class=""></td>
                <?php foreach($infos as $info) :?>
                    <td class="nom"><?=$info->telephone_client ?> </td> 
                <?php endforeach; ?> 
            </tr>
            <tr>
                <td>.</td>
            </tr>
            <tr>
                <td class="intitule">Email: </td>
                <td class=""></td>
                <?php foreach($infos as $info) :?>
                    <td class="nom"><?=$info->email?> </td> 
                <?php endforeach; ?> 
            </tr>
            
        </table>
    </div>
    <page_footer>
        <div class="footer">
            <hr>
            <span>Rép. De Guinée/Conakry/Commune de Ratoma/Lambagny/Centre Commercial</span><br>
            <!-- <span>Rép. De Guinée</span> -->
            <div style="text-align: center"></div>
            <div class="cordonnees"><span style="font-weight:bold"></span></div>
        </div>
    </page_footer>
    <br><br>
    <table>
        <thead>
            <tr>
                <th style="text-align:center;width:300px" class="designation">Désignation</th>
                <th style="text-align:center" class="quantite">Quantité</th>
                <th style="text-align:center" class="prixunitaire">Prix Unitaire</th>
                <th style="text-align:center" class="prixtotal">Prix Total</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($prints as $print) : ?>
                <tr>
                    <td style="text-align:left" class="designation">
                        <?php
                        $chaine = $print->designation;
                        $result = wordwrap($chaine, 58, "<br>\n");
                        echo $result = substr($result, 0, 500);
                        ?>
                    </td>
                    <td><?= $print->quantite ?></td>
                    <td><?= number_format($print->prixunitaire, '0', '.', ',') ?></td>
                    <td style="text-align:right"><?= number_format((int) ($print->prixunitaire) * (int) ($print->quantite), '0', '.', ','); ?></td>
                </tr>
            <?php endforeach; ?>
            <tr style="background-color:#cbcbcb;">
                <td colspan="3" style="text-align: left">Total</td>
                <td style="text-align:right;font-weight:bold">
                    <?php $total = 0; ?>
                    <?php foreach ($infos as $info) : ?>
                        <?php $total = (int) $info->montanttotal; ?>
                    <?php endforeach; ?>
                    <?= number_format($total, '0', '.', ',') . ' GNF'; ?>
                </td>
            </tr>
        
            <tr>
                <td style="text-align: left;border:1px solid white;"></td>
            </tr>
            <tr>
                <td style="text-align:left;border:1px dashed white;font-size:16px;" colspan="4" class="designation">
                    <?="Arrêtée la présente facture à la somme de : "?>
                    <span style="font-weight:bold"><?= $fmt->format($total)?></span> <?=" Francs guinéens";?>
                    
                
                </td>
            </tr>
        
            <tr>
                <td style="text-align: left;border:1px solid white;"></td>
            </tr>
            <tr>
                <td style="text-align: left;border:1px solid white;"></td>
            </tr>
            <tr>
            <td colspan="2" style="text-align: left;border:1px solid white;font-size:16px;">
                &nbsp;
                Le client
            </td>
            <td colspan="2" style="text-align:center;margin-left:20px;border:1px solid white;font-size:16px;">
                La Comptabilité
            </td>
            </tr>
        </tbody>
    </table>
</page>
<?php
$content = ob_get_clean();
require_once(dirname(__FILE__) . '/html2pdf/html2pdf.class.php');
try {
    $uniq = uniqid();
    $pdf = new HTML2PDF('P', 'A4', 'fr');
    $pdf->pdf->SetDisplayMode('fullpage');
    $pdf->pdf->SetTitle('Ma facture ...');
    $pdf->pdf->SetAuthor('athakim');
    $pdf->pdf->SetProtection(array('print'));
    $pdf->writeHTML($content);
    ob_get_clean();
    $pdf->Output("facture{$uniq}.pdf");
    // header("location:{LINK}factures");
} catch (HTML2PDF_exception $e) {
    echo $e->getMessage();
    exit;
}
