
<?php
$succes = [];
$erreurs = [];
$avertissement = [];
require_once ("includes/head.php");?>
<body>
<?php require_once ("includes/header.php");?>
<?php require_once ("om.php");?>
<div class="container">
    <br>
		<?php if(isset($avertissement) AND !empty($avertissement)):?>
			<?php foreach($avertissement as $get):?>
				<p class="alert alert-warning"><?=$get?></p>
			<?php endforeach;?>
		<?php endif;?>
		<?php if(isset($succes) AND !empty($succes)):?>
			<?php foreach($succes as $get):?>
				<p class="alert alert-success"><?=$get?></p>
			<?php endforeach;?>
		<?php endif;?>
		<?php if(isset($erreurs) AND !empty($erreurs)):?>
			<?php foreach($erreurs as $get):?>
				<p class="alert alert-danger"><?=$get?></p>
			<?php endforeach;?>
		<?php endif;?>
    <form action="" method="post">	
        <fieldset>
            <h1>Message</h1>
            <hr>
            <div class="form-group">
                <label for="">Numéro </label>
                <input type="text" name="destinataire" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Numéro Récepteur</label>
                <input type="number" name="prix" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Entête Message</label>
                <input type="number" name="quantite" class="form-control">
            </div>
            <div class="form-group">
                <label for="exampleTextarea">Message</label>
                <textarea class="form-control" name="message" id="exampleTextarea" rows="3"></textarea>
            </div>
        </fieldset>
        <fieldset>
            <h1>Information compte orange dev</h1>
            <hr>
            <div class="form-group">
                <label for="">Client ID </label>
                <input type="text" name="idClient" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Client Secret</label>
                <input type="number" name="secretClient" class="form-control">
            </div>
            <button type="submit" name="Envoyer" value="Envoyer" class="btn btn-primary">Envoyer</button>
        </fieldset>
    </form>
</div>
<div class="pt-3">

</div>
<?php require_once ("includes/footer.php");?>
</body>
<?php require_once ("includes/foot.php");?>
</html>