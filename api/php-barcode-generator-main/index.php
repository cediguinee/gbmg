<?php
require 'vendor/autoload.php';
// This will output the barcode as HTML output to display in the browser
$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body style="margin-left: 300px;">
    <?php
    for ($i = 0; $i < 10; $i++) {
        echo "<pre>";
        echo $generator->getBarcode('0812', $generator::TYPE_CODE_39);
        echo "<pre>";
    }
    ?>
</body>

</html>