
    $(document).ready(function(){
        let produit = document.querySelector("#produit");
        let prixachat = document.querySelector("#prixachat");
        let prixvente = document.querySelector("#prixvente");
        const link=$('#link').val();
        produit.addEventListener("change",function (){
            let idProduit = produit.value;
            console.log(idProduit);
            $.ajax({
                type: "GET",
                url: link+"api/ajax/produits.php",
                data: { id: idProduit},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    prixachatdb=data[0].prixAchatProduits
                    prixventedb=data[0].prixDeVenteProduits
                    prixachat.value =prixachatdb;
                    prixvente.value = prixventedb;
                    console.log(prixachat);
                    console.log(prixvente);
                },
                errors: function (error) {
                    console.log(error);
                }
            });
        });
        let fournisseur = document.querySelector("#fournisseur");
        let tel = document.querySelector("#tel");
        let nom = document.querySelector("#nom");
        let adresse = document.querySelector("#adresse");
        fournisseur.addEventListener("change",function (){
            let idFournisseur = fournisseur.value;
            //console.log(idFournisseur);
            $.ajax({
                type: "GET",
                url: link+"api/ajax/fournisseur.php",
                data: { id: idFournisseur},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    nom.value = data[0].prenomFournisseurs+' '+data[0].nomFournisseurs;
                    tel.value = data[0].telephoneFournisseurs;
                    adresse.value = data[0].adresseFournisseurs;
                },
                errors: function (error) {
                    console.log(error);
                }
            });
        });
    })
   
