
$(document).ready(function(){
    let produit = document.querySelector("#produit");
    let prixachat = document.querySelector("#prixachat");
    let prixvente = document.querySelector("#prixvente");
    const link=$('#link').val();
    produit.addEventListener("change",function (){
        let idProduit = produit.value;
        console.log(idProduit);
        $.ajax({
            type: "GET",
            url: link+"api/ajax/produits.php",
            data: { id: idProduit},
            dataType: "json",
            success: function (data) {
                console.log(data);
                prixachatdb=data[0].prixAchatProduits
                prixventedb=data[0].prixDeVenteProduits
                prixachat.value =prixachatdb;
                prixvente.value = prixventedb;
                console.log(prixachat);
                console.log(prixvente);
            },
            errors: function (error) {
                console.log(error);
            }
        });
    });
    let client = document.querySelector("#client");
    let tel = document.querySelector("#tel");
    let nom = document.querySelector("#nom");
    let adresse = document.querySelector("#adresse");
    client.addEventListener("change",function (){
        let idClient = client.value;
        console.log(idClient);
        $.ajax({
            type: "GET",
            url: link+"api/ajax/clients.php",
            data: { id: idClient},
            dataType: "json",
            success: function (data) {
                console.log(data);
                nom.value = data[0].prenomClient+' '+data[0].nomClient;
                tel.value = data[0].telephoneClient;
                adresse.value = data[0].adresseClient;
            },
            errors: function (error) {
                console.log(error);
            }
        });
    });
})

