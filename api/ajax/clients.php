<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html;charset=UTF-8');
require_once('../../config/config.php');
require_once('../../functions/functions.php');
require_once('../../config/db.php');
require_once('../../models/Client.php');

use models\Client;

if (isset($_GET) and !empty($_GET)) {
    extract($_GET);
    if (isset($id) and !empty($id)) {
        $data = Client::getClientById($id);
        echo json_encode($data);
    }
}

