<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html;charset=UTF-8');
require_once ('../../config/config.php');
require_once ('../../functions/functions.php');
require_once ('../../config/db.php');
require_once ('../../models/Produits.php');
use models\Produits;
if(isset($_GET) AND !empty($_GET)){
    extract($_GET);
    if(isset($id) AND !empty($id)){
        // $data = Produits::showAllProduits();
        $data = Produits::showProduitsById($id);
        echo json_encode($data);
    }
}
