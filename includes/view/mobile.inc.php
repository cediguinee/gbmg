<div class="mobile-menu-area">
    <!--offcanvas menu area start-->
    <div class="off_canvars_overlay"></div>
    <div class="offcanvas_menu">
        <div class="offcanvas_menu_wrapper">
            <div class="canvas_close">
                <a href="javascript:void(0)"><i class="far fa-times-circle"></i></a>
            </div>
            <div class="mobile-logo text-center mb-30">
                <a href="home-marketing.html">
                    <img width="150px" height="40" src="<?=LINK?>assets/view/logogbmg.png" alt="logo">
                </a>
            </div>
            <div id="menu" class="text-left ">
                <ul class="offcanvas_main_menu">
                    <li class="menu-item-has-children active"><a class="nav-link" href="<?=LINK?>home">Acceuil</a>

                    </li>

                    <li class="menu-item-has-children"><a class="nav-link" href="<?=LINK?>apropos">Apropos</a>

                    </li>
                    <li class="menu-item-has-children"><a class="nav-link" href="<?=LINK?>formations">Formations et Services</a>

                    </li>
                    <li class="menu-item-has-children"><a class="nav-link" href="<?=LINK?>contact">Contact</a>

                    </li>
                    <li class="menu-item-has-children"><a class="nav-link" href="<?=LINK?>inscription">Inscription</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
