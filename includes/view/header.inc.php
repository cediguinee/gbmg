
<!-- header area Ends -->


<div class="header-area">
    <header>
        <!-- top header start-->
        <div class="top-header top-header-style-1">
            <div class="container">
                <div class="top-header-inner d-flex align-items-center justify-content-between flex-wrap">
                    <div class="header-location d-flex align-items-center flex-wrap">
                        <i class="flaticon-location-pin"></i>
                        <h5>T6 entre la BICIGUI et station Shell SOS lavage, route le Prince.</span></h5>
                    </div>
                    <div class="header-right-portion d-flex align-items-center justify-content-between flex-wrap">
                        <div class="header-email d-flex align-items-center justify-content-center pl-0">
                            <i class="flaticon-email pr-10"></i>
                            <h5>Email : <span><a href="mailto:info@gbmgroupsarlu.com">info@gbmgroupsarlu.com</a></span></h5>

                        </div>
                        <div class="social-icon header-socials float-end">
                            <ul>
                                <li><a href="https://www.facebook.com/𝙂𝙡𝙤𝙗𝙖𝙡-𝘽𝙪𝙨𝙞𝙣𝙚𝙨𝙨-𝙈𝙖𝙧𝙠𝙚𝙩𝙞𝙣𝙜-GBM-Group-SARLU-770030240030453/"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com/GbmSarlu?t=AAMlzc5HYpwoFbIxPxzJcQ&s=09"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/invites/contact/?i=10wms3g6s6t4&utm_content=mfk2uty"><i class="fab fa-instagram-square"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="<?=LINK.'connexion'?>"><i class="fa fa-location-arrow"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main menu start -->
        <div class="main-menu-area" id="myHeader">
            <div class="container">
                <div class="main-menu-inner pos-relative">
                    <div class="logo">
                        <a href="home-marketing.html"><img height="40px" width="173px" src="<?=LINK?>assets/view/logogbmg.png" alt="logo here" class="img-fluid"></a>
                    </div>
                    <div class="main-menu text-center">
                        <ul class="nav-links">
                            <li><a class="nav-link" href="<?=LINK?>home">Acceuil</a>
                                <ul class="drop-down">
                                </ul>
                            </li>
                            <li><a class="nav-link" href="#">Apropos</a>
                                <ul class="drop-down">
                                </ul>
                            </li>
                            <li><a class="nav-link" href="<?=LINK?>formations">Formations et Services</a>
                                <ul class="drop-down">
                                </ul>
                            </li>
                            <li><a class="nav-link" href="<?=LINK?>contact">Contact</a>
                                <ul class="drop-down">
                                </ul>
                            </li>
                            <li><a class="nav-link" href="<?=LINK?>inscription">Inscription</a>
                                <ul class="drop-down">
                                </ul>
                            </li>

                        </ul>
                    </div>
                    <div class="main-menu-section">
                        <span class="btn-search btn-search-style-1"><i class="fas fa-search"></i></span>
                        <a href="tel:+224623535956"><i class="flaticon-phone-call pr-10"></i>+224 623 53 59 56</a>
                    </div>
                    <div class="canvas_open">
                        <a href="javascript:void(0)">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </div>
                <!-- search popup start-->
                <div class="td-search-popup" id="td-search-popup">
                    <form action="" method="get" class="search-form">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Recherche...">
                        </div>
                        <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <!-- search popup end-->
                <div class="body-overlay" id="body-overlay"></div>
            </div>
        </div>
    </header>
</div>