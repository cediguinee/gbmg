<!-- footer section start -->
<div class="footer-section top-spacing">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="footer-child mb-5 mb-xl-0 pt-80 pt-sm-0">
                    <h4 class="footer-tittle text-white pos-relative">A Propos</h4>
                    <p class="text pb-40 pt-40 text-white">GBMG est à la T6 près de la BICIGUI, Conakry, Guinée.</p>
                    <div class="social-icon footer-socials">
                        <ul>
                            <li><a href="https://www.facebook.com/𝙂𝙡𝙤𝙗𝙖𝙡-𝘽𝙪𝙨𝙞𝙣𝙚𝙨𝙨-𝙈𝙖𝙧𝙠𝙚𝙩𝙞𝙣𝙜-GBM-Group-SARLU-770030240030453/"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="https://twitter.com/GbmSarlu?t=AAMlzc5HYpwoFbIxPxzJcQ&s=09"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/invites/contact/?i=10wms3g6s6t4&utm_content=mfk2uty"><i class="fab fa-instagram-square"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/gbmgroupsarlu"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="https://youtube.com/channel/UCrVIobv0Hw9I9ULOyl-eTPA"><i class="fab fa-youtube"></i></a></li>
                            <li><a href="http://tiktok.com/@gbmgroupsarlu"><i class="fab fa-tiktok"></i></a></li>
                            <li><a href="connexion"><i class="fa fa-location-arrow"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-sm-6 mb-5 mb-xl-0">
                <div class="footer-child">
                    <h4 class="footer-tittle text-white pos-relative">Menu</h4>
                    <ul class="footer-links pt-40">
                        <li><span></span><a href="home">Acceuil</a></li>
                        <li><span></span><a href="apropos">Apropos</a></li>
                        <li><span></span><a href="formations">Formations</a></li>
                        <li><span></span><a href="contact">Contact</a></li>
                        <li><span></span><a href="inscription">Inscription</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-md-6 col-sm-6">
                <div class="footer-child mt-5 mt-lg-0">
                    <h4 class="footer-tittle text-white pos-relative">Contact</h4>
                    <div class="footer-contact pb-20 pt-40">
                        <ul>
                            <li class="d-flex"><i class="flaticon-location-pin pr-10"></i>
                                <span class="d-block">T6 près de la BICIGUI, <br> Conakry, Guinée,</span>
                            </li>
                            <li><a href="tel:+1(123)456789"><i class="flaticon-phone-call"></i>+224 623 53 59 56</a></li>
                            <li><a href="mailto:info@example.com"><i class="flaticon-email"></i>info@gbmgroupsarlu.com</a></li>
                            <li ><i class="flaticon-clock"></i><span>Ouverture: 9H:00 - 20H:00 </span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-bottom -->
    </div>
</div>
<div class="footer-bottom pb-30">
    <div class="row d-flex justify-content-center">
        <div class="col-xl-5 col-md-8 text-center">
            <div class="coppyright-text">
                <p class="text-black-50">Copyright © 2021, <span><?=SITE_NAME?></span> | Conception <a
                            class="text-white" href="www.cediguinee.com"> <span><?=AUTHOR?></span></a>.</p>
                <!-- scroll to up button -->
                <a href="#" class="to-top"><i class="fas fa-arrow-up"></i></a>
            </div>
        </div>
    </div>
</div>
<!-- footer section Ends -->