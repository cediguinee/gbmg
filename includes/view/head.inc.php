<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- title -->
    <title><?=SITE_NAME .' | '.ucfirst($page)?></title>
    <!-- fav icon -->
    <link rel="icon" href="<?=LINK?>assets/view/images/logo/fav-icon.png">
    <meta name="keywords" content="consulting, digital consultancy, Multi-Purpose, html, shop, ecommerce, digital agency, finance, consult agency" />
    <meta name="description" content="Buconz - Multi-Purpose Consulting Business HTML5 Template">

    <!-- flaticon -->
    <link rel="stylesheet" href="<?=LINK?>assets/view/webfonts/flaticon/flaticon.css">
    <!-- font icon -->
    <link rel="stylesheet" href="<?=LINK?>assets/view/css/all.min.css">
    <link rel="stylesheet" href="<?=LINK?>assets/view/css/fontawesome.min.css">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="<?=LINK?>assets/view/vendors/css/bootstrap.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="<?=LINK?>assets/view/animate/css/aos.css">
    <!-- slick slider  -->
    <link rel="stylesheet" href="<?=LINK?>assets/view/slick-slider/css/slick.css">
    <!-- popup-video -->
    <link rel="stylesheet" href="<?=LINK?>assets/view/popup-video/css/popup.css">
    <!--  mobile menu css    -->
    <link rel="stylesheet" href="<?=LINK?>assets/view/css/menu.css">
    <!-- our own design  -->
    <link rel="stylesheet" href="<?=LINK?>assets/view/css/home-management.css">
    <link rel="stylesheet" href="<?=LINK?>assets/view/css/our-team.css">
    <link rel="stylesheet" href="<?=LINK?>assets/view/css/style.css">
    <link rel="stylesheet" href="<?=LINK?>assets/view/css/responsive.css">


</head>