<!-- js here-->
<script src="<?=LINK?>assets/view/js/jquery.min.js"></script>
<!-- bootstrap js -->
<script src="<?=LINK?>assets/view/vendors/js/bootstrap.bundle.js"></script>
<!-- slider js -->
<script src="<?=LINK?>assets/view/slick-slider/js/slick.min.js"></script>
<!-- counter up js -->
<script src="<?=LINK?>assets/view/counter-up/js/jquery.waypoints.min.js"></script>
<script src="<?=LINK?>assets/view/counter-up/js/jquery.counterup.min.js"></script>
<!-- popup-video js -->
<script src="<?=LINK?>assets/view/popup-video/js/magnific-popup.js"></script>
<!-- easy pie chart -->
<script src="<?=LINK?>assets/view/js/easypiechart.min.js"></script>
<!-- animate js -->
<script src="<?=LINK?>assets/view/animate/js/aos.js"></script>
<!-- progress bar js -->
<script src="<?=LINK?>assets/view/progress-bar/js/jquery.lineProgressbar.js"></script>
<!-- typeit js -->
<script src="<?=LINK?>assets/view/js/typeit.min.js"></script>
<!-- mobile-menu js -->
<script src="<?=LINK?>assets/view/js/mobile-menu.js"></script>
<!-- custom js -->
<script src="<?=LINK?>assets/view/js/main.js"></script>


<script>
    $('.carousel').carousel();
    //typeit
    new TypeIt(".text-type", {
        speed:200,
        loop:true,
        strings:['Management',],
        breakLines:false,
    }).go();
</script>