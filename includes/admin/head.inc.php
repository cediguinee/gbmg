<head>

        <!-- Basic -->
        <meta charset="UTF-8">

        <title><?=SITE_NAME .' | '.ucfirst($page)?></title>
        <meta name="keywords" content="<?=SITE_NAME .' | '.ucfirst($page)?>" />
        <meta name="description" content="<?=SITE_NAME .' | '.ucfirst($page)?>">
        <meta name="author" content="<?=SITE_NAME .' | '.ucfirst($page)?>">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/jquery-ui/jquery-ui.theme.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/select2/css/select2.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/select2-bootstrap-theme/select2-bootstrap.min.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/bootstrap-multiselect/css/bootstrap-multiselect.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/dropzone/basic.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/dropzone/dropzone.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/summernote/summernote-bs4.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/codemirror/lib/codemirror.css" />
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/codemirror/theme/monokai.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?=LINK?>assets/admin/css/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="<?=LINK?>assets/admin/css/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="<?=LINK?>assets/admin/css/custom.css">

    <!-- Head Libs -->
    <script src="<?=LINK?>assets/admin/vendor/modernizr/modernizr.js"></script>
    </head>