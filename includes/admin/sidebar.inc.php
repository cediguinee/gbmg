<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title text-white">
            Menu Principal
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">

                <ul class="nav nav-main ">
                    <li>
                        <a class="nav-link" href="<?=LINK.'tableau_de_bord'?>">
                            <i class="bx bx-home-alt" aria-hidden="true"></i>
                            <span>Tableau de Bord</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class="bx bx-collection" aria-hidden="true"></i>
                            <span>Gestion Modules/Formations</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouvelle_formation'?>">
                                    Nouvelle Formation
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'liste_des_formations'?>">
                                    Liste des formations
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouveau_module'?>">
                                    Nouveau Module
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'liste_des_modules'?>">
                                    Liste des Modules
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-book-alt" aria-hidden="true"></i>
                            <span>Prospects</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouveau_prospect'?>">
                                    Nouveau Prospect
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'liste_des_procpets'?>">
                                    Liste des prospects
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-money aria-hidden="true"></i>
                            <span>Dépenses</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'type_depense'?>">
                                    Type de Dépense
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouveau_depense'?>">
                                    Nouveau Dépense
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'liste_des_depenses'?>">
                                    Liste des Dépenses
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-user-circle aria-hidden="true"></i>
                            <span>Participants</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouveau_client'?>">
                                    Nouveau Participant
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'liste_des_clients'?>">
                                    Liste des Participants
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'recherche_participant'?>">
                                    Recherche des Participants
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'liste_des_partenaires'?>">
                                    Liste des Partenaires
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'nouveau_payement'?>">
                                    Nouveau Payement
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'liste_des_payements'?>">
                                    Liste des Payements
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'nouveau_reinscrit'?>">
                                    Réinscription Participant
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'solde_des_clients'?>">
                                    Solde des Participants
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-mail-send aria-hidden="true"></i>
                            <span>Notifications</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'notification_partenaire'?>">
                                    Notification Partenaire
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'notification_speciale'?>">
                                    Notification Spéciale
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'notification_prospects'?>">
                                    Notification Prospects
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'notification_client'?>">
                                    Notification Participants
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'notification_employes'?>">
                                    Notification des Employés
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bxl-wordpress aria-hidden="true"></i>
                            <span>Annonces</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouvelle_annonce'?>">
                                    Nouvelle Annonce
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'liste_des_annonces'?>">
                                    Liste des Annonces
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-user-check aria-hidden="true"></i>
                            <span>Employes</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouveau_employe'?>">
                                    Nouvelle Employé
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'liste_des_employes'?>">
                                    Liste des Employés
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouveau_payement_employe'?>">
                                    Nouveau Payement
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'liste_des_payements_employes'?>">
                                    Liste des Payements
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-check-circle aria-hidden="true"></i>
                            <span>Utilisateurs</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'liste_des_utilisateurs'?>">
                                    Liste des utilisateurs
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'autorisation'?>">
                                    Autorisations
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-package aria-hidden="true"></i>
                            <span>Produits</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouveau_produit'?>">
                                    Nouveau Produit
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'liste_des_produits'?>">
                                    Liste des produits
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'ƒ'?>">
                                    Stock
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-user aria-hidden="true"></i>
                            <span>Clients</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'new_client'?>">
                                    Nouveau Client
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'list_client'?>">
                                    Liste des Clients
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'payement_client'?>">
                                    Payement des Clients
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'solde_client'?>">
                                    Solde des Clients
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-user-check aria-hidden="true"></i>
                            <span>Fournisseurs</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'new_fournisseur'?>">
                                    Nouveau Fournisseur
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'list_fournisseur'?>">
                                    Liste des Fournisseurs
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'payement_fournisseur'?>">
                                    Payement des Fournisseurs
                                </a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="<?=LINK.'solde_fournisseur'?>">
                                    Solde des Fournisseur
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-plus aria-hidden="true"></i>
                            <span>Achats</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouveau_achat'?>">
                                    Nouveau Achat
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'liste_des_achats'?>">
                                    Liste des produits
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-minus aria-hidden="true"></i>
                            <span>Ventes</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouvelle_vente'?>">
                                    Nouvelle Vente
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'liste_des_ventes'?>">
                                    Liste des Ventes
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent ">
                        <a class="nav-link" href="<?=LINK.'stock'?>">
                            <i class="bx bx-store "></i>
                            <span>Stocks</span>
                        </a>
                    </li>
                    <li class="nav-parent ">
                        <a class="nav-link" href="#">
                            <i class="bx bx-cog aria-hidden="true"></i>
                            <span>Configurations</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="<?=LINK.'nouvelle_slider'?>">
                                    Nouveau Slider
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'liste_des_sliders'?>">
                                    Liste des Sliders
                                </a>
                            </li>
                            <li class="nav-active">
                                <a class="nav-link" href="<?=LINK.'partenaires'?>">
                                Partenaires
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>


        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>

    </div>

</aside>