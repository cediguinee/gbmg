<footer id="footer">
    <div class="container">
        <div class="column-one-fourth">
            <h5 class="line"><span>Tweets.</span></h5>
            <div id="tweets"></div>
        </div>
        <div class="column-one-fourth">
            <h5 class="line"><span>LES CATEGORIES</span></h5>
            <ul class="footnav">
                <?php if (isset($getAllCat) and !empty($getAllCat)) : ?>
                    <?php foreach ($getAllCat as $getca) : ?>
                        <li><a href="#"><i class="icon-right-open"></i><?= $getca->nomCat ?></a></li>
                        <li></li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
        <div class="column-one-fourth">
            <h5 class="line"><span>NOUVELLES IMAGES</span></h5>
            <div class="flickrfeed">
                <ul id="" class="thumbs">
                    <?php if (isset($getImg) and !empty($getImg)) : ?>
                        <?php foreach ($getImg as $getIm) : ?>
                            <a href="detailarticles/<?= $getIm->slog ?>"><img src="<?= LINK ?>assets/img/imageAnnonce/<?= $getIm->photoAnnonce ?>" style="width: 70px; height: 40px;" alt="MyPassion" class="" /></a>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>

                <div>

                </div>
            </div>
        </div>
        <div class="column-one-fourth">
            <h5 class="line"><span>A PROPOS</span></h5>
            <p>
                Le Musée National de Sandervalia est le musée national de la Guinée, situé dans la commune de Kaloum à Conakry la capitale du pays. Il comprend deux salles d'exposition : la salle d'exposition permanente et la salle d'exposition temporaire. L'exposition permanente présente trois parties : le cultuel ; les arts et les activités économiques.Le Musée National de Guinée est situé dans le Quartier Sandervalia dans la commune de Kaloum non loin du Boulevard Telly Diallo. La case construite en 1896 par Aimé Olivier de Sanderval se trouve à droite de l'entrée principale.
            </p>
        </div>
        <p class="copyright">Copyright Musée National de Guinée - Conception <a style="color: white;" href="https://www.cediguinee.com">CEDIG-SARL</a></p>
    </div>
</footer>