<header class="header">
    <div class="logo-container">
        <a href="<?=LINK.'tableau_de_bord'?>" class="logo">
            <img src="<?=LINK?>assets/admin/img/logogbmg.png" width="75" height="35" alt="Porto Admin" />
        </a>

        <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>

    </div>

    <div class="header-right">
    <input type="text" id="link" hidden value="<?=LINK?>">
        <span class="separator"></span>
        <div id="userbox" class="userbox">
            <?php
            $userConnected = \models\Personnels::getUsersByLogin($_SESSION['gbmg']['login']);
            if(isset($userConnected) AND !empty($userConnected)):?>
            <?php foreach($userConnected as $item):?>
            <a href="#" data-bs-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="<?=LINK."assets/photos/avatars/".$item->photoPersonnels?>" alt="<?=$item->prenomPersonnels.' '.$item->nomPersonnels?>" class="rounded-circle" data-lock-picture="<?=LINK."assets/photos/avatars/".$item->photoPersonnels?>" />
                </figure>
                <div class="profile-info" data-lock-name="J<?=$item->prenomPersonnels.' '.$item->nomPersonnels?>" data-lock-email="<?=$item->emailUsers?>">
                    <span class="name"><?=$item->prenomPersonnels.' '.$item->nomPersonnels?></span>
                    <span class="role"><?=$item->roleUsers?></span>
                </div>

                <i class="fa custom-caret"></i>
            </a>
            <?php endforeach;?>
            <?php endif;?>
            <div class="dropdown-menu">
                <ul class="list-unstyled mb-2">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="profil"><i class="bx bx-user-circle"></i> Mon profil</a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="<?=LINK.'home'?>"><i class="fa fa-home"></i> Aller sur le site</a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="<?=LINK.'deconnexion'?>"><i class="bx bx-power-off"></i> Déconnexion</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>