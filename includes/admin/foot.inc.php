<!-- Vendor -->
<script src="<?=LINK?>assets/admin/vendor/jquery/jquery.js"></script>
<script src="<?=LINK?>assets/admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="<?=LINK?>assets/admin/vendor/popper/umd/popper.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?=LINK?>assets/admin/vendor/common/common.js"></script>
<script src="<?=LINK?>assets/admin/vendor/nanoscroller/nanoscroller.js"></script>
<script src="<?=LINK?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="<?=LINK?>assets/admin/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Specific Page Vendor -->
<script src="<?=LINK?>assets/admin/vendor/select2/js/select2.js"></script>
<script src="<?=LINK?>assets/admin/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/datatables/media/js/dataTables.bootstrap5.min.js"></script>
<script src=<?=LINK?>assets/admin/"vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="<?=LINK?>assets/admin/vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?=LINK?>assets/admin/js/theme.js"></script>

<!-- Theme Custom -->
<script src="<?=LINK?>assets/admin/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?=LINK?>assets/admin/js/theme.init.js"></script>

<!-- Examples -->
<script src="<?=LINK?>assets/admin/js/examples/examples.datatables.default.js"></script>
<script src="<?=LINK?>assets/admin/js/examples/examples.datatables.row.with.details.js"></script>
<script src="<?=LINK?>assets/admin/js/examples/examples.datatables.tabletools.js"></script>

<!-- Examples -->
<script src="<?=LINK?>assets/admin/js/examples/examples.modals.js"></script>



