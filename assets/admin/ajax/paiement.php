<?php
use models\PayementFormations;
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html;charset=UTF-8');
require_once('../../../config/config.php');
require_once('../../../config/db.php');
require_once('../../../functions/functions.php');
require_once('../../../models/PayementFormations.php');
if (isset($_GET) and !empty($_GET)) {
    extract($_GET);
    $prixformation=PayementFormations::getprixformation($matricule,$idFormation,$idModule);
    $paiement=PayementFormations::getpaiement($matricule,$idFormation,$idModule);
    $data=$prixformation-$paiement;
    echo json_encode($data);
}
