$(document).ready(function () {
    const LINK = $('#link').val();
    $('#matricule').change(function () {
        matricule = $(this).val();
    })
    $('#idModule').change(function () {
        idModule = $(this).val();
    })
    $('#idFormation').change(function () {
        idFormation = $(this).val();
        $.ajax({
            url: LINK + "assets/admin/ajax/paiement.php",
            method: 'GET',
            data: {
              matricule: matricule,
              idModule: idModule,
              idFormation: idFormation
            },
            dataType: 'json',
            success: function (data) {
                $('#restapayer').val(data)
            },
            error: function (e) {
              console.log(e);
            }
          })
    })
    $('#montant').keyup(function(){
        montant=$(this).val();
        restapayer=$('#restapayer').val();
        $('#reste').val(restapayer-montant)
    })
})
