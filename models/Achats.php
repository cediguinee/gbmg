<?php

namespace models;

class Achats
{
    public static function addAchat(string $referenceAchats, string $idProduits, string $quantiteAchats, string $prixAchats, string $totalAchats, string $dateAchats, string $users, string $montantpaye, string $restepaye, string $idFournisseur, string $total):void{
        global $con;
        $req=$con->prepare("INSERT INTO achats (referenceAchats, idProduits, quantiteAchats, prixAchats, totalAchats, dateAchats, users, montantpaye, restepaye, idFournisseur, total) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($referenceAchats), secure($idProduits), secure($quantiteAchats), secure($prixAchats), secure($totalAchats), secure($dateAchats), secure($users), secure($montantpaye), secure($restepaye), secure($idFournisseur), secure($total)]);
        $req->closeCursor();
    }
    public static function updateAchat(string $referenceAchats, string $idProduits, string $quantiteAchats, string $prixAchats, string $totalAchats, string $dateAchats, string $users, string $montantpaye, string $restepaye, string $idFournisseur, string $total, string $id):void{
        global $con;
        $req=$con->prepare("UPDATE achats SET referenceAchats = ?, idProduits = ?, quantiteAchats = ?, prixAchats = ?, totalAchats = ?, dateAchats = ?, users = ?, montantpaye = ?, restepaye = ?, idFournisseur = ?, total = ? WHERE idAchats = ?;");
        $req->execute([secure($$referenceAchats), secure($idProduits), secure($quantiteAchats), secure($prixAchats), secure($totalAchats), secure($dateAchats), secure($users), secure($montantpaye), secure($restepaye), secure($idFournisseur), secure($total), secure($id)]);
        $req->closeCursor();
    }
    public static function deleteAchat(string $idAchats):void{
        global $con;
        $req=$con->prepare("DELETE FROM achats WHERE 	idAchats  = ?");
        $req->execute([secure($idAchats)]);
        $req->closeCursor();
    }
    public static function countProduitsAchat(string $idProduits):array{
        global $con;
        $req=$con->prepare("SELECT SUM(quantiteAchats) as quantite FROM achats WHERE idProduits =?;");
        $req->execute([secure($idProduits)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function deleteAchatFacture(string $referenceAchats):void{
        global $con;
        $req=$con->prepare("DELETE FROM achats WHERE 	referenceAchats  = ?");
        $req->execute([secure($referenceAchats)]);
        $req->closeCursor();
    }
    public static function getAchatById(string $idAchats):array {
        global $con;
        $req=$con->prepare("SELECT * FROM achats WHERE idAchats = ?");
        $req->execute([secure($idAchats)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getIdFournisseurById(string $idFournisseur):array {
        global $con;
        $req=$con->prepare("SELECT DISTINCT referenceAchats, montantpaye, restepaye, total FROM achats WHERE idfournisseur = ?");
        $req->execute([secure($idFournisseur)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getIclientsById(string $idClients):array {
        global $con;
        $req=$con->prepare("SELECT * FROM achats WHERE idFournisseurs = ?");
        $req->execute([secure($idClients)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    //ventes
    public static function AchatsReference(): array
    {
        global $con;
        $req = $con->prepare("SELECT DISTINCT referenceAchats FROM achats;");
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAchatsAll(): array
    {
        global $con;
        $req = $con->prepare("SELECT referenceAchats FROM achats GROUP BY referenceAchats;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function AchatsReferenceTotal(string $referenceAchats): array
    {
        global $con;
        $req = $con->prepare("SELECT total FROM achats WHERE referenceAchats = ? LIMIT 1;");
        $req->execute([secure($referenceAchats)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    //
    public static function getAchatByIdReferenceAchat(string $reference):array {
        global $con;
        $req=$con->prepare("SELECT * FROM achats INNER JOIN produits INNER JOIN fournisseurs WHERE produits.idProduits=achats.idProduits AND fournisseurs.idFournisseurs =achats.idFournisseur AND referenceAchats = ?");
        $req->execute([secure($reference)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAchatByIdFournisseur(string $idFournisseur):array {
        global $con;
        $req=$con->prepare("SELECT referenceAchats FROM achats WHERE idFournisseur = ? GROUP BY referenceAchats;");
        $req->execute([secure($idFournisseur)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAchatByIdFournisseurAndReference(string $reference):array {
        global $con;
        $req=$con->prepare("SELECT * FROM achats WHERE achats.referenceAchats = ? LIMIT 1");
        $req->execute([secure($reference)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAchatByIdFournisseurAndReferenceIdFournisseur(string $reference, string $idfournisseur):array {
        global $con;
        $req=$con->prepare("SELECT * FROM achats INNER JOIN fournisseurs WHERE achats.idFournisseur = fournisseurs.idFournisseurs AND achats.referenceAchats = ? AND achats.idFournisseur = ? LIMIT 1");
        $req->execute([secure($reference),secure($idfournisseur)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAchatAll():array {
        global $con;
        $req=$con->prepare("SELECT referenceAchats FROM achats GROUP BY referenceAchats;");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAchatAllByReference(string $reference):array {
        global $con;
        $req=$con->prepare("SELECT * FROM achats INNER JOIN produits INNER JOIN fournisseurs WHERE produits.idProduits=achats.idProduits AND achats.idFournisseur=fournisseurs.idFournisseurs AND achats.idProduits=produits.idProduits AND referenceAchats = ? ORDER BY produits.idProduits DESC LIMIT 1;");
        $req->execute([secure($reference)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAchat($users):array {
        global $con;
        $req=$con->prepare("SELECT * FROM achats INNER JOIN produits WHERE produits.idProduits=achats.idProduits AND users = ? ORDER BY produits.idProduits DESC;");
        $req->execute([secure($users)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}