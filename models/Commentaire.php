<?php

namespace models;

class Commentaire
{
    public static function addCommentaire(string $nom, string $email, string $commentaire, string $idAnnone, string $statut): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO commentaires SET nom = ?, email = ?, commentaire = ?, idAnnonce = ?, statut =?");
        $req->execute([secure($nom), secure($email), secure($commentaire), secure($idAnnone), secure($statut)]);
        $req->closeCursor();
    }
    public static function getAllCommentaire():array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM commentaires INNER JOIN annonces WHERE commentaires.idAnnonce=annonces.idAnnonce ORDER BY idcommentaire DESC");
        $req->execute();
        $resultat = [];
        while($data = $req->fetchObject()){
            array_push($resultat,$data);
        }
        $req->closeCursor();
        return $resultat;
    }
    public static function updatestatus(string $statut, string $idcommentaire): void
    {
        global $con;
        $req = $con->prepare("UPDATE commentaires SET  statut =? WHERE idcommentaire = ?");
        $req->execute([secure($statut), secure($idcommentaire)]);
        $req->closeCursor();
    }
    public static function deletecommentaire(string $idcommentaire): void
    {
        global $con;
        $req = $con->prepare("DELETE FROM commentaires WHERE idcommentaire = ?");
        $req->execute([secure($idcommentaire)]);
        $req->closeCursor();
    }
    public static function getIDByComment(string $idcommentaire): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM commentaires INNER JOIN annonces WHERE commentaires.idAnnonce=annonces.idAnnonce AND idcommentaire = ?");
        $req->execute([secure($idcommentaire)]);
        $resultat = [];
        while($data = $req->fetchObject()){
            array_push($resultat,$data);
        }
        $req->closeCursor();
        return $resultat;
    }
    public static function getAnnonneParComment(string $slog): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM commentaires INNER JOIN annonces WHERE commentaires.idAnnonce=annonces.idAnnonce AND annonces.slog= ? and statut='Active' ORDER BY idcommentaire DESC");
        $req->execute([secure($slog)]);
        $resultat = [];
        while($data = $req->fetchObject()){
            array_push($resultat,$data);
        }
        $req->closeCursor();
        return $resultat;
    }
    public static function getCommentApprouver():array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM commentaires WHERE  statut='Active'");
        $req->execute();
        $resultat = [];
        while($data = $req->fetchObject()){
            array_push($resultat,$data);
        }
        $req->closeCursor();
        return $resultat;
    }
    public static function getCommentDesaprouver():array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM commentaires WHERE  statut='Desactive'");
        $req->execute();
        $resultat = [];
        while($data = $req->fetchObject()){
            array_push($resultat,$data);
        }
        $req->closeCursor();
        return $resultat;
    }
}
