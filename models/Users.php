<?php


namespace models;


class Users
{
    public static function addUSers(string $identifientUsers, string $emailUsers, string $passwordUsers, string $roleUsers, string $etatUsers):void{
        global $con;
        $req=$con->prepare('INSERT INTO users (identifientUsers, emailUsers, passwordUsers, roleUsers, etatUsers) VALUES (?, ?, ?, ?, ?);');
        $req->execute([secure($identifientUsers), secure($emailUsers), secure($passwordUsers), secure($roleUsers), secure($etatUsers)]);
        $req->closeCursor();
    }
    public static function editUSers(string $emailUsers, string $passwordUsers, string $roleUsers, string $etatUsers, string $identifientUsers):void{
        global $con;
        $req=$con->prepare('UPDATE users SET emailUsers = ?, passwordUsers = ?, roleUsers = ?, etatUsers = ? WHERE identifientUsers = ?;');
        $req->execute([secure($identifientUsers), secure($emailUsers), secure($passwordUsers), secure($roleUsers), secure($etatUsers), secure($identifientUsers)]);
        $req->closeCursor();
    }
    public static function updateUSersStatut(string $etatUsers, string $identifientUsers):void{
        global $con;
        $req=$con->prepare('UPDATE users SET etatUsers = ? WHERE identifientUsers = ?;');
        $req->execute([secure($etatUsers), secure($identifientUsers)]);
        $req->closeCursor();
    }
    public static function verifyUsers(string $email, string $login):int{
        global $con;
        $req=$con->prepare('SELECT * FROM users WHERE  emailUsers = ? AND loginUsers = ?');
        $req->execute([secure($email),secure($login)]);
        $req->closeCursor();
        return $req->rowCount();
    }
    public static function updateUsers(string $password, string $login):void{
        global $con;
        $req=$con->prepare("UPDATE users SET passwordUsers = ? WHERE identifientUsers = ?;");
        $req->execute([secure($password),secure($login)]);
        $req->closeCursor();
    }
    public static function getUsers(string $login):array {
        global $con;
        $req=$con->prepare("SELECT * FROM users INNER JOIN personnels INNER JOIN professions INNER JOIN fonctions WHERE personnels.identifientPersonnels=users.identifientUsers AND identifientUsers = ? AND fonctions.idFonctions=personnels.idFonctions AND professions.idProfessions=personnels.idProfessions;");
        $req->execute([secure($login)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getUsersConnect(string $login):array {
        global $con;
        $req=$con->prepare("SELECT * FROM users INNER JOIN personnels WHERE personnels.identifientPersonnels=users.identifientUsers AND identifientUsers = ?");
        $req->execute([secure($login)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function updatePassword(string $password, string $login):void{
        global $con;
        $req=$con->prepare("UPDATE users SET passwordUsers = ? WHERE  identifientUsers = ?;");
        $req->execute([secure($password),secure($login)]);
        $req->closeCursor();
    }

}