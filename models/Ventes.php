<?php

namespace models;

class Ventes
{
    public static function addVentes(string $referenceventes, string $idProduits, string $quantiteventes, string $prixventes, string $totalventes, string $dateventes, string $users, string $montantpaye, string $restepaye, string $idFournisseur, string $total): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO ventes (referenceventes, idProduits, quantiteventes, prixventes, totalventes, dateventes, users, montantpaye, restepaye, idClients, total) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($referenceventes), secure($idProduits), secure($quantiteventes), secure($prixventes), secure($totalventes), secure($dateventes), secure($users), secure($montantpaye), secure($restepaye), secure($idFournisseur), secure($total)]);
        $req->closeCursor();
    }

    public static function updateVentes(string $referenceventes, string $idProduits, string $quantiteventes, string $prixventes, string $totalventes, string $dateventes, string $users, string $montantpaye, string $restepaye, string $idFournisseur, string $total, string $id): void
    {
        global $con;
        $req = $con->prepare("UPDATE ventes SET referenceventes = ?, idProduits = ?, quantiteventes = ?, prixventes = ?, totalventes = ?, dateventes = ?, users = ?, montantpaye = ?, restepaye = ?, idFournisseur = ?, total = ? WHERE idVentes = ?;");
        $req->execute([secure($$referenceventes), secure($idProduits), secure($quantiteventes), secure($prixventes), secure($totalventes), secure($dateventes), secure($users), secure($montantpaye), secure($restepaye), secure($idFournisseur), secure($total), secure($id)]);
        $req->closeCursor();
    }

    public static function deleteVentes(string $idventes): void
    {
        global $con;
        $req = $con->prepare("DELETE FROM ventes WHERE idVentes  = ?");
        $req->execute([secure($idventes)]);
        $req->closeCursor();
    }
    public static function getIclientsById(string $idClients):array {
        global $con;
        $req=$con->prepare("SELECT DISTINCT referenceVentes, montantpaye, restepaye, totalVentes, total FROM ventes WHERE idClients = ?");
        $req->execute([secure($idClients)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function countProduitsVentes(string $idProduits): array
    {
        global $con;
        $req = $con->prepare("SELECT SUM(quantiteventes) as quantite FROM ventes WHERE idProduits =?;");
        $req->execute([secure($idProduits)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function deleteVentesFacture(string $referenceventes): void
    {
        global $con;
        $req = $con->prepare("DELETE FROM ventes WHERE referenceventes  = ?");
        $req->execute([secure($referenceventes)]);
        $req->closeCursor();
    }
    //ventes
    public static function VentesReference(): array
    {
        global $con;
        $req = $con->prepare("SELECT DISTINCT referenceVentes FROM ventes;");
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function VentesReferenceTotal(string $referenceVentes): array
    {
        global $con;
        $req = $con->prepare("SELECT total FROM ventes WHERE referenceVentes = ? LIMIT 1;");
        $req->execute([secure($referenceVentes)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    //
    public static function getVentesById(string $idventes): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM ventes WHERE idVentes = ?");
        $req->execute([secure($idventes)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getVentesByIdReferenceVentes(string $reference): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM ventes INNER JOIN produits INNER JOIN client WHERE produits.idProduits=ventes.idProduits AND client.idClient =ventes.idClients AND referenceventes = ?");
        $req->execute([secure($reference)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getVentesByIdFournisseur(string $idClients): array
    {
        global $con;
        $req = $con->prepare("SELECT referenceventes FROM ventes WHERE idClients = ? GROUP BY referenceventes;");
        $req->execute([secure($idClients)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getVentesByIdFournisseurAndReference(string $reference): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM ventes WHERE ventes.referenceventes = ? LIMIT 1");
        $req->execute([secure($reference)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getVentesByIdFournisseurAndReferenceIdFournisseur(string $reference, string $idfournisseur): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM ventes INNER JOIN client WHERE ventes.idClients=client.idClient AND ventes.referenceVentes = ? AND ventes.idClients = ? LIMIT 1");
        $req->execute([secure($reference), secure($idfournisseur)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getVentesAll(): array
    {
        global $con;
        $req = $con->prepare("SELECT referenceventes FROM ventes GROUP BY referenceventes;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getVentesAllByReference(string $reference): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM ventes INNER JOIN produits INNER JOIN client WHERE produits.idProduits=ventes.idProduits AND ventes.idClients=client.idClient AND ventes.idProduits=produits.idProduits AND referenceventes = ? ORDER BY produits.idProduits DESC LIMIT 1;");
        $req->execute([secure($reference)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getAllVentes($users): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM ventes INNER JOIN produits WHERE produits.idProduits=ventes.idProduits AND users = ? ORDER BY produits.idProduits DESC;");
        $req->execute([secure($users)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
}