<?php

namespace models;

class Types
{
    public static function addTypes(string $nom):void{
        global $con;
        $req=$con->prepare("INSERT INTO types (libelleTypes) VALUES (?);");
        $req->execute([secure($nom)]);
        $req->closeCursor();
    }
    public static function updateTypes(string $libelleTypes, string $idTypes):void{
        global $con;
        $req=$con->prepare("UPDATE types SET libelleTypes = ? WHERE idTypes = ?;");
        $req->execute([secure($libelleTypes), secure($idTypes)]);
        $req->closeCursor();
    }
    public static function deleteTypes(string $idTypes):void{
        global $con;
        $req=$con->prepare("DELETE FROM types WHERE idTypes = ?");
        $req->execute([secure($idTypes)]);
        $req->closeCursor();
    }
    public static function getTypesById(string $idClient):array {
        global $con;
        $req=$con->prepare("SELECT * FROM types WHERE idTypes = ?");
        $req->execute([secure($idClient)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getAllTypes():array {
        global $con;
        $req=$con->prepare("SELECT * FROM types ORDER BY idTypes DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}