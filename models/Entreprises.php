<?php


namespace models;


class Entreprises
{
    public static function addEntreprise(string $nomEntreprisesConfigurations, string $abreviationConfigurations, string $addresseConfigurations, string $telephonePDVConfigurations, string $telephoneConfigurations, string $descriptionConfigurations, string $logoConfigurations):void{
        global $con;
        $req=$con->prepare("INSERT INTO configurations (nomEntreprisesConfigurations, abreviationConfigurations, addresseConfigurations, telephonePDVConfigurations, telephoneConfigurations, descriptionConfigurations, logoConfigurations) VALUES (?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($nomEntreprisesConfigurations), secure($abreviationConfigurations) , secure($addresseConfigurations), secure($telephonePDVConfigurations), secure($telephoneConfigurations), secure($descriptionConfigurations), secure($logoConfigurations)]);
        $req->closeCursor();
    }
    public static function editEntreprises(string $nomEntreprisesConfigurations, string $abreviationConfigurations, string $addresseConfigurations, string $telephonePDVConfigurations, string $telephoneConfigurations, string $descriptionConfigurations, string $logoConfigurations, string $idConfigurations):void {
        global $con;
        $req=$con->prepare("UPDATE configurations SET nomEntreprisesConfigurations = ?, abreviationConfigurations = ?, addresseConfigurations = ?, telephonePDVConfigurations = ?, telephoneConfigurations = ?, descriptionConfigurations = ?, logoConfigurations = ? WHERE $idConfigurations = ?;");
        $req->execute([secure($nomEntreprisesConfigurations), secure($abreviationConfigurations) , secure($addresseConfigurations), secure($telephonePDVConfigurations), secure($telephoneConfigurations), secure($descriptionConfigurations), secure($logoConfigurations), secure($idConfigurations)]);
        $req->closeCursor();
    }
    public static function getEntrepriseById(string $idEntreprise):array {
        global $con;
        $req=$con->prepare("SELECT * FROM configurations WHERE idConfigurations = ?;");
        $req->execute([secure($idEntreprise)]);
        $resultats = [];
        while ($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getEntreprise():array {
        global $con;
        $req=$con->prepare("SELECT * FROM entreprise");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}