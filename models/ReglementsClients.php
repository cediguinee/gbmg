<?php

namespace models;

class ReglementsClients
{
    public static function addReglement(string $montantReglements, string $users, string $idClient, string $reference):void{
        global $con;
        $req=$con->prepare("INSERT INTO reglementclients (montantReglements, users, idClient, reference) VALUES (?, ?, ?, ?);");
        $req->execute([secure($montantReglements),secure($users),secure($idClient), secure($reference)]);
        $req->closeCursor();
    }
    public static function getReglementById(string $idClients):array {
        global $con;
        $req=$con->prepare("SELECT * FROM reglementclients WHERE idClient = ?");
        $req->execute([secure($idClients)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getReglementSommeById(string $idClients):array {
        global $con;
        $req=$con->prepare("SELECT SUM(montantReglements) as montantReglements FROM reglementclients WHERE idClient = ?");
        $req->execute([secure($idClients)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllReglements(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM reglementclients INNER JOIN client WHERE client.idClient=reglementclients.idClient ORDER BY reglementclients.idReglements DESC;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function delReglementsClients(string $idReglements):void{
        global $con;
        $req=$con->prepare("DELETE FROM reglementclients WHERE idReglements = ?;");
        $req->execute([secure($idReglements)]);
    }
}