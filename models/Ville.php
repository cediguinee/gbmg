<?php


namespace models;


class Ville
{
    public static function addVille(string $nom):void{
        global $con;
        $req=$con->prepare("INSERT INTO ville (nomVille) VALUES (?);");
        $req->execute([secure($nom)]);
        $req->closeCursor();
    }
    public static function updateVille(string $nomVille, string $id):void{
        global $con;
        $req=$con->prepare("UPDATE ville SET nomVille = ? WHERE idVille = ?;");
        $req->execute([secure($nomVille), secure($id)]);
        $req->closeCursor();
    }
   
   
    public static function deleteVille(string $idClients):void{
        global $con;
        $req=$con->prepare("DELETE FROM ville WHERE idVille = ?");
        $req->execute([secure($idClients)]);
        $req->closeCursor();
    }
    public static function getVilleById(string $idClient):array {
        global $con;
        $req=$con->prepare("SELECT * FROM ville WHERE idVille = ?");
        $req->execute([secure($idClient)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function verifyByNomVille(string $nom):array {
        global $con;
        $req=$con->prepare("SELECT * FROM ville WHERE nomVille = ?");
        $req->execute([secure($nom)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllVille():array {
        global $con;
        $req=$con->prepare("SELECT * FROM ville ORDER BY idVille DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
 

}