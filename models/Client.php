<?php

namespace models;

class Client
{
    public static function addClient(string $prenomClient, string $nomClient, string $adresseClient, string $telephoneClient):void{
        global $con;
        $req=$con->prepare("INSERT INTO client (prenomClient, nomClient, adresseClient, telephoneClient) VALUES (?, ?, ?, ?);");
        $req->execute([secure($prenomClient), secure($nomClient), secure($adresseClient), secure($telephoneClient)]);
        $req->closeCursor();
    }
    public static function updateClient(string $prenomClient, string $nomClient, string $adresseClient, string $telephoneClient, string $idClient):void{
        global $con;
        $req=$con->prepare("UPDATE client SET prenomClient = ?, nomClient = ?, adresseClient = ?, telephoneClient = ? WHERE idClient = ?;");
        $req->execute([secure($prenomClient), secure($nomClient), secure($adresseClient), secure($telephoneClient), secure($idClient)]);
        $req->closeCursor();
    }
    public static function deleteClient(string $idClients):void{
        global $con;
        $req=$con->prepare("DELETE FROM client WHERE idClient = ?");
        $req->execute([secure($idClients)]);
        $req->closeCursor();
    }
    public static function getClientById(string $idClient):array {
        global $con;
        $req=$con->prepare("SELECT * FROM client WHERE idClient = ?");
        $req->execute([secure($idClient)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllClient():array {
        global $con;
        $req=$con->prepare("SELECT * FROM client ORDER BY idClient DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllClients():array {
        global $con;
        $req=$con->prepare("SELECT * FROM client ORDER BY idClient DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}