<?php


namespace models;


class Professions
{
    public static function getAllProfessions():array {
        global $con;
        $req=$con->prepare("SELECT * FROM professions");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}