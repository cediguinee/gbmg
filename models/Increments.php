<?php

namespace models;

class Increments
{
    public static function addIncrement(): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO incrementachat (increment) VALUES (NULL);");
        $req->execute();
        $req->closeCursor();
    }
    public static function getAllIncrement(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM incrementachat");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
}