<?php


namespace models;


class Personnels
{
    public static function addPersonnel(string $nomPersonnels, string $prenomPersonnels, string $sexePersonnels, string $identifientPersonnels, string $photoPersonnels, string $adressePersonnels, string $fonctionPersonnels, string $professionPersonnels, string $emailPersonnels, string $telephonePersonnels, string $perePersonnels, string $merePersonnels, string $personneContactPersonnels, string $dateNaissancePersonnels, string $lieuNaissancePersonnels, string $paysPersonnels, string $residencePersonnels, string $villePersonnels, string $salairePersonnels, string $cinPersonnels, string $matriculePersonnels):void{
        global $con;
        $req=$con->prepare("INSERT INTO personnels (nomPersonnels, prenomPersonnels, sexePersonnels, identifientPersonnels, photoPersonnels, adressePersonnels, fonctionPersonnels, professionPersonnels, emailPersonnels, telephonePersonnels, perePersonnels, merePersonnels, personneContactPersonnels, dateNaissancePersonnels, lieuNaissancePersonnels, paysPersonnels, residencePersonnels, villePersonnels, salairePersonnels, cinPersonnels, matriculePersonnels) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($nomPersonnels), secure($prenomPersonnels), secure($sexePersonnels), secure($identifientPersonnels), secure($photoPersonnels), secure($adressePersonnels), secure($fonctionPersonnels), secure($professionPersonnels), secure($emailPersonnels), secure($telephonePersonnels), secure($perePersonnels), secure($merePersonnels), secure($personneContactPersonnels), secure($dateNaissancePersonnels), secure($lieuNaissancePersonnels), secure($paysPersonnels), secure($residencePersonnels), secure($villePersonnels), secure($salairePersonnels), secure($cinPersonnels), secure($matriculePersonnels)]);
        $req->closeCursor();
    }
    public static function editPersonnel(string $nomPersonnels, string $prenomPersonnels, string $sexePersonnels, string $identifientPersonnels, string $photoPersonnels, string $adressePersonnels, string $emailPersonnels, string $telephonePersonnels, string $id):void{
        global $con;
        $req=$con->prepare("UPDATE personnels SET nomPersonnels = ?, prenomPersonnels = ?, sexePersonnels = ?, identifientPersonnels = ?, photoPersonnels = ?, adressePersonnels = ?, emailPersonnels = ?, telephonePersonnels = ? WHERE  idPersonnels = ?");
        $req->execute([secure($nomPersonnels), secure($prenomPersonnels), secure($sexePersonnels), secure($identifientPersonnels), secure($photoPersonnels), secure($adressePersonnels), secure($emailPersonnels), secure($telephonePersonnels),secure($id)]);
        $req->closeCursor();
    }
    public static function verifyPersonne(string $email):int{
        global $con;
        $req=$con->prepare('SELECT * FROM personnels WHERE emailPersonnels = ?');
        $req->execute([secure($email)]);
        $req->closeCursor();
        return $req->rowCount();
    }
    public static function verifyEmailPersonnel(string $email):int{
        global $con;
        $req=$con->prepare('SELECT * FROM personnels WHERE emailPersonnels = ?');
        $req->execute([secure($email)]);
        $req->closeCursor();
        return $req->rowCount();
    }
    public static function verifyNumeroPersonnel(string $numero):int{
        global $con;
        $req=$con->prepare('SELECT * FROM personnels WHERE telephonePersonnels = ?');
        $req->execute([secure($numero)]);
        $req->closeCursor();
        return $req->rowCount();
    }
    public static function verifyIdentifientPersonnel(string $login):int{
        global $con;
        $req=$con->prepare('SELECT * FROM personnels WHERE identifientPersonnels = ?');
        $req->execute([secure($login)]);
        $req->closeCursor();
        return $req->rowCount();
    }
    public static function selectPersonne(string $email):array {
        global $con;
        $req=$con->prepare("SELECT * FROM personnels INNER JOIN users  WHERE users.emailUsers=personnels.emailPersonnels AND emailPersonnels = ?;");
        $req->execute([secure($email)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getUsersByLogin(string $login):array {
        global $con;
        $req=$con->prepare("SELECT * FROM personnels INNER JOIN users WHERE users.emailUsers=personnels.emailPersonnels  AND users.identifientUsers = ?");
        $req->execute([secure($login)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllPersonnels():array {
        global $con;
        $req=$con->prepare("SELECT * FROM personnels INNER JOIN users WHERE  users.identifientUsers=personnels.identifientPersonnels;");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function deletePersonnels(string $id):void{
        global $con;
        $req=$con->prepare("DELETE FROM personnels WHERE idPersonnels = ?");
        $req->execute([secure($id)]);
        $req->closeCursor();
    }
    public static function getAllPersonnelsById(string $id):array {
        global $con;
        $req=$con->prepare("SELECT * FROM personnels INNER JOIN users  WHERE users.identifientUsers=personnels.identifientPersonnels AND personnels.idPersonnels = ?");
        $req->execute([secure($id)]);
        $resulats = [];
        while($data = $req->fetchObject()){
            array_push($resulats,$data);
        }
        $req->closeCursor();
        return $resulats;
    }
    public static function getUsersByEmail(string $email):array {
        global $con;
        $req=$con->prepare("SELECT * FROM personnels INNER JOIN users WHERE users.emailUsers=personnels.emailPersonnels  AND emailPersonnels = ?;");
        $req->execute([secure($email)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function updatePersonnels(string $nomPersonnels, string $prenomPersonnels, string $adressePersonnels, string $identifientPersonnels):void{
        global $con;
        $req=$con->prepare("UPDATE personnels SET nomPersonnels = ?, prenomPersonnels = ?, adressePersonnels = ? WHERE identifientPersonnels = ?;");
        $req->execute([secure($nomPersonnels), secure($prenomPersonnels), secure($adressePersonnels), secure($identifientPersonnels)]);
        $req->closeCursor();
    }
    public static function updatePersonnelsInfo(string $nomPersonnels, string $prenomPersonnels, string $photoPersonnels, string $adressePersonnels, string $emailPersonnels, string $identifientPersonnels):void{
        global $con;
        $req=$con->prepare("UPDATE personnels SET nomPersonnels = ?, prenomPersonnels = ?, photoPersonnels = ?, adressePersonnels = ?, emailPersonnels = ? WHERE identifientPersonnels = ?;");
        $req->execute([secure($nomPersonnels), secure($prenomPersonnels), secure($photoPersonnels), secure($adressePersonnels), secure($emailPersonnels), secure($identifientPersonnels)]);
        $req->closeCursor();
    }

}