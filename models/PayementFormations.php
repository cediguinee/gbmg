<?php

namespace models;

class PayementFormations
{
    public static function addPayementFormations(string $matriculeParticipants, string $idFormations, string $idModules, string $prixFormationNet, string $montantPayements, string $restePayements, string $identifientUsers, string $numeroRecu):void{
        global $con;
        $req=$con->prepare("INSERT INTO payements (matriculeParticipants, idFormations, idModules, prixFormationNet, montantPayements, restePayements, identifientUsers, numeroRecu) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($matriculeParticipants), secure($idFormations), secure($idModules), secure($prixFormationNet), secure($montantPayements), secure($restePayements), secure($identifientUsers), secure($numeroRecu)]);
        $req->closeCursor();
    }
    public static function updatePayementFormations(string $matriculeParticipants, string $idFormations, string $idModules, string $prixFormationNet, string $montantPayements, string $restePayements, string $identifientUsers, string $idPayements):void{
        global $con;
        $req=$con->prepare("UPDATE payements SET matriculeParticipants = ?, idFormations = ?, idModules = ?, prixFormationNet = ?, montantPayements = ?, restePayements = ?, identifientUsers = ? WHERE idPayements = ?;");
        $req->execute([secure($matriculeParticipants), secure($idFormations), secure($idModules), secure($prixFormationNet), secure($montantPayements), secure($restePayements), secure($identifientUsers), secure($idPayements)]);
        $req->closeCursor();
    }
    public static function deletePayementFormations(string $idPayements):void{
        global $con;
        $req=$con->prepare("DELETE FROM payements WHERE idPayements = ?");
        $req->execute([secure($idPayements)]);
        $req->closeCursor();
    }
    public static function getPayementFormationsById(string $idPayements):array {
        global $con;
        $req=$con->prepare("SELECT * FROM payements WHERE idPayements = ?;");
        $req->execute([secure($idPayements)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getPayementFormationsByReference(string $referencePayements):array {
        global $con;
        $req=$con->prepare("SELECT * FROM payements INNER JOIN categories INNER JOIN souscategorie INNER JOIN participants WHERE participants.matriculeParticipants=payements.matriculeParticipants AND payements.idFormations=souscategorie.idSousCat AND payements.idModules=categories.idCat AND payements.numeroRecu = ?");
        $req->execute([secure($referencePayements)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function verifyByMatricule(string $matricule):array {
        global $con;
        $req=$con->prepare("SELECT * FROM payements WHERE matriculeParticipants = ?;");
        $req->execute([secure($matricule)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllPayementFormations():array {
        global $con;
        $req=$con->prepare("SELECT * FROM payements ORDER BY idPayements DESC;");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;

    }
    public static function getpaiement(string $matricule,  string $idf,string $idm): string
    {
        global $con;
        $req = $con->prepare("SELECT SUM(montantPayements) as prixnet FROM payements WHERE matriculeParticipants=? AND idFormations=? AND idModules=?");
        $req->execute([secure($matricule),secure($idf),secure($idm)]);
        $n = $req->fetch()[0];
        if (empty($n)) {
            return 0;
        } else {
            return $n;
        }
    }
    public static function getprixformation(string $matricule,  string $idf,string $idm): string
    {
        global $con;
        $req = $con->prepare("SELECT SUM(prixFormationNet) as prixnet FROM payements WHERE matriculeParticipants=? AND idFormations=? AND idModules=?");
        $req->execute([secure($matricule),secure($idf),secure($idm)]);
        $n = $req->fetch()[0];
        if (empty($n)) {
            return 0;
        } else {
            return $n;
        }
    }

    
}