<?php


namespace models;


class Categorie
{
    public static function addCategorie(string $nomCat): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO categories (nomCat) VALUES (?);");
        $req->execute([secure($nomCat)]);
        $req->closeCursor();
    }
    public static function updateCategorie(string $nomCat, string $idCat): void
    {
        global $con;
        $req = $con->prepare("UPDATE categories SET nomCat = ? WHERE idCat = ?;");
        $req->execute([secure($nomCat), secure($idCat)]);
        $req->closeCursor();
    }


    public static function deleteCategorie(string $idCat): void
    {
        global $con;
        $req = $con->prepare("DELETE FROM categories WHERE idCat = ?");
        $req->execute([secure($idCat)]);
        $req->closeCursor();
    }
    public static function getCatgorieById(string $idCat): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM categories WHERE idCat = ?");
        $req->execute([secure($idCat)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function verifyByNomCategorie(string $nomCat): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM categories WHERE nomCat = ?");
        $req->execute([secure($nomCat)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllCategorie(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM categories ORDER BY nomCat DESC");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
}
