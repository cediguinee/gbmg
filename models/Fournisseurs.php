<?php

namespace models;

class Fournisseurs
{
    public static function addFournisseurs(string $prenomFournisseurs, string $nomFournisseurs, string $adresseFournisseurs, string $telephoneFournisseurs): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO fournisseurs (prenomFournisseurs, nomFournisseurs, adresseFournisseurs, telephoneFournisseurs) VALUES (?, ?, ?, ?);");
        $req->execute([secure($prenomFournisseurs), secure($nomFournisseurs), secure($adresseFournisseurs), secure($telephoneFournisseurs)]);
        $req->closeCursor();
    }

    public static function updateFournisseurs(string $prenomFournisseurs, string $nomFournisseurs, string $adresseFournisseurs, string $telephoneFournisseurs, string $idFournisseurs): void
    {
        global $con;
        $req = $con->prepare("UPDATE fournisseurs SET prenomFournisseurs = ?, nomFournisseurs = ?, adresseFournisseurs = ?, telephoneFournisseurs = ? WHERE idFournisseurs = ?;");
        $req->execute([secure($prenomFournisseurs), secure($nomFournisseurs), secure($adresseFournisseurs), secure($telephoneFournisseurs), secure($idFournisseurs)]);
        $req->closeCursor();
    }

    public static function deleteFournisseurs(string $idFournisseurss): void
    {
        global $con;
        $req = $con->prepare("DELETE FROM fournisseurs WHERE idFournisseurs = ?");
        $req->execute([secure($idFournisseurss)]);
        $req->closeCursor();
    }

    public static function getFournisseursById(string $idFournisseurs): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM fournisseurs WHERE idFournisseurs = ?");
        $req->execute([secure($idFournisseurs)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getAllFournisseurs(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM fournisseurs ORDER BY idFournisseurs DESC");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
}