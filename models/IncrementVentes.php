<?php

namespace models;

class IncrementVentes
{
    public static function addIncrement(): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO incrementvente (increment) VALUES (NULL);");
        $req->execute();
        $req->closeCursor();
    }
    public static function getAllIncrement(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM incrementvente");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
}