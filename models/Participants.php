<?php

namespace models;

class Participants
{
    public static function addParticipants(string $matriculeParticipants, string $civiliteParticipants, string $prenomParticipants, string $nomParticipants, string $contactParticipants, string $adresseParticipants, string $dateNaissanceParticipants, string $professionParticipants, string $residenceParticipants, string $villeParticipants, string $paysParticipants, string $langueParticipants, string $parQuiParticipants, string $numeroPartenaireParticipants): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO participants (matriculeParticipants, civiliteParticipants, prenomParticipants, nomParticipants, contactParticipants, adresseParticipants, dateNaissanceParticipants, professionParticipants, residenceParticipants, villeParticipants, paysParticipants, langueParticipants, parQuiParticipants, numeroPartenaireParticipants) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($matriculeParticipants), secure($civiliteParticipants), secure($prenomParticipants), secure($nomParticipants), secure($contactParticipants), secure($adresseParticipants), secure($dateNaissanceParticipants), secure($professionParticipants), secure($residenceParticipants), secure($villeParticipants), secure($paysParticipants), secure($langueParticipants), secure($parQuiParticipants), secure($numeroPartenaireParticipants)]);
        $req->closeCursor();
    }
    public static function updateParticipants(string $matriculeParticipants, string $civiliteParticipants, string $prenomParticipants, string $nomParticipants, string $contactParticipants, string $adresseParticipants, string $dateNaissanceParticipants, string $professionParticipants, string $residenceParticipants, string $villeParticipants, string $paysParticipants, string $langueParticipants, string $parQuiParticipants, string $numeroPartenaireParticipants, string $idParticipants): void
    {
        global $con;
        $req = $con->prepare("UPDATE participants SET matriculeParticipants = ?, civiliteParticipants = ?, prenomParticipants = ?, nomParticipants = ?, contactParticipants = ?, adresseParticipants = ?, dateNaissanceParticipants = ?, professionParticipants = ?, residenceParticipants = ?, villeParticipants = ?, paysParticipants = ?, langueParticipants = ?, parQuiParticipants = ?, numeroPartenaireParticipants = ? WHERE idParticipants = ?;");
        $req->execute([secure($matriculeParticipants), secure($civiliteParticipants), secure($prenomParticipants), secure($nomParticipants), secure($contactParticipants), secure($adresseParticipants), secure($dateNaissanceParticipants), secure($professionParticipants), secure($residenceParticipants), secure($villeParticipants), secure($paysParticipants), secure($langueParticipants), secure($parQuiParticipants), secure($numeroPartenaireParticipants), secure($idParticipants)]);
        $req->closeCursor();
    }
    public static function getAllParticipantsParCategorie(string $idFormation, string $idModule): array
    {
        global $con;
        $req = $con->prepare("SELECT DISTINCT participants.matriculeParticipants, participants.idParticipants, participants.prenomParticipants, participants.nomParticipants, participants.contactParticipants, participants.civiliteParticipants, participants.adresseParticipants, categories.nomCat, souscategorie.libelle FROM participants INNER JOIN payements INNER JOIN categories INNER JOIN souscategorie WHERE categories.idCat=payements.idModules AND souscategorie.idSousCat=payements.idFormations AND participants.matriculeParticipants=payements.matriculeParticipants AND payements.idFormations = ? AND payements.idModules = ? ORDER BY participants.idParticipants DESC;");
        $req->execute([secure($idFormation),secure($idModule)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllParticipantsParCategorieWhhiteStatut(string $idFormation, string $idModule, int $statut): array
    {
        global $con;
        $req = $con->prepare("SELECT DISTINCT participants.matriculeParticipants, participants.idParticipants, participants.prenomParticipants, participants.nomParticipants, participants.contactParticipants, participants.civiliteParticipants, participants.adresseParticipants, categories.nomCat, souscategorie.libelle, participants.etatParticipants  FROM participants INNER JOIN payements INNER JOIN categories INNER JOIN souscategorie WHERE categories.idCat=payements.idModules AND souscategorie.idSousCat=payements.idFormations AND participants.matriculeParticipants=payements.matriculeParticipants AND payements.idFormations = ? AND payements.idModules = ? AND participants.etatParticipants= ? ORDER BY participants.idParticipants DESC;");
        $req->execute([secure($idFormation),secure($idModule), secure($statut)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function updateStatutParticipants(string $id): void
    {
        global $con;
        $req = $con->prepare("UPDATE participants SET etatParticipants = '1' WHERE idParticipants = ?;");
        $req->execute([secure($id)]);
        $req->closeCursor();
    }

    public static function deleteParticipants(string $idParticipants): void
    {
        global $con;
        $req = $con->prepare("DELETE FROM participants WHERE idParticipants = ?");
        $req->execute([secure($idParticipants)]);
        $req->closeCursor();
    }
    public static function getParticipantsById(string $idParticipants): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM participants WHERE idParticipants = ?");
        $req->execute([secure($idParticipants)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllClientsNotification($idFormation,$idModule): array
    {
        global $con;
        $req = $con->prepare("SELECT DISTINCT participants.matriculeParticipants, participants.prenomParticipants, participants.nomParticipants, participants.contactParticipants FROM participants INNER JOIN payements WHERE payements.idFormations= ? AND payements.idModules= ?;");
        $req->execute([secure($idFormation),secure($idModule)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getParticipantsByMatricule(string $matricule): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM participants WHERE matriculeParticipants = ?");
        $req->execute([secure($matricule)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getAllParticipants(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM participants ORDER BY idParticipants DESC;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllParticipantsPayements(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM participants INNER JOIN payements INNER JOIN categories INNER JOIN souscategorie WHERE participants.matriculeParticipants=payements.matriculeParticipants AND categories.idCat=payements.idModules  AND souscategorie.idSousCat=payements.idFormations ORDER BY idParticipants DESC");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllParticipantsSolde(string $matricule): array
    {
        global $con;
        $req = $con->prepare("SELECT SUM(prixFormationNet) as prixformation, SUM(montantPayements) as avanceformation FROM payements WHERE matriculeParticipants = ?;");
        $req->execute([secure($matricule)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllParticipantsPartenaires(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM participants WHERE etatParticipants =1 ORDER BY idParticipants DESC;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllParticipantsPayementsFormations(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM participants INNER JOIN payements WHERE participants.matriculeParticipants=payements.matriculeParticipants ORDER BY idParticipants DESC;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllParticipantsPayementsLimit(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM participants INNER JOIN payements WHERE participants.matriculeParticipants=payements.matriculeParticipants ORDER BY idParticipants DESC LIMIT 10;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllParticipantsPartenaire(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM participants WHERE etatParticipants = 1 ORDER BY idParticipants DESC;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

}