<?php


namespace models;


class Annonces
{
    public static function addAnnonce(string $nameAnnonce, string $dateAnnonce, string $photoAnnonce, int $idSousCat , string $descriptionAnnonce, string $slog):void{
        global $con;
        $req=$con->prepare("INSERT INTO annonces (nameAnnonce,dateAnnonce,photoAnnonce,idSousCat ,descriptionAnnonce,slog) VALUES (?,?,?,?,?,?);");
        $req->execute([secure($nameAnnonce), secure($dateAnnonce), secure($photoAnnonce), secure($idSousCat ), secure($descriptionAnnonce),secure($slog)]);
        $req->closeCursor();
    }
    public static function updateAnnonce(string $nameAnnonce, string $dateAnnonce, string $photoAnnonce, string $idSousCat, string $descriptionAnnonce, string $idAnnonce):void{
        global $con;
        $req=$con->prepare("UPDATE annonces SET nameAnnonce = ?, dateAnnonce=?, photoAnnonce=?, idSousCat=?, descriptionAnnonce=? WHERE idAnnonce = ?;");
        $req->execute([secure($nameAnnonce), secure($dateAnnonce), secure($photoAnnonce), secure($idSousCat), secure($descriptionAnnonce), secure($idAnnonce)]);
        $req->closeCursor();
    }

   
    public static function deleteAnnonce(string $idAnnonce):void{
        global $con;
        $req=$con->prepare("DELETE FROM annonces WHERE idAnnonce = ?");
        $req->execute([secure($idAnnonce)]);
        $req->closeCursor();
    }
    public static function getAnnonceById(string $idAnnonce):array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat AND idAnnonce = ?");
        $req->execute([secure($idAnnonce)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function verifyByNomAnnonce(string $nameAnnonce):array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces WHERE nameAnnonce = ?");
        $req->execute([secure($nameAnnonce)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonce():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
 
    public static function getbyidSouscat(string $idSousCat):array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces WHERE idSousCat = ?");
        $req->execute([secure($idSousCat)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonceLimited():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce DESC LIMIT 4");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonceLimited1():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce ASC LIMIT 1");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonceLimited2():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce DESC LIMIT 2");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonceLimited5():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce ASC LIMIT 5");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonceSlide1():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce ASC LIMIT 4");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonceLimited51():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce DESC LIMIT 5");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonceByIdCategorie(string $idSousCategorie):array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat AND annonces.idSousCat = ? ORDER by idAnnonce DESC ");
        $req->execute([secure($idSousCategorie)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllAnnonceSlide2():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce DESC LIMIT 4");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllImages():array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat ORDER by idAnnonce DESC LIMIT 9");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAnnonceBySlog(string $slog):array {
        global $con;
        $req=$con->prepare("SELECT * FROM annonces INNER JOIN souscategorie WHERE annonces.idSousCat=souscategorie.idSousCat AND slog = ?");
        $req->execute([secure($slog)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }

}