<?php


namespace models;


class Partenaires
{
    public static function addPartenaires(string $photo, string $entreprise, string $sites): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO partenaires (photo, entreprise, sites) VALUES (?, ?, ?);");
        $req->execute([secure($photo),secure($entreprise),secure($sites)]);
        $req->closeCursor();
    }

    public static function deletePartenaires(string $id): void
    {
        global $con;
        $req = $con->prepare("DELETE FROM partenaires WHERE idPartenaire = ?");
        $req->execute([secure($id)]);
        $req->closeCursor();
    }

    public static function getAllPartenaires(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM partenaires ORDER BY idPartenaire DESC");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
}
