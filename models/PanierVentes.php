<?php

namespace models;

class PanierVentes
{
    public static function addPanier(string $idProduits, string $quantite, string $prix, string $total, string $users):void{
        global $con;
        $req=$con->prepare("INSERT INTO paniersventes (idProduits, quantite, prix, total, users) VALUES (?, ?, ?, ?, ?);;");
        $req->execute([secure($idProduits), secure($quantite), secure($prix), secure($total), secure($users)]);
        $req->closeCursor();
    }
    public static function updatePanier(string $idProduits, string $quantite, string $prix, string $total, string $users, string $id):void{
        global $con;
        $req=$con->prepare("UPDATE paniersventes SET idProduits = ?, quantite = ?, prix = ?, total = ?, users = ? WHERE idPaniers = ?;");
        $req->execute([secure($idProduits), secure($quantite), secure($prix), secure($total), secure($users), secure($id)]);
        $req->closeCursor();
    }
    public static function updatePanierQuantite(string $quantite, string $prix, string $total, string $users, string $idProduits):void{
        global $con;
        $req=$con->prepare("UPDATE paniersventes SET  quantite = ?, prix = ?, total = ? WHERE users = ? AND idProduits = ?;");
        $req->execute([secure($quantite),secure($prix),secure($total),secure($users), secure($idProduits)]);
        $req->closeCursor();
    }

    public static function deletePanier(string $idPaniers):void{
        global $con;
        $req=$con->prepare("DELETE FROM paniersventes WHERE idPaniers = ?");
        $req->execute([secure($idPaniers)]);
        $req->closeCursor();
    }
    public static function deletePanierAll():void{
        global $con;
        $req=$con->prepare("TRUNCATE TABLE paniersventes;");
        $req->execute();
        $req->closeCursor();
    }
    public static function delete(string $users):void{
        global $con;
        $req=$con->prepare("DELETE FROM paniersventes WHERE users = ?;");
        $req->execute([secure($users)]);
        $req->closeCursor();
    }
    public static function getPanierById(string $idPaniers):array {
        global $con;
        $req=$con->prepare("SELECT * FROM paniersventes WHERE idPaniers = ?");
        $req->execute([secure($idPaniers)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getAllPanier($users):array {
        global $con;
        $req=$con->prepare("SELECT * FROM paniersventes INNER JOIN produits WHERE produits.idProduits=paniersventes.idProduits AND users = ? ORDER BY idPaniers DESC");
        $req->execute([secure($users)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}