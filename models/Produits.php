<?php

namespace models;

class Produits
{
 public static function  addProduits(string $libelleProduits, string $prixAchatProduits, string $prixDeVenteProduits, string $dateProduits):void{
     global $con;
     $req=$con->prepare("INSERT INTO produits (libelleProduits, prixAchatProduits, prixDeVenteProduits, dateProduits) VALUES ( ?, ?, ?, ?);");
     $req->execute([secure($libelleProduits), secure($prixAchatProduits), secure($prixDeVenteProduits),secure($dateProduits)]);
     $req->closeCursor();
 }
 public static function  updateProduits(string $libelleProduits, string $prixAchatProduits, string $prixDeVenteProduits, string $dateProduits,string $idProduits):void{
     global $con;
     $req=$con->prepare("UPDATE produits SET libelleProduits = ?, prixAchatProduits = ?, prixDeVenteProduits = ?, dateProduits = ? WHERE idProduits = ?;");
     $req->execute([secure($libelleProduits), secure($prixAchatProduits), secure($prixDeVenteProduits),secure($dateProduits),secure($idProduits)]);
     $req->closeCursor();
 }
    public static function  deleteProduits(string $idProduits):void{
        global $con;
        $req=$con->prepare("DELETE FROM produits WHERE idProduits = ?;");
        $req->execute([secure($idProduits)]);
        $req->closeCursor();
    }
 public static function  showAllProduits():array{
     global $con;
     $req=$con->prepare("SELECT * FROM produits ORDER BY idProduits DESC");
     $req->execute();
     $resultats = [];
     while($data = $req->fetchObject()){
         array_push($resultats,$data);
     }
     $req->closeCursor();
     return $resultats;
 }
 public static function  showProduitsById(string $idProduits):array{
     global $con;
     $req=$con->prepare("SELECT * FROM produits WHERE idProduits = ?");
     $req->execute([secure($idProduits)]);
     $resultats = [];
     while($data = $req->fetchObject()){
         array_push($resultats,$data);
     }
     $req->closeCursor();
     return $resultats;
 }
}