<?php

namespace models;

class Sliders
{
    public static function addSlider(string $nom, string $description, string $photo, string $status):void{
        global $con;
        $req=$con->prepare("INSERT INTO sliders (nom, description, photo, status) VALUES (?, ?, ?, ?);");
        $req->execute([secure($nom), secure($description), secure($photo), secure($status)]);
        $req->closeCursor();
    }
    public static function editSlider(string $nom, string $description, string $photo, string $status, string $id):void {
        global $con;
        $req=$con->prepare("UPDATE sliders SET nom = ?, description = ?, photo = ?, status = ? WHERE id = ?;");
        $req->execute([secure($nom), secure($description), secure($photo), secure($status), secure($id)]);
        $req->closeCursor();
    }
    public static function verifySlider(string $modele):int {
        global $con;
        $req=$con->prepare("SELECT * FROM sliders WHERE nom = ?;");
        $req->execute([secure($modele)]);
        $req->closeCursor();
        return $req->rowCount();
    }
    public static function getSliderById(string $id):array {
        global $con;
        $req=$con->prepare("SELECT * FROM sliders WHERE id = ?;");
        $req->execute([secure($id)]);
        $resultats = [];
        while ($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function delSlider(string $id):void{
        global $con;
        $req=$con->prepare("DELETE FROM sliders WHERE id = ?");
        $req->execute([secure($id)]);
    }
    public static function getAllSlider():array {
        global $con;
        $req=$con->prepare("SELECT * FROM sliders;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllSliderLimit():array {
        global $con;
        $req=$con->prepare("SELECT * FROM sliders ORDER BY id DESC LIMIT 1;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function updateStatus(string $status, string $id):void
    {
        global $con;
        $req = $con->prepare("UPDATE sliders SET status=? WHERE id=?");
        $req->execute([secure($status), secure($id)]);
        $req->closeCursor();
    }

}