<?php

namespace models;

class Prospects
{
    public static function addProspects(string $nomPospects, string $prenomPospects, string $contactPospects, string $emailPospects, string $idCategories, string $idSousCategories): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO prospects (nomPospects, prenomPospects, contactPospects, emailPospects, idCategories, idSousCategories) VALUES (?, ?, ?, ?, ?, ?);");
        $req->execute([secure($nomPospects), secure($prenomPospects), secure($contactPospects), secure($emailPospects), secure($idCategories), secure($idSousCategories)]);
        $req->closeCursor();
    }
    public static function updateProspects(string $nomPospects, string $prenomPospects, string $contactPospects, string $emailPospects, string $idCategories, string $idSousCategories, string $idProspects): void
    {
        global $con;
        $req = $con->prepare("UPDATE prospects SET nomPospects = ?, prenomPospects = ?, contactPospects = ?, emailPospects = ?, idCategories = ?, idSousCategories = ? WHERE idPospects = ?;");
        $req->execute([secure($nomPospects), secure($prenomPospects), secure($contactPospects), secure($emailPospects), secure($idCategories), secure($idSousCategories), secure($idProspects)]);
        $req->closeCursor();
    }


    public static function deleteProspects(string $idCat): void
    {
        global $con;
        $req = $con->prepare("DELETE FROM prospects WHERE idPospects = ?");
        $req->execute([secure($idCat)]);
        $req->closeCursor();
    }
    public static function getProspectsById(string $idProspect): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM prospects WHERE idPospects = ?;");
        $req->execute([secure($idProspect)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function verifyProspects(string $idCategories, string $idSousCategories, string $contactPospects): int
    {
        global $con;
        $req = $con->prepare("SELECT * FROM prospects WHERE idCategories = ? AND idSousCategories = ? AND contactPospects = ?;");
        $req->execute([secure($idCategories), secure($idSousCategories), secure($contactPospects)]);
        $req->closeCursor();
        return $req->rowCount();
    }
    public static function getAllProspects(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM prospects INNER JOIN categories INNER JOIN souscategorie WHERE categories.idCat=prospects.idCategories AND souscategorie.idSousCat = prospects.idSousCategories ORDER BY idPospects DESC;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllProspectsNotification(string $idModule, string $idFormation): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM prospects INNER JOIN categories INNER JOIN souscategorie WHERE categories.idCat=prospects.idCategories AND souscategorie.idSousCat = prospects.idSousCategories AND prospects.idCategories = ? AND prospects.idSousCategories = ? ORDER BY idPospects DESC;");
        $req->execute([secure($idModule),secure($idFormation)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllProspectsById(string $idProspects): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM prospects INNER JOIN categories INNER JOIN souscategorie WHERE categories.idCat=prospects.idCategories AND souscategorie.idSousCat = prospects.idSousCategories AND prospects.idProspects = ? ORDER BY idPospects DESC;");
        $req->execute([secure($idProspects)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
}