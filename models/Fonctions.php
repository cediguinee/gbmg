<?php


namespace models;


class Fonctions
{
    public static function getAllFonctions():array {
        global $con;
        $req=$con->prepare("SELECT * FROM fonctions");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}