<?php


namespace models;


class Partenaire
{
    public static function addPartenaire(string $photo, string $entreprise, string $sites):void{
        global $con;
        $req=$con->prepare("INSERT INTO partenaires (photo,entreprise,sites) VALUES (?,?,?);");
        $req->execute([secure($photo), secure($entreprise), secure($sites)]);
        $req->closeCursor();
    }
    public static function updatePartenaire(string $photo, string $entreprise, string $sites, string $id):void{
        global $con;
        $req=$con->prepare("UPDATE partenaires SET photo = ?, entreprise=?, sites=?  WHERE idPartenaire = ?;");
        $req->execute([secure($photo), secure($entreprise), secure($sites), secure($id)]);
        $req->closeCursor();
    }

   
    public static function deletePartenaire(string $idPartenaire):void{
        global $con;
        $req=$con->prepare("DELETE FROM partenaires WHERE idPartenaire = ?");
        $req->execute([secure($idPartenaire)]);
        $req->closeCursor();
    }
    public static function getPartenaireById(string $idPartenaire):array {
        global $con;
        $req=$con->prepare("SELECT * FROM partenaires WHERE idPartenaire = ?");
        $req->execute([secure($idPartenaire)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function verifyByNomEntreprise(string $entreprise):array {
        global $con;
        $req=$con->prepare("SELECT * FROM partenaires WHERE entreprise = ?");
        $req->execute([secure($entreprise)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllPartenaires():array {
        global $con;
        $req=$con->prepare("SELECT * FROM partenaires ORDER by idPartenaire DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
 
    

}