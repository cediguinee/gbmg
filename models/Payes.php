<?php

namespace models;

class Payes
{
    public static function addPayes(string $idPersonnels,string $montantPayes, string $identifientUsers, string $datePayes, string $moisPayes, string $anneePayes):void{
        global $con;
        $req=$con->prepare("INSERT INTO payes (idPersonnels, montantPayes, identifientUsers, datePayes, moisPayes, anneePayes) VALUES (?, ?, ?, ?, ?, ?);");
        $req->execute([secure($idPersonnels), secure($montantPayes), secure($identifientUsers), secure($datePayes), secure($moisPayes), secure($anneePayes)]);
        $req->closeCursor();
    }
    public static function updatePayes(string $idPersonnels,string $montantPayes, string $identifientUsers, string $datePayes, string $moisPayes, string $anneePayes, string $idPayes):void{
        global $con;
        $req=$con->prepare("UPDATE payes SET idPersonnels = ?, montantPayes = ?, identifientUsers = ?, datePayes = ?, moisPayes = ?, anneePayes = ?    WHERE idPayes = ?;");
        $req->execute([secure($idPersonnels), secure($montantPayes), secure($identifientUsers), secure($datePayes), secure($moisPayes), secure($anneePayes), secure($idPayes)]);
        $req->closeCursor();
    }
    public static function deletePayes(string $idPayes):void{
        global $con;
        $req=$con->prepare("DELETE FROM payes WHERE idPayes = ?");
        $req->execute([secure($idPayes)]);
        $req->closeCursor();
    }
    public static function getPayesById(string $idPayes):array {
        global $con;
        $req=$con->prepare("SELECT * FROM payes WHERE idPayes = ?");
        $req->execute([secure($idPayes)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllPayes():array {
        global $con;
        $req=$con->prepare("SELECT * FROM payes ORDER BY idPayes DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllPayesEmployes():array {
        global $con;
        $req=$con->prepare("SELECT * FROM payes INNER JOIN personnels WHERE payes.idPersonnels = personnels.idPersonnels ORDER  BY payes.idPayes DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}