<?php


namespace models;


class Prefecture
{
    public static function addPrefecture(string $nom):void{
        global $con;
        $req=$con->prepare("INSERT INTO prefecture (nomPrefecture) VALUES (?);");
        $req->execute([secure($nom)]);
        $req->closeCursor();
    }
    public static function updatePrefecture(string $nomPrefecture, string $id):void{
        global $con;
        $req=$con->prepare("UPDATE prefecture SET nomPrefecture = ? WHERE idPrefecture = ?;");
        $req->execute([secure($nomPrefecture), secure($id)]);
        $req->closeCursor();
    }
   
   
    public static function deletePrefecture(string $idClients):void{
        global $con;
        $req=$con->prepare("DELETE FROM prefecture WHERE idPrefecture = ?");
        $req->execute([secure($idClients)]);
        $req->closeCursor();
    }
    public static function getPrefectureById(string $idClient):array {
        global $con;
        $req=$con->prepare("SELECT * FROM prefecture WHERE idPrefecture = ?");
        $req->execute([secure($idClient)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function verifyByNomprefecture(string $nom):array {
        global $con;
        $req=$con->prepare("SELECT * FROM prefecture WHERE nomPrefecture = ?");
        $req->execute([secure($nom)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getAllprefecture():array {
        global $con;
        $req=$con->prepare("SELECT * FROM prefecture ORDER BY idPrefecture DESC");
        $req->execute();
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
 

}