<?php 
namespace models;
class Login{
     public static function veriflogin(string $email, string $password):array {
            global $con;
            $req=$con->prepare("SELECT * FROM users INNER JOIN personnels WHERE personnels.identifientPersonnels=users.identifientUsers AND identifientUsers = ? AND passwordUsers = ?");
            $req->execute([secure($email),secure($password)]);
            $resultats = [];
            while($data = $req->fetchObject()){
                    array_push($resultats,$data);
            }
            $req->closeCursor();
            return $resultats;
     }
     public static function getLastConnexion(string $login):array {
         global $con;
         $req=$con->prepare("SELECT * FROM logins WHERE users = ? ORDER BY idLogins DESC LIMIT 5");
         $req->execute([secure($login)]);
         $resultats = [];
         while($data = $req->fetchObject()){
             array_push($resultats,$data);
         }
         $req->closeCursor();
         return $resultats;
     }
     public static function verifyConnexion(string $email,string $login):array {
         global $con;
         $req=$con->prepare("SELECT * FROM users WHERE emailUsers = ? AND identifientUsers = ?");
         $req->execute([secure($email), secure($login)]);
         $resultats = [];
         while($data = $req->fetchObject()){
             array_push($resultats,$data);
         }
         $req->closeCursor();
         return $resultats;
     }
     public static function selectUsersById(string $idPersonnel):array {
         global $con;
         $req=$con->prepare("SELECT * FROM users WHERE idUsers  = ? LIMIT 1");
         $req->execute([secure($idPersonnel)]);
         $resultats = [];
         while($data = $req->fetchObject()){
             array_push($resultats,$data);
         }
         $req->closeCursor();
         return $resultats;
     }

    public static function updatePassword(string $password, string $login):void{
        global $con;
        $req=$con->prepare("UPDATE users SET passwordUsers = ? WHERE identifientUsers	 = ?;");
        $req->execute([secure($password),secure($login)]);
        $req->closeCursor();
    }
     public static function updateUser(string $etatUsers, string $idUsers):void{
         global $con;
         $req=$con->prepare("UPDATE users SET etatUsers = ? WHERE idUsers = ?;");
         $req->execute([secure($etatUsers),secure($idUsers)]);
         $req->closeCursor();
     }
}