<?php


namespace models;


class SousCategorie
{

        public static function addSousCategorie(string $libelle):void{
            global $con;
            $req=$con->prepare("INSERT INTO souscategorie (libelle) VALUES (?);");
            $req->execute([secure($libelle)]);
            $req->closeCursor();
        }
        public static function updateSousCat(string $libelle, string $id):void{
            global $con;
            $req=$con->prepare("UPDATE souscategorie SET libelle = ? WHERE idSousCat = ?");
            $req->execute([secure($libelle), secure($id)]);
            $req->closeCursor();
        }
       
       
        public static function deleteSousCat(string $idSousCat):void{
            global $con;
            $req=$con->prepare("DELETE FROM souscategorie WHERE idSousCat = ?");
            $req->execute([secure($idSousCat)]);
            $req->closeCursor();
        }
        public static function getSousCatById(string $idSousCat):array {
            global $con;
            $req=$con->prepare("SELECT * FROM souscategorie WHERE idSousCat = ?");
            $req->execute([secure($idSousCat)]);
            $resultats = [];
            while($data = $req->fetchObject()){
                array_push($resultats,$data);
            }
            $req->closeCursor();
            return $resultats;
        }
        public static function verifyByLibelle(string $libelle):array {
            global $con;
            $req=$con->prepare("SELECT * FROM souscategorie WHERE libelle = ?");
            $req->execute([secure($libelle)]);
            $resultats = [];
            while($data = $req->fetchObject()){
                array_push($resultats,$data);
            }
            $req->closeCursor();
            return $resultats;
        }
        public static function getAllSousCategorie():array {
            global $con;
            $req=$con->prepare("SELECT * FROM souscategorie  ORDER by idSousCat DESC");
            $req->execute();
            $resultats = [];
            while($data = $req->fetchObject()){
                array_push($resultats,$data);
            }
            $req->closeCursor();
            return $resultats;
        }
        public static function getSousIdCat(string $idCat):array {
            global $con;
            $req=$con->prepare("SELECT * FROM souscategorie WHERE idSousCat = ?");
            $req->execute([secure($idCat)]);
            $resultats = [];
            while($data = $req->fetchObject()){
                array_push($resultats,$data);
            }
            $req->closeCursor();
            return $resultats;
        }

        public static function detailsouscategorie(string $idSousCat):array {
            global $con;
            $req=$con->prepare("SELECT * FROM souscategorie INNER JOIN annonces,categories WHERE souscategorie.idSousCat=annonces.idSousCat AND souscategorie.idCat=categories.idCat AND souscategorie.idSousCat=?");
            $req->execute([secure($idSousCat)]);
            $resultats = [];
            while($data = $req->fetchObject()){
                array_push($resultats,$data);
            }
            $req->closeCursor();
            return $resultats;
        }
    
}