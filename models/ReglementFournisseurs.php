<?php

namespace models;

class ReglementFournisseurs
{
    public static function addReglement(string $montantReglements, string $users, string $idfournisseur, string $reference): void
    {
        global $con;
        $req = $con->prepare("INSERT INTO reglementfournisseurs (montantReglements, users, idfournisseur, reference) VALUES (?, ?, ?, ?);");
        $req->execute([secure($montantReglements), secure($users), secure($idfournisseur), secure($reference)]);
        $req->closeCursor();
    }

    public static function getReglementById(string $idfournisseur): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM reglementfournisseurs WHERE idfournisseur = ?");
        $req->execute([secure($idfournisseur)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getReglementSommeById(string $idfournisseur): array
    {
        global $con;
        $req = $con->prepare("SELECT SUM(montantReglements) as montantReglements FROM reglementfournisseurs WHERE idfournisseur = ?");
        $req->execute([secure($idfournisseur)]);
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }

    public static function getAllReglements(): array
    {
        global $con;
        $req = $con->prepare("SELECT * FROM reglementfournisseurs INNER JOIN fournisseurs WHERE fournisseurs.idFournisseurs=reglementfournisseurs.idfournisseur ORDER BY reglementfournisseurs.idReglements DESC;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()) {
            array_push($resultats, $data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function delReglementsFournisseur(string $idReglements):void{
        global $con;
        $req=$con->prepare("DELETE FROM reglementfournisseurs WHERE idReglements = ?");
        $req->execute([secure($idReglements)]);
    }

}
