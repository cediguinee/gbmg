<?php


namespace models;


class Connexions
{
    public static function addConnexion(string $users):void{
        global $con;
        $req=$con->prepare("INSERT INTO logins (users) VALUES (?);");
        $req->execute([secure($users)]);
        $req->closeCursor();
    }
    public static function getConnexion(string $users):array {
        global $con;
        $req=$con->prepare("SELECT * FROM logins WHERE users = ? ORDER BY idLogins DESC LIMIT 1;");
        $req->execute([secure($users)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}