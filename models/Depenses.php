<?php

namespace models;

class Depenses
{
    public static function addDepenses(string $personnesDepenses, string $dateDepenses, string $montantDepenses, string $descriptionDepenses, string $identifientUsers, string $quantiteDepenses, string $totalDepenses, string $idTypes):void{
        global $con;
        $req=$con->prepare("INSERT INTO depenses (personnesDepenses, dateDepenses, montantDepenses, descriptionDepenses, identifientUsers, quantiteDepenses, totalDepenses, idTypes) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($personnesDepenses), secure($dateDepenses), secure($montantDepenses), secure($descriptionDepenses), secure($identifientUsers), secure($quantiteDepenses), secure($totalDepenses), secure($idTypes)]);
        $req->closeCursor();
    }
    public static function editDepenses(string $personnesDepenses, string $dateDepenses, string $montantDepenses, string $descriptionDepenses, string $identifientUsers,string $quantiteDepenses, string $totalDepenses, string $idTypes, string $idDepenses):void {
        global $con;
        $req=$con->prepare("UPDATE depenses SET personnesDepenses = ?,  dateDepenses = ?, montantDepenses = ?, descriptionDepenses = ?, identifientUsers = ?, quantiteDepenses = ?, totalDepenses = ?, idTypes = ? WHERE idDepenses = ?;");
        $req->execute([secure($personnesDepenses), secure($dateDepenses), secure($montantDepenses), secure($descriptionDepenses), secure($identifientUsers),secure($quantiteDepenses), secure($totalDepenses), secure($idTypes), secure($idDepenses)]);
        $req->closeCursor();
    }
    public static function getDepensesById(string $idDepenses):array {
        global $con;
        $req=$con->prepare("SELECT * FROM depenses WHERE idDepenses = ?");
        $req->execute([secure($idDepenses)]);
        $resultats = [];
        while ($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function delDepenses(string $idDepenses):void{
        global $con;
        $req=$con->prepare("DELETE FROM depenses WHERE idDepenses = ?;");
        $req->execute([secure($idDepenses)]);
    }
    public static function getAllDepenses():array {
        global $con;
        $req=$con->prepare("SELECT * FROM depenses INNER JOIN personnels WHERE depenses.identifientUsers=personnels.identifientPersonnels ORDER BY depenses.idDepenses DESC;");
        $req->execute();
        $resultats = [];
        while ($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;
    }
}