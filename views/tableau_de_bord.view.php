<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>Tableau de bord</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Tableau de</span></li>

                        <li><span>Bord</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="row mb-3">
                        <div class="col-md-3">
                            <section class="card card-featured-left card-featured-primary mb-3">
                                <div class="card-body">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col widget-summary-col-icon">
                                            <div class="summary-icon bg-primary">
                                                <i class="fa fa-user-graduate"></i>
                                            </div>
                                        </div>
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title">Participants</h4>
                                                <div class="info">
                                                    <strong class="amount"><?=number_format(count(\models\Participants::getAllParticipants()))?></strong>
                                                    <span class="text-primary"></span>
                                                </div>
                                            </div>
                                            <div class="summary-footer">
                                                <a class="text-muted text-uppercase" href="<?=LINK.'nouveau_client'?>">Nouveau Participant</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-md-3">
                            <section class="card card-featured-left card-featured-warning">
                                <div class="card-body">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col widget-summary-col-icon">
                                            <div class="summary-icon bg-warning">
                                                <i class="fa fa-users"></i>
                                            </div>
                                        </div>
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title">Prospects</h4>
                                                <div class="info">
                                                    <strong class="amount"><?=number_format(count(\models\Prospects::getAllProspects()))?></strong>
                                                </div>
                                            </div>
                                            <div class="summary-footer">
                                                <a class="text-muted text-uppercase" href="<?=LINK.'nouveau_prospect'?>">Nouveau prospect</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-md-3">
                            <section class="card card-featured-left card-featured-secondary">
                                <div class="card-body">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col widget-summary-col-icon">
                                            <div class="summary-icon bg-secondary">
                                                <i class="fa fa-box"></i>
                                            </div>
                                        </div>
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title">Produits</h4>
                                                <div class="info">
                                                    <strong class="amount"><?=number_format(count(\models\Produits::showAllProduits()))?></strong>
                                                </div>
                                            </div>
                                            <div class="summary-footer">
                                                <a class="text-muted text-uppercase" href="<?=LINK.'nouveau_produit'?>">Nouveau Produit</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-md-3">
                            <section class="card card-featured-left card-featured-success">
                                <div class="card-body">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col widget-summary-col-icon">
                                            <div class="summary-icon bg-success">
                                                <i class="fa fa-user-circle"></i>
                                            </div>
                                        </div>
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title">Clients</h4>
                                                <div class="info">
                                                    <strong class="amount"><?=number_format(count(\models\Client::getAllClient()))?></strong>
                                                </div>
                                            </div>
                                            <div class="summary-footer">
                                                <a class="text-muted text-uppercase" href="<?=LINK.'new_client'?>">Nouveau client</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6">
                            <section class="card card-featured-left card-featured-tertiary mb-3">
                                <div class="card-body">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col widget-summary-col-icon">
                                            <div class="summary-icon bg-tertiary">
                                                <i class="fas fa-shopping-cart"></i>
                                            </div>
                                        </div>
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title">N° d'Achat</h4>
                                                <div class="info">
                                                    <strong class="amount"><?=number_format(count(\models\Achats::getAchatAll()))?></strong>
                                                </div>
                                            </div>
                                            <div class="summary-footer">
                                                <a class="text-muted text-uppercase" href="<?=LINK.'nouveau_achat'?>">Nouvel achat</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-xl-6">
                            <section class="card card-featured-left card-featured-quaternary">
                                <div class="card-body">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col widget-summary-col-icon">
                                            <div class="summary-icon bg-quaternary">
                                                <i class="fas fa-user"></i>
                                            </div>
                                        </div>
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title">N° Utilisateur</h4>
                                                <div class="info">
                                                    <strong class="amount"><?=number_format(count(\models\Personnels::getAllPersonnels()))?></strong>
                                                </div>
                                            </div>
                                            <div class="summary-footer">
                                                <a class="text-muted text-uppercase" href="<?=LINK.'nouveau_employe'?>">Crée un employé</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h5>Montant des formations</h5>

                <hr>
                <div class="col-md-4">
                    <section class="card">
                        <div class="card-body">
                            <div class="h4 font-weight-bold mb-0">
                                <?php
                                  echo number_format($prixFormationNet,'0',',',',').' GNF';
                                ?>
                            </div>
                            <p class="text-3 text-muted mb-0">Totals des formations</p>
                        </div>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="card">
                        <div class="card-body">
                            <div class="h4 font-weight-bold mb-0">
                                <?php
                                echo number_format($montantPayements,'0',',',',').' GNF';
                                ?>
                            </div>
                            <p class="text-3 text-muted mb-0">Totals payes</p>
                        </div>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="card">
                        <div class="card-body">
                            <div class="h4 font-weight-bold mb-0">
                                <?php
                                echo number_format($restePayements,'0',',',',').' GNF';
                                ?>
                            </div>
                            <p class="text-3 text-muted mb-0">Totals des restes</p>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <h5>Totals montants</h5>
                <hr>
                <div class="col-md-4">
                    <section class="card">
                        <div class="card-body">
                            <div class="h4 font-weight-bold mb-0"> <?php
                                echo number_format($montantdesachats,'0',',',',').' GNF';
                                ?></div>
                            <p class="text-3 text-muted mb-0">Totals des Achats</p>
                        </div>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="card">
                        <div class="card-body">
                            <div class="h4 font-weight-bold mb-0">
                                <?php
                                echo number_format($montantdesventes,'0',',',',').' GNF';
                                ?>
                            </div>
                            <p class="text-3 text-muted mb-0">Totals des Ventes</p>
                        </div>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="card">
                        <div class="card-body">
                            <div class="h4 font-weight-bold mb-0">
                                <?php
                                echo number_format($solde,'0',',',',').' GNF';
                                ?>
                            </div>
                            <p class="text-3 text-muted mb-0">Total rapport des ventes</p>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row pt-4 mt-1">
                <div class="col-xl-12">
                    <section class="card">
                        <header class="card-header card-header-transparent">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Liste des dernières inscriptions</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-responsive-md table-striped mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Prénoms & Nom</th>
                                    <th>Montant</th>
                                    <th>Avance</th>
                                    <th>Reste</th>
                                    <th>Statut</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $increment = 1;
                                if(isset($getParticipants) AND !empty($getParticipants)):?>
                                    <?php foreach($getParticipants as $participant):?>
                                        <tr>
                                            <td><?=$increment++?></td>
                                            <td><?=ucfirst($participant->prenomParticipants).' '.strtoupper($participant->nomParticipants).' | '.$participant->numeroPartenaireParticipants?></td>
                                            <td><?=number_format($participant->prixFormationNet).' GNF'?></td>
                                            <td>
                                                <?=number_format($participant->montantPayements).' GNF'?>
                                            </td>
                                            <td>
                                                <?=number_format($participant->restePayements).' GNF'?>
                                            </td>
                                            <td>
                                                <?php if($participant->restePayements==0):?>
                                                    <span class="badge badge-success">Fini</span>
                                                <?php else:?>
                                                    <span class="badge badge-warning">Encoure</span>
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>
