<!DOCTYPE html>
<!--[if IE 9]>
<html class="no-js lt-ie10" lang="en">
<![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<?php require_once("includes/head.inc.php"); ?>
<?php require_once("includes/header.inc.php"); ?>

<body>
<!-- Page Wrapper -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
Available classes:
'page-loading'      enables page preloader
-->
<div id="page-wrapper" class="page-loading">
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->

    <!-- END Preloader -->
    <?php require_once("includes/loader.inc.php"); ?>
    <!-- Page Container -->
    <!-- In the PHP version you can set the following options from inc/config file -->
    <!--
    Available #page-container classes:

    'sidebar-light'                                 for a light main sidebar (You can add it along with any other class)

    'sidebar-visible-lg-mini'                       main sidebar condensed - Mini Navigation (> 991px)
    'sidebar-visible-lg-full'                       main sidebar full - Full Navigation (> 991px)

    'sidebar-alt-visible-lg'                        alternative sidebar visible by default (> 991px) (You can add it along with any other class)

    'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
    'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar

    'fixed-width'                                   for a fixed width layout (can only be used with a static header/main sidebar layout)

    'enable-cookies'                                enables cookies for remembering active color theme when changed from the sidebar links (You can add it along with any other class)
-->
    <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
        <!-- Alternative Sidebar -->
        <div id="sidebar-alt" tabindex="-1" aria-hidden="true">
            <!-- Toggle Alternative Sidebar Button (visible only in static layout) -->
            <a href="javascript:void(0)" id="sidebar-alt-close" onclick="App.sidebar('toggle-sidebar-alt');"><i class="fa fa-times"></i></a>

            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll-alt">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Profile -->
                    <?php require_once("includes/profil.inc.php"); ?>
                    <!-- END Profile -->

                    <!-- Settings -->
                    <?php require_once("includes/settings.inc.php"); ?>
                    <!-- END Settings -->
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Alternative Sidebar -->

        <!-- Main Sidebar -->
        <?php require_once("includes/sidebar.inc.php"); ?>
        <!-- END Main Sidebar -->
        <!-- Main Container -->
        <div id="main-container">
            <?php require_once("includes/header.inc.php"); ?>
            <!-- END Header -->
            <!-- Page content -->
            <div id="page-content">
                <!-- Table Styles Header -->
                <div class="content-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="header-section">
                                <h1>Liste des utilisateurs</h1>
                            </div>
                        </div>
                        <div class="col-sm-6 hidden-xs">
                            <div class="header-section">
                                <ul class="breadcrumb breadcrumb-top">
                                    <li><?= SITE_NAME ?></li>
                                    <li><a href="<?= LINK . 'nouveau_employer' ?>">Liste du personnel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Table Styles Header -->

                <!-- Table Styles Block -->

                <div class="block full">
                    <?php if (isset($warnings) and !empty($warnings)) : ?>
                        <?php foreach ($warnings as $get) : ?>
                            <div class="alert alert-warning alert-dismissible message" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <div class="alert-icon">
                                    <i class="icon-info"></i>
                                </div>
                                <div class="alert-message">
                                    <span><strong>Information: </strong> <?= $get ?>.</span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (isset($success) and !empty($success)) : ?>
                        <?php foreach ($success as $get) : ?>
                            <div class="alert alert-success alert-dismissible message" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <div class="alert-icon">
                                    <i class="icon-info"></i>
                                </div>
                                <div class="alert-message">
                                    <span><strong>Information: </strong> <?= $get ?>.</span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="row">
                        <!-- User Widgets -->
                        <?php if(isset($getAllEmployes) AND !empty($getAllEmployes)):?>
                            <?php foreach($getAllEmployes as $employe):?>
                                <div class="col-lg-4 col-md-6 ">
                                <div class="widget">
                                    <div class="widget-content themed-background-flat text-right clearfix">
                                        <a href="javascript:void(0)" class="pull-left">
                                            <img src="assets/img/avatars/<?=$employe->photoPersonnels?>" alt="avatar" class="img-circle img-thumbnail img-thumbnail-avatar-2x">
                                        </a>
                                        <h3 class="widget-heading text-light"><?=$employe->prenomPersonnels.' '.$employe->nomPersonnels?></h3>
                                        <h5 class="widget-heading text-light-op"><?=$employe->roleUsers?></h5>
                                    </div>
                                    <!-- <div class="widget-content themed-background-muted text-center">
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-effect-ripple btn-danger" style="overflow: hidden; position: relative;"><i class="fa fa-share"></i> Message</a>
                                            <a href="javascript:void(0)" class="btn btn-effect-ripple btn-danger" style="overflow: hidden; position: relative;"><i class="fa fa-suitcase"></i> Note</a>
                                        </div>
                                    </div> -->
                            
                                </div>
                            </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <!-- END User Widgets -->
                    </div>
                </div>
                <!-- END Datatables Block -->
            </div>
            <!-- END Page Content -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>

<!-- END Page Wrapper -->

<?php require_once("includes/foot.inc.php"); ?>
</body>

</html>

