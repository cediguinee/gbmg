<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Nouvelle Dépense</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Nouvelle</span></li>

                        <li><span>Dépense</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire des dépenses</h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données des depenses, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">

                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Par qui  <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $nom;?>
                                        <input type="text" name="nom" value="<?=$nom;?>" class="form-control" placeholder="Ex.: Mariame Diallo" required="">
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Date <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $date;?>
                                        <input type="date" name="date" value="<?=$date;?>" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Type <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $type;?>
                                        <select name="type"  data-plugin-selectTwo class="form-control populate" class="form-control">
                                            <option value="">Veuillez séléctionner le type de dépense</option>
                                            <?php if(isset($getAllTypes) AND !empty($getAllTypes)):?>
                                                <?php foreach($getAllTypes as $allType):?>
                                                    <option value="<?=$allType->idTypes?>"><?=$allType->libelleTypes?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Quantité <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $quantite;?>
                                        <input type="number" name="quantite" id="quantite" value="<?=$quantite;?>" class="form-control" placeholder="600" required="">
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Montant <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $montant;?>
                                        <input type="number" name="montant" id="montant" value="<?=$montant;?>" class="form-control" placeholder="6000000" required="">
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Designation <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $description;?>
                                        <textarea name="description" class="form-control" id="" required="" cols="10" rows="5"><?=$description;?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Total <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $total;?>
                                        <input type="number" name="total" id="total"  value="<?=$total;?>" class="form-control" placeholder="" readonly>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button class="btn btn-primary">Valider</button>
                                        <button type="reset" class="btn btn-default">Annuler</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </form>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->

<?php require_once ("includes/admin/foot.inc.php");?>
<script>
    $(document).ready(function (){
        let quantite = document.querySelector('#quantite');
        let montant = document.querySelector('#montant');
        let total = document.querySelector('#total');
        let montantTotal = '';

        quantite.addEventListener('keyup',function (){
            montantTotal = quantite.value * montant.value ;
            //console.log("La quantité : "+quantite.value);
            //console.log("Le montant : "+montant.value);
            console.log(montantTotal);
            total.value = montantTotal;
        });
        montant.addEventListener('keyup',function (){
            montantTotal = quantite.value * montant.value ;
            //console.log("La quantité : "+quantite.value);
            //console.log("Le montant : "+montant.value);
            console.log(montantTotal);
            total.value = montantTotal;
        });

    })
</script>
</body>
</html>




