<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- title -->
    <title>Buconz - Multi-Purpose Consulting Business HTML5 Template</title>
    <!-- fav icon -->
    <link rel="icon" href="assets/images/logo/fav-icon.png">
    <meta name="keywords" content="consulting, digital consultancy, Multi-Purpose, html, shop, ecommerce, digital agency, finance, consult agency" />
    <meta name="description" content="Buconz - Multi-Purpose Consulting Business HTML5 Template">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/css/all.min.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/css/fontawesome.min.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/vendors/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/css/menu.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/css/index.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/css/menu.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/css/style.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/css/responsive.css">
</head>

<body>

    <div class="mobile-menu-area">
        <div class="off_canvars_overlay"></div>
        <div class="offcanvas_menu">
            <div class="offcanvas_menu_wrapper">
                <div class="canvas_close">
                    <a class="view-btn" href="javascript:void(0)"><i class="far fa-times-circle"></i></a>
                </div>
                <div class="mobile-logo text-center mb-30">
                    <a class="view-btn" href="home-marketing.html">
                        <img src="assets/images/logo/fav-icon.png" alt="logo">
                    </a>
                </div>
                <div id="menu" class="text-left ">
                    <ul class="offcanvas_main_menu">
                        <li><a class="nav-link" href="<?= LINK ?>home">Acceuil</a></li>
                        <li><a class="nav-link" href="#">Apropos</a></li>
                        <li><a class="nav-link" href="<?= LINK ?>formations">Formations et Services</a></li>
                        <li><a class="nav-link" href="<?= LINK ?>contact">Contact</a></li>
                        <li><a class="nav-link" href="<?= LINK ?>inscription">Inscription</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--offcanvas menu area end-->
    <!-- header area start -->
    <div class="header-area" id="myHeader">
        <div class="container">
            <div class="header">
                <div class="logo demo-logo">
                <a href="home-marketing.html"><img height="40px" width="173px" src="<?=LINK?>assets/view/logogbmg.png" alt="logo here" class="img-fluid"></a>
                </div>
                <div class="main-menu">
                    <nav>
                        <ul>
                            <li><a class="nav-link" href="<?=LINK?>home">Acceuil</a></li>
                            <li><a class="nav-link" href="#">Apropos</a></li>
                            <li><a class="nav-link" href="<?=LINK?>formations">Formations et Services</a></li>
                            <li><a class="nav-link" href="<?=LINK?>contact">Contact</a></li>
                            <li><a class="nav-link" href="<?=LINK?>inscription">Inscription</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="menu-section">
                    <!-- <a href="#" class="default-btn">Buy Now!</a> -->
                </div>

                <div class="canvas_open">
                    <a href="javascript:void(0)">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- header area ends -->

    <!-- banner section start -->
    <div class="banner-section section-padding" id="home">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-center">
                    <div class="banner-content">
                        <span class="sub-heading text-move">35 Total Pages</span>
                        <h1>Buconz - Multi-Purpose Consulting Business HTML5 Template</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- banner section start -->

    <!-- home page demo start -->
    <div class="demo-home-page top-spacing" id="demo">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-center mb-60 pos-relative">
                    <div class="section-title pos-relative">
                        <h2>NOS ACTIIVITÉS</h2>
                        <h5>Quelles images de nos activités</h5>
                        <!-- <div class="section-title-bg">
                            <img src="assets/images/demo-image/shape.svg" alt="image here">
                        </div> -->
                    </div>
                    <div class="wrapper d-flex justify-content-between">
                        <div class="cell-ball cell-ball-1"></div>
                        <div class="cell-ball cell-ball-2"></div>
                        <div class="cell-ball cell-ball-3"></div>
                        <div class="cell-ball cell-ball-4"></div>
                        <div class="cell-ball cell-ball-5"></div>
                    </div>
                </div>
            </div>
            <div class="container">
            <div class="row">
            <?php if(!empty($getAllAnnonces)):?>
                <?php foreach($getAllAnnonces as $allAnnonce):?>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="demo-box">
                        <div class="demo-thumbnail">
                            <img src="<?=LINK."assets/photos/annonces/".$allAnnonce->photoAnnonce?>" alt="image here">
                        </div>
                        <div class="demo-thumbnail-info">
                            <a class="btn-theme" href="home-marketing.html" target="_blank"><?=$allAnnonce->nameAnnonce?></a>
                        </div>
                        <div class="demo-box-meta">
                            <h4><?=$allAnnonce->libelle?></h4>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            <?php endif;?>
            </div>
            </div>
        </div>
    </div>
    <!-- home page demo ends -->

    <!-- promo section start -->
    <div class="promo-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="promo-content top-spacing">
                        <span>23</span>
                        <h2 class="text-white">Prepared Inner Pages</h2>
                        <p class="text-white">Buconz HTML templates suitable for any type Consulting business website. Its the great Consulting business HTML Template that enables you to create a professional business website.</p>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="promo-thumbs-contnet">
                        <div class="thumb">
                            <img src="assets/images/demo-image/promo-1.jpg" alt="image here" class="img-fluid">
                        </div>
                        <div class="thumb">
                            <img src="assets/images/demo-image/promo-2.jpg" alt="image here" class="img-fluid">
                        </div>
                        <div class="thumb">
                            <img src="assets/images/demo-image/promo-3.jpg" alt="image here" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- promo section Ends -->

    <!-- inner page demo start -->
  
    <!-- inner page demo ends -->

    <!-- feature-section start-->
    <div class="feature-section top-spacing" id="feature">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-center mb-60">
                    <div class="section-title pos-relative">
                        <h2>Ils nosus ont fait confiance</h2>
                        <div class="section-title-bg">
                            <img src="assets/images/demo-image/shape.svg" alt="image here">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/bootstrap.png" alt="icon here">
                        </div>
                        <h4>Built on Bootstrap (v5.x)</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/aos.png" alt="icon here">
                        </div>
                        <h4>Aos Animation</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/flaticon.png" alt="icon here">
                        </div>
                        <h4>Flaticon Images</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/font-aswome.png" alt="icon here">
                        </div>
                        <h4>Font Awesome</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/font.png" alt="icon here">
                        </div>
                        <h4>Google Fonts</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/w3c.png" alt="icon here">
                        </div>
                        <h4>Valid HTML5 & CSS Code</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/retina.png" alt="icon here">
                        </div>
                        <h4>Clean & Professional Code</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/reponsive.png" alt="icon here">
                        </div>
                        <h4>Fully Responsive Layout</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/design.png" alt="icon here">
                        </div>
                        <h4>Smooth Transition Effects</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/browser.png" alt="icon here">
                        </div>
                        <h4>Browser Compatibility</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/maps.png" alt="icon here">
                        </div>
                        <h4>Google Map</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="feature-box text-center">
                        <div class="feature-icon">
                            <img src="assets/images/icon/wrench.png" alt="icon here">
                        </div>
                        <h4>Easy to Customize</h4>
                    </div>
                </div>

            
                
            </div>
        </div>
    </div>

    <?php require_once ("includes/view/footer.inc.php");?>
<!-- footer section Ends -->
<!-- js here-->
    <a href="#" class="to-top"><i class="fas fa-arrow-up"></i></a>
    <script src="<?= LINK ?>assets/view/js/jquery.min.js"></script>
    <script src="<?= LINK ?>assets/view/vendors/js/bootstrap.bundle.js"></script>
    <script src="<?= LINK ?>assets/view/js/typeit.min.js"></script>
    <script src="<?= LINK ?>assets/view/js/mobile-menu.js"></script>
    <script src="<?= LINK ?>assets/view/js/main.js"></script>
    <script src="<?= LINK ?>assets/view/animate/js/aos.js"></script>
    <script>
        new TypeIt(".text-move", {
            speed: 200,
            loop: true,
            strings: ['Built on Bootstrap.', 'W3 Valid.', 'Easily Customizable.', 'Fully Responsive Layout.', 'Developer Friendly.', 'Well Documented.'],
            breakLines: false,
        }).go();
    </script>
</body>

</html>