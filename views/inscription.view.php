<!DOCTYPE html>
<html lang="en">
<?php require_once ("includes/view/head.inc.php");?>
<body>

<?php require_once ("includes/view/header.inc.php");?>
<?php require_once ("includes/view/mobile.inc.php");?>

<!-- banner section start -->
<div class="banner-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 p-0 text-center">
                <div class="banner-content">
                    <h1 class="text-white">Inscription</h1>
                    <span>Acceuil &#8250; Inscription</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner section Ends -->

<!-- contact-section start -->
<div class="contact-section-1 section-padding section-padding-mobile">
    <div class="container">
        <div class="row justify-content-center pb-50">
            <div class="col-xl-4 text-center">
                <span class="sub-heading bar">Inscrivez-vous</span>
                <h3>Restez informer de nous formation à venir</h3>
            </div>
        </div>
        <div class="row justify-content-between">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="message">
                    <?php if(isset($success) AND !empty($success)):?>
                        <?php foreach ($success as $info):?>
                            <div class="alert alert-success mt-2">
                                <strong>Information : </strong> <?=$info?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if(isset($warnings) AND !empty($warnings)):?>
                        <?php foreach ($warnings as $info):?>
                            <div class="alert alert-warning mt-2">
                                <strong>Avertissemnt : </strong> <?=$info?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if(isset($erreurs) AND !empty($erreurs)):?>
                        <?php foreach ($erreurs as $info):?>
                            <div class="alert alert-danger mt-2">
                                <strong>Erreur : </strong> <?=$info?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
                <div class="contact-form">
                    <h4 id="warning" style="color: #FF5E15;" class="pb-10"></h4>
                    <form action="" method="post">
                        <div class="mt-2">
                            <?php global $prenom?>
                            <input class="form-control" value="<?=$prenom?>" name="prenom" type="text" placeholder="Votre prénom" required>
                        </div>
                        <div class="mt-2">
                            <?php global $nom?>
                            <input class="form-control" value="<?=$nom?>" name="nom" type="text" placeholder="Votre nom" required>
                        </div>
                        <div class="mt-2">
                            <?php global $telephone?>
                            <input class="form-control" value="<?=$telephone?>" name="telephone" type="number" placeholder="Votre numéro" required>
                        </div>
                        <div class="mt-2">
                            <?php global $email?>
                            <input class="form-control" name="email" value="<?=$email?>" type="email" placeholder="Votre Addresse Email " required>
                        </div>
                        <div class="mt-2">
                            <select class="form-control" name="idFormation" id="">
                                <option value="">Veuillez selectionner un formation</option>
                                <?php if(!empty($getSousCategores)):?>
                                    <?php foreach ($getSousCategores as $category):?>
                                         <option value="<?=$category->idSousCat?>"><?=$category->libelle?></option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>
                        <div class="mt-2">
                            <select class="form-control" name="idModule" id="">
                                <option value="">Veuillez selectionner un module</option>
                                <?php if(!empty($getCategories)):?>
                                <?php foreach ($getCategories as $category):?>
                                    <option value="<?=$category->idCat?>"><?=$category->nomCat?></option>
                                <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>

                        <button class="b-primary mt-30" type="submit">Je m'inscris<i class="fas fa-long-arrow-alt-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- contact section Ends -->

<!-- footer-bottom -->
<?php require_once ("includes/view/footer.inc.php");?>
<!-- footer section Ends -->


<!-- js here-->
<?php require_once ("includes/view/foot.inc.php");?>


</body>
</html>
