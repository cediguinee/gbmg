<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Nouveau Vente</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Nouveau</span></li>

                        <li><span>Vente</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire de vente de produit </h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données de la vente, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">
                                <div class="row form-group pb-3">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" readonly value="<?=$reference?>" name="reference" id="" placeholder="">
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $client?>
                                            <label class="col-form-label" for="formGroupExampleInput">Clients</label>
                                            <select name="client" id="client" class="form-control">
                                                <option value="">Veuillez séléctionner le client</option>
                                                <?php if(isset($getClients) AND !empty($getClients)):?>
                                                    <?php foreach($getClients as $client):?>
                                                        <option  value="<?=$client->idClient?>"><?=$client->telephoneClient.' | '.$client->prenomClient.' '.$client->nomClient?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $nom?>
                                            <label class="col-form-label" for="formGroupExampleInput">Prénoms & Nom</label>
                                            <input type="text" class="form-control" value="<?=$nom?>" name="nom" id="nom" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $tel?>
                                            <label class="col-form-label" for="formGroupExampleInput">Contact</label>
                                            <input type="text" class="form-control" value="<?=$tel?>" name="tel" id="tel" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $adresse;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Adresse</label>
                                            <input type="text" class="form-control"  value="<?=$adresse?>" name="adresse" id="adresse" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $montant?>
                                            <label class="col-form-label" for="formGroupExampleInput">Montant (GNF)</label>
                                            <input type="number" class="form-control" value="<?=$montant?>" name="montant" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $date;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Date d'achat</label>
                                            <input type="date" class="form-control"  value="<?=$date?>" name="date" id="" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button type="submit" value="valider" name="valider" class="btn btn-primary"> <i class="fa fa-check"></i> Valider</button>
                                        <button type="reset" class="btn btn-default"><i class="fa fa-minus"></i> Annuler</button>
                                        <a class="mb-1 mt-1 me-1 modal-basic btn btn-secondary" href="#modalBasic"><i class="fa fa-plus"></i> Ajouter produit</a>
                                        <a class="mb-1 mt-1 me-1 modal-basic btn btn-primary" href="#modalfournisseur"><i class="fa fa-users"></i> Ajouter d'un client</a>
                                    </div>
                                </div>
                            </footer>
                        </section>
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Liste des produits séléctionnés pour la vente</h2>
                            </header>
                            <div class="card-body">
                                <table>
                                    <thead>
                                    <tr role="row">
                                        <th >#</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable-editable" rowspan="1" colspan="1" aria-label="Description : activate to sort column ascending" style="width: 408px;">Description</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable-editable" rowspan="1" colspan="1" aria-label="Quantité : activate to sort column ascending" style="width: 208px;">Quantité</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable-editable" rowspan="1" colspan="1" aria-label="Montant  : activate to sort column ascending" style="width: 208px;">Montant</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable-editable" rowspan="1" colspan="1" aria-label="Montant Total : activate to sort column ascending" style="width: 208px;">Montant Total</th>
                                        <th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Actions" style="width: 65.8594px;">Actions</th></tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $increment = 1;
                                    $montantTotal = 0;
                                    if(isset($getPanier) AND !empty($getPanier)): ?>
                                        <?php foreach ($getPanier as $item):?>
                                            <?php
                                            $montantTotal += $item->total;?>
                                            <tr data-item-id="44" role="row" class="odd">
                                                <td class="sorting_1"><?=$increment++ ?></td>
                                                <td><?=$item->libelleProduits?></td>
                                                <td><?=number_format($item->quantite)?></td>
                                                <td><?=number_format($item->prix).' GNF'?></td>
                                                <td><?=number_format($item->total).' GNF'?></td>
                                                <td class="actions">
                                                    <a href="" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="<?=LINK.'nouvelle_vente/'.$item->idPaniers?>" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    <tfooter>
                                        <tr>
                                            <td> <h4>Total :</h4> </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><h4><?=number_format($montantTotal).' GNF'?></h4></td>
                                            <input type="text" hidden name="totalPaye" value="<?=$montantTotal?>">
                                        </tr>
                                    </tfooter>
                                    </tbody>
                                </table>
                            </div>
                </div>
        </section>
        </form>
        <!-- Modal Basic -->
        <a class="mb-1 mt-1 me-1 modal-basic btn btn-default" href="#modalBasic">Produit</a>

        <div id="modalBasic" class="modal-block mfp-hide">
            <form action="" method="post">
                <section class="card">
                    <header class="card-header">
                        <h2 class="card-title">Séléctionner un produit</h2>
                    </header>
                    <div class="card-body">
                        <div class="modal-wrapper">
                            <div class="modal-text">
                                <div class="row form-group pb-3">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $produit;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Libélle</label>
                                            <select name="produit" id="produit" class="form-control" class="form-control">
                                                <option value="">Veuillez séléctionner le produit</option>
                                                <?php if(isset($getProduits) AND !empty($getProduits)):?>
                                                    <?php foreach($getProduits as $produit):?>
                                                        <option  value="<?=$produit->idProduits?>"><?=$produit->libelleProduits?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $quantite?>
                                            <label class="col-form-label" for="formGroupExampleInput">Quantité</label>
                                            <input type="number" class="form-control" value="<?=$quantite?>" name="quantite" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $prixachat;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Prix d'achat</label>
                                            <input type="number" class="form-control"  value="<?=$prixachat?>" name="prixachat" id="prixachat" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $prixvente?>
                                            <label class="col-form-label" for="formGroupExampleInput">Prix de vente</label>
                                            <input type="number" class="form-control" value="<?=$prixvente?>" name="prixvente" id="prixvente" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="card-footer">
                        <div class="row">
                            <div class="col-md-12 text-end">
                                <button type="submit" name="panier" value="panier" class="btn btn-outline-success">Ajouter</button>
                                <button class="btn btn-outline-warning modal-dismiss">Annuler</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </form>
        </div>
    </div>
    <!-- col-lg-6 -->
    <a class="mb-1 mt-1 me-1 modal-basic btn btn-default" href="#modalfournisseur">Client</a>
    <div id="modalfournisseur" class="modal-block mfp-hide">
        <form action="" method="post">
            <section class="card">
                <header class="card-header">
                    <h2 class="card-title">Veuillez saisir les données du client</h2>
                </header>
                <div class="card-body">
                    <div class="modal-wrapper">
                        <div class="modal-text">
                            <div class="row form-group pb-3">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?php global $prenom?>
                                        <label class="col-form-label" for="formGroupExampleInput">Prénoms</label>
                                        <input type="text" class="form-control" value="<?=$prenom?>" name="prenom" id="formGroupExampleInput" placeholder="">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?php global $nom?>
                                        <label class="col-form-label" for="formGroupExampleInput">Nom</label>
                                        <input type="text" class="form-control" value="<?=$nom?>" name="nom" id="formGroupExampleInput" placeholder="">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?php global $tel?>
                                        <label class="col-form-label" for="formGroupExampleInput">Contact</label>
                                        <input type="text" class="form-control" value="<?=$tel?>" name="tel" id="formGroupExampleInput" placeholder="">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?php global $adresse;?>
                                        <label class="col-form-label" for="formGroupExampleInput">Adresse</label>
                                        <input type="text" class="form-control"  value="<?=$adresse?>" name="adresse" id="formGroupExampleInput" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-end">
                            <button type="submit" name="fournisseur" value="fournisseur" class="btn btn-outline-success">Ajouter</button>
                            <button class="btn btn-outline-warning modal-dismiss">Annuler</button>
                        </div>
                    </div>
                </footer>
            </section>
        </form>
    </div>
    </div>
    </div>

    <!-- end: page -->
</section>
</div>

<?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>
<script src="<?=LINK?>api/ajax/ventes.js?v=<?=uniqid()?>"></script>

</body>
</html>






