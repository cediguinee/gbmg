<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Liste des Payement Employés</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Liste</span></li>

                        <li><span>Payement</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                            </div>

                            <h2 class="card-title">Liste des Payements Employés</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Photo</th>
                                    <th>Prénoms & Nom</th>
                                    <th>Fonction</th>
                                    <th>Profession</th>
                                    <th>Salaire</th>
                                    <th>Mois & Année</th>
                                    <th>Actions(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $increment = 1;
                                if(isset($getAllEmployesPayement) AND !empty($getAllEmployesPayement)): ?>
                                    <?php foreach ($getAllEmployesPayement as $item):?>
                                        <tr data-item-id="44" role="row" class="odd">
                                            <td class="sorting_1"><?=$increment++ ?></td>
                                            <td>
                                                <img height="50px" width="50px" style="border-radius: 25px"  src="<?=LINK."assets/photos/avatars/".$item->photoPersonnels?>" alt="">
                                            </td>
                                            <td><?=$item->prenomPersonnels.' '.$item->nomPersonnels?></td>
                                            <td><?=$item->fonctionPersonnels?></td>
                                            <td><?=$item->professionPersonnels?></td>
                                            <td><?=number_format($item->montantPayes).' GNF'?></td>
                                            <td><?php
                                                if($item->moisPayes=='01'){
                                                    echo "Janvier-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='02'){
                                                    echo "Février-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='03'){
                                                    echo "Marc-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='04'){
                                                    echo "Avril-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='05'){
                                                    echo "Mai-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='06'){
                                                    echo "Juin-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='07'){
                                                    echo "Juillet-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='08'){
                                                    echo "Août-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='09'){
                                                    echo "Septembre-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='10'){
                                                    echo "Octombre-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='11'){
                                                    echo "Novembre-{$item->anneePayes}";
                                                }
                                                if($item->moisPayes=='12'){
                                                    echo "Décembre-{$item->anneePayes}";
                                                }
                                                ?></td>
                                            <td class="actions">
                                                <a href="<?=LINK.'edit_employe/'.$item->idPayes?>" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                                <?php if(isset($_SESSION['gbmg']['role']) AND $_SESSION['gbmg']['role']=="Administrateur"):?>
                                                    <a href="<?=LINK.'liste_des_payements_employes/'.$item->idPayes?>" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                                <?php endif;?>

                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>





