<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Nouveau Client</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Nouveau</span></li>

                        <li><span>Client</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(!empty($erreursM)):?>
                            <div class="alert alert-danger ">
                                <strong>Erreur : </strong> <?=$erreursM?>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($warningsM)):?>
                            <div class="alert alert-warning ">
                                <strong>Erreur : </strong> <?=$warningsM?>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($successM)):?>
                            <div class="alert alert-success ">
                                <strong>Erreur : </strong> <?=$successM?>
                            </div>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire de recherche des clients</h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données du client, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">
                                <div class="row form-group pb-3">


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-form-label" for="formGroupExampleInput">Module</label>
                                            <?php global $idModule;?>
                                            <select name="idModule" value="<?=$idModule;?>" name="idModule" id="" data-plugin-selectTwo class="form-control populate" class="form-control">
                                                <option value="">Veuillez séléctionner le choix le module</option>
                                                <?php if(isset($getCategories) AND !empty($getCategories)):?>
                                                    <?php foreach ($getCategories as $category):?>
                                                        <option <?=($category->idCat==$idModule)?'selected':''?> value="<?=$category->idCat?>"><?=$category->nomCat?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-form-label" for="formGroupExampleInput">Formation</label>
                                            <?php global $idFormation;?>
                                            <select name="idFormation" value="<?=$idModule;?>" name="idModule" id="" data-plugin-selectTwo class="form-control populate" class="form-control">
                                                <option value="">Veuillez séléctionner le choix</option>
                                                <?php if(isset($getSousCategores) AND !empty($getSousCategores)):?>
                                                    <?php foreach ($getSousCategores as $getSousCategore):?>
                                                        <option <?=($getSousCategore->idSousCat==$idFormation)?'selected':''?> value="<?=$getSousCategore->idSousCat?>"><?=$getSousCategore->libelle?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button class="btn btn-primary"> <i class="fa fa-search"></i> Rechercher</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </form>
                </div>
                <!-- col-lg-6 -->
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>

                        <h2 class="card-title">Liste des Clients (Vous avez <?=(!empty($getClients)?count($getClients):0)?> clients)</h2>
                    </header>
                    <div class="card-body">
                        <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Matricule</th>
                                <th>Prénoms et Nom</th>
                                <th>Téléphone</th>
                                <th>Adresse</th>
                                <th>Formation</th>
                                <th>Module</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $increment = 1;
                            if(isset($getClients) AND !empty($getClients)): ?>
                                <?php foreach ($getClients as $item):?>
                                    <tr>
                                        <td><?=$increment++ ?></td>
                                        <td><?=ucfirst($item->matriculeParticipants)?></td>
                                        <td><?=$item->civiliteParticipants." ".ucfirst($item->prenomParticipants)." ".strtoupper($item->nomParticipants)?></td>
                                        <td><?=ucfirst($item->contactParticipants)?> </td>
                                        <td><?=ucfirst($item->adresseParticipants)?></td>
                                        <td><?=ucfirst($item->libelle)?> </td>
                                        <td><?=ucfirst($item->nomCat)?> </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>


</body>
</html>




