<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Liste des soldes fournisseurs</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Liste</span></li>

                        <li><span>Soldes</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                            </div>

                            <h2 class="card-title">Liste des soldes</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Prénoms & Nom</th>
                                    <th>Téléphone</th>
                                    <th>Adresse</th>
                                    <th>Solde</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $increment = 1;
                                if(isset($getClients) AND !empty($getClients)): ?>
                                    <?php foreach ($getClients as $items):?>
                                        <tr data-item-id="44" role="row" class="odd">
                                            <td class="sorting_1"><?=$increment++ ?></td>
                                            <td><?=$items->prenomFournisseurs.' '.$items->nomFournisseurs?></td>
                                            <td><?=$items->telephoneFournisseurs?></td>
                                            <td><?=$items->adresseFournisseurs?></td>
                                            <td>
                                                <?php

                                                $getReglements = \models\ReglementFournisseurs::getReglementById($items->idFournisseurs);
                                                $getVente = \models\Achats::getIdFournisseurById($items->idFournisseurs);
                                                global $montantpaye;
                                                global $montantrestant;
                                                global $montanttotal;
                                                global $montantregle;
                                                global $reste;

                                                $montantpaye = 0;
                                                $montantrestant = 0;
                                                $montanttotal = 0;
                                                $montantregle = 0;
                                                $reste = 0;
                                                foreach ($getVente as $vente):
                                                    $montantpaye += $vente->montantpaye;
                                                    $montantrestant += $vente->restepaye;
                                                    $montanttotal += $vente->total;
                                                endforeach;

                                                foreach ($getReglements as $reglement):
                                                    $montantregle += $reglement->montantReglements;
                                                endforeach;
                                                $data = $montanttotal-($montantpaye+$montantregle);
                                                echo number_format($data).' GNF';
                                                ?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>






