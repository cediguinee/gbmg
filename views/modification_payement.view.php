<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Modification Payement</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Modification</span></li>

                        <li><span>Payement</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire de modification de payement</h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données de payement, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">
                                <div class="row form-group pb-3">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <?php global $matricule;?>
                                            <input type="text" name="idPayements" hidden value="<?=$idPayements?>">
                                            <label class="col-form-label" for="formGroupExampleInput">Matricule</label>
                                            <select name="matricule" id="" class="form-control">
                                                <option value="">Veuillez séléctionner le matricule</option>
                                                <?php if(!empty($getParticipants)):?>
                                                    <?php foreach($getParticipants as $participant):?>
                                                        <option <?=($matricule==$participant->matriculeParticipants)?'selected':'';?> value="<?=$participant->matriculeParticipants?>"><?=$participant->matriculeParticipants.' | '.$participant->prenomParticipants.' '.$participant->nomParticipants.' | '.$participant->contactParticipants?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-form-label" for="formGroupExampleInput">Module</label>
                                            <?php global $idModule;?>
                                            <select name="idModule" value="<?=$idModule;?>" name="idModule" id="" class="form-control">
                                                <option value="">Veuillez séléctionner le choix le module</option>
                                                <?php if(isset($getCategories) AND !empty($getCategories)):?>
                                                    <?php foreach ($getCategories as $category):?>
                                                        <option <?=($category->idCat==$idModule)?'selected':''?> value="<?=$category->idCat?>"><?=$category->nomCat?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-form-label" for="formGroupExampleInput">Formation</label>
                                            <?php global $idFormation;?>
                                            <select name="idFormation" value="<?=$idModule;?>" name="idModule" id="" class="form-control">
                                                <option value="">Veuillez séléctionner le choix</option>
                                                <?php if(isset($getSousCategores) AND !empty($getSousCategores)):?>
                                                    <?php foreach ($getSousCategores as $getSousCategore):?>
                                                        <option <?=($getSousCategore->idSousCat==$idFormation)?'selected':''?> value="<?=$getSousCategore->idSousCat?>"><?=$getSousCategore->libelle?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $montant;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Montant</label>
                                            <input type="number" class="form-control" value="<?=$montant?>" name="montant" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $avance;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Avance</label>
                                            <input type="number" class="form-control" value="<?=$avance?>" name="avance" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button class="btn btn-primary">Valider</button>
                                        <button type="reset" class="btn btn-default">Annuler</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </form>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>




