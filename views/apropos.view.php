<!DOCTYPE html>
<html lang="en">
<?php require_once ("includes/view/head.inc.php");?>
<body>

<?php require_once ("includes/view/header.inc.php");?>
<?php require_once ("includes/view/mobile.inc.php");?>

<!-- banner section start -->
<div class="banner-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 p-0 text-center">
                <div class="banner-content">
                    <h1 class="text-white" data-aos="fade-right">Apropos de nous</h1>
                    <span data-aos="fade-left">Acceuil &#187; Apropos de nous</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner section Ends -->

<!-- about us section start -->
<div class="aboutus-section section-padding">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-xl-6 col-lg-6">
                <div class="aboutus-content pt-40 pt-md-0" data-aos="fade-right">
                    <span class="sub-heading bar pb-10">Apropos de nous</span>
                    <h3 class="tittle pb-20">One Of The Fastest Way To Gain Business Success</h3>
                    <p class="text pb-10">Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                        aliquyam erat, sed diam voluptua. At vero eos et accusam et justo
                        duo dolores et ea rebum.</p>
                    <p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                        ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                        eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                        gubergren, no sea takimata sanctus.
                    </p>
                </div>
                <div class="aboutus-feature d-flex flex-wrap align-items-start justify-content-start">
                    <div class="feature-item feature-item-style-1 text-center">
                        <div class="feature-box">
                            <div class="icon">
                                <img src="<?=LINK?>assets/images/icon/consistency.svg" alt="icon here" class="img-fluid">
                            </div>
                            <h4>Consistency</h4>
                        </div>
                    </div>
                    <div class="feature-item feature-item-style-1 text-center">
                        <div class="feature-box">
                            <div class="icon">
                                <img src="<?=LINK?>assets/images/icon/consistency-1.svg" alt="icon here" class="img-fluid">
                            </div>
                            <h4>Strategy</h4>
                        </div>
                    </div>
                    <div class="feature-item feature-item-style-1 text-center">
                        <div class="feature-box">
                            <div class="icon">
                                <img src="<?=LINK?>assets/images/icon/consistency-2.svg" alt="icon here" class="img-fluid">
                            </div>
                            <h4>Investment</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6" data-aos="fade-left">
                <div class="aboutus-thumb mt-5 mt-lg-0">
                    <img src="<?=LINK?>assets/images/aboutus/image-1.png" alt="image here" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about us section Ends -->

<!-- work section start  -->
<div class="work-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-md-6 col-lg-3 col-sm-6">
                <div class="counter-box counter-box-style-1 mb-sm-4">
                    <h1 class="counter">950</h1>
                    <span>Satisfied Clients</span>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-lg-3 col-sm-6">
                <div class="counter-box counter-box-style-1 mb-sm-4">
                    <h1 class="counter">250</h1>
                    <span>Expert Member</span>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-lg-3 col-sm-6">
                <div class="counter-box counter-box-style-1">
                    <h1 class="counter">890</h1>
                    <span>Active Project</span>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-lg-3 col-sm-6">
                <div class="counter-box counter-box-style-1">
                    <h1 class="counter">360</h1>
                    <span>Winning Award</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- work section Ends  -->

<!-- feature section start -->
<div class="feature-section top-spacing">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6" data-aos="fade-right">
                <div class="feature-thumb">
                    <img src="<?=LINK?>assets/images/feature/feature-4.png" alt="image here" class="img-fluid">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6" data-aos="fade-left">
                <div class="feature-content pb-30 mt-5 mt-lg-0">
                    <span class="sub-heading bar pb-10">What We Do?</span>
                    <h3 class="tittle pb-20">What Will You Get From Our Dedicated Company</h3>
                    <p>Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                        aliquyam erat, sed diam voluptua. At vero eos et accusam et justo
                        duo dolores et ea rebum.
                    </p>
                </div>
                <div class="feature-single pb-10">
                    <h4><span>01</span> Consistency</h4>
                    <p>Consistency sadipscing elitr, sed diam nonumy eirmod tempor
                        invidunt ut labore et dolore magna aliquyam erat.
                    </p>
                </div>
                <div class="feature-single pb-10">
                    <h4><span>02</span> Strategy</h4>
                    <p>Consistency sadipscing elitr, sed diam nonumy eirmod tempor
                        invidunt ut labore et dolore magna aliquyam erat.
                    </p>
                </div>
                <div class="feature-single">
                    <h4><span>03</span> Final Output</h4>
                    <p>Consistency sadipscing elitr, sed diam nonumy eirmod tempor
                        invidunt ut labore et dolore magna aliquyam erat.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- feature section start -->

<!-- barnd section start  -->
<div class="brand-section section-padding" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="slider-5">
                <div class="brand-item">
                    <img src="<?=LINK?>assets/images/brand/1.png" alt="image here" class="img-fluid main-logo">
                    <img src="<?=LINK?>assets/images/brand/1.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="<?=LINK?>assets/images/brand/2.png" alt="image here" class="img-fluid main-logo">
                    <img src="<?=LINK?>assets/images/brand/2.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="<?=LINK?>assets/images/brand/3.png" alt="image here" class="img-fluid main-logo">
                    <img src="<?=LINK?>assets/images/brand/3.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="<?=LINK?>assets/images/brand/5.png" alt="image here" class="img-fluid main-logo">
                    <img src="<?=LINK?>assets/images/brand/5.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="<?=LINK?>assets/images/brand/6.png" alt="image here" class="img-fluid main-logo">
                    <img src="<?=LINK?>assets/images/brand/6.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="<?=LINK?>assets/images/brand/3.png" alt="image here" class="img-fluid main-logo">
                    <img src="<?=LINK?>assets/images/brand/3.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="<?=LINK?>assets/images/brand/1.png" alt="image here" class="img-fluid main-logo">
                    <img src="<?=LINK?>assets/images/brand/1.png" alt="image here" class="img-fluid hover-logo">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- barnd section Ends -->

<!-- footer-bottom -->
<?php require_once ("includes/view/footer.inc.php");?>
<!-- footer section Ends -->


<!-- js here-->
<?php require_once ("includes/view/foot.inc.php");?>


<!-- js here-->



</body>
</html>
