<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Liste des Produits</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Liste</span></li>

                        <li><span>Produits</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                            </div>

                            <h2 class="card-title">Liste des Produits</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Libelle</th>
                                    <th>Prix d'achat</th>
                                    <th>Prix de vente</th>
                                    <th>Date</th>
                                    <th>Actions(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $increment = 1;
                                if(isset($getAllProduits) AND !empty($getAllProduits)): ?>
                                    <?php foreach ($getAllProduits as $item):?>
                                        <tr data-item-id="44" role="row" class="odd">
                                            <td class="sorting_1"><?=$increment++ ?></td>
                                            <td><?=$item->libelleProduits?></td>
                                            <td><?=number_format($item->prixAchatProduits).' GNF'?></td>
                                            <td><?=number_format($item->prixDeVenteProduits).' GNF'?></td>
                                            <td><?=date_format(date_create($item->dateProduits),'d/m/Y')?></td>
                                            <td class="actions">
                                                <a href="<?=LINK.'modification_produit/'.$item->idProduits?>" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                                <?php if(isset($_SESSION['gbmg']['role']) AND $_SESSION['gbmg']['role']=="Administrateur"):?>
                                                    <a href="<?=LINK.'liste_des_produits/'.$item->idProduits?>" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                                <?php endif;?>

                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>




