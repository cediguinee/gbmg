<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Liste des Clients</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Liste</span></li>

                        <li><span>Clients</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                            </div>

                            <h2 class="card-title">Liste des Clients</h2>
                        </header>
                        <div class="card-body">
                            <div id="datatable-editable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer"><div class="row"><div class="col-lg-6"><div class="dataTables_length" id="datatable-editable_length"><label><select name="datatable-editable_length" aria-controls="datatable-editable" class="form-select form-select-sm select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true"><option value="10" data-select2-id="3">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select><span class="select2 select2-container select2-container--bootstrap" dir="ltr" data-select2-id="2" style="width: 61px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-datatable-editable_length-lo-container"><span class="select2-selection__rendered" id="select2-datatable-editable_length-lo-container" role="textbox" aria-readonly="true" title="10">10</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span> records per page</label></div></div><div class="col-lg-6"><div id="datatable-editable_filter" class="dataTables_filter"><label><input type="search" class="form-control pull-right" placeholder="Search..." aria-controls="datatable-editable"></label></div></div></div><div class="table-responsive"><table class="table table-bordered table-striped mb-0 dataTable no-footer" id="datatable-editable" role="grid">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Prénom</th>
                                            <th>Nom</th>
                                            <th>Téléphone</th>
                                            <th>Adresse</th>
                                            <th>Actions(s)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $increment = 1;
                                        if(isset($getAllClients) AND !empty($getAllClients)): ?>
                                            <?php foreach ($getAllClients as $item):?>
                                                <tr>
                                                    <td class="sorting_1"><?=$increment++ ?></td>
                                                    <td><?=$item->prenomClient?></td>
                                                    <td><?=$item->nomClient?></td>
                                                    <td><?=$item->telephoneClient?></td>
                                                    <td><?=$item->adresseClient?></td>
                                                    <td class="actions">
                                                        <a href="<?=LINK.'edit_client/'.$item->idClient?>" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                                        <?php if(isset($_SESSION['gbmg']['role']) AND $_SESSION['gbmg']['role']=="Administrateur"):?>
                                                            <a href="<?=LINK.'list_client/'.$item->idClient?>" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                                        <?php endif;?>
                                                </tr>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>




