<!DOCTYPE html>
<html lang="en">
<?php require_once ("includes/view/head.inc.php");?>
<body>

<?php require_once ("includes/view/header.inc.php");?>
<?php require_once ("includes/view/mobile.inc.php");?>

<!-- banner section start -->
<div class="banner-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 p-0 text-center">
                <div class="banner-content">
                    <h1 class="text-white" data-aos="fade-right">Formations et Services</h1>
                    <span data-aos="fade-left">Acceuil &#187; Formations & Services</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner section Ends -->

<!-- service section start -->
<div class="service-section section-padding section-padding-mobile">
    <div class="container">
        <div class="row justify-content-center" data-aos="fade-up">
            <div class="col-xl-6 text-center">
                <span class="sub-heading bar pb-10">Nos Services</span>
                <h3 class="pb-30">Formations et Services</h3>
            </div>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-xl-4 col-lg-4 col-md-6 p-0">
                <div class="service-box text-center">
                    <div class="service-icon">
                        <i class="flaticon-money"></i>
                    </div>
                    <div class="service-desc">
                        <h4><a href="service-single.html" class="text-color-blue">Insurance Consulting</a></h4>
                        <p class="text text-center pb-20">Sed diam nonumy eirmod tempor Invidunt
                            ut labore et dolore magna Aliquyam
                            erat, sed diam voluptua.</p>
                        <a href="service-single.html" class="btn-view">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 p-0">
                <div class="service-box text-center">
                    <div class="service-icon">
                        <i class="flaticon-branding"></i>
                    </div>
                    <div class="service-desc">
                        <h4><a href="service-single.html" class="text-color-blue">Financial Consulting</a></h4>
                        <p class="text text-center pb-20">Sed diam nonumy eirmod tempor Invidunt
                            ut labore et dolore magna Aliquyam
                            erat, sed diam voluptua.</p>
                        <a href="service-single.html" class="btn-view">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 p-0">
                <div class="service-box text-center">
                    <div class="service-icon">
                        <i class="flaticon-financial-profit"></i>
                    </div>
                    <div class="service-desc">
                        <h4><a href="service-single.html" class="text-color-blue">Investment Tranding</a></h4>
                        <p class="text text-center pb-20">Sed diam nonumy eirmod tempor Invidunt
                            ut labore et dolore magna Aliquyam
                            erat, sed diam voluptua.</p>
                        <a href="service-single.html" class="btn-view">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 p-0">
                <div class="service-box text-center">
                    <div class="service-icon">
                        <i class="flaticon-gear"></i>
                    </div>
                    <div class="service-desc">
                        <h4><a href="service-single.html" class="text-color-blue">Financial Analysis</a></h4>
                        <p class="text text-center pb-20">Sed diam nonumy eirmod tempor Invidunt
                            ut labore et dolore magna Aliquyam
                            erat, sed diam voluptua.</p>
                        <a href="service-single.html" class="btn-view">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 p-0">
                <div class="service-box text-center">
                    <div class="service-icon">
                        <i class="flaticon-taxation"></i>
                    </div>
                    <div class="service-desc">
                        <h4><a href="service-single.html" class="text-color-blue">Taxation Planning</a></h4>
                        <p class="text text-center pb-20">Sed diam nonumy eirmod tempor Invidunt
                            ut labore et dolore magna Aliquyam
                            erat, sed diam voluptua.</p>
                        <a href="service-single.html" class="btn-view">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 p-0">
                <div class="service-box text-center">
                    <div class="service-icon">
                        <i class="flaticon-innovation"></i>
                    </div>
                    <div class="service-desc">
                        <h4><a href="service-single.html" class="text-color-blue">Manufacturing Plants</a></h4>
                        <p class="text text-center pb-20">Sed diam nonumy eirmod tempor Invidunt
                            ut labore et dolore magna Aliquyam
                            erat, sed diam voluptua.</p>
                        <a href="service-single.html" class="btn-view">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- service section Ends -->

<!-- service contact start -->
<div class="service-contact pt-5 pt-lg-0">
    <div class="container">
        <div class="row d-flex align-items-center justify-content-between">
            <div class="col-xl-7 col-lg-7">
                <div class="contact-cocntent float-lg-end mb-5 mb-lg-0">
                    <span class="pb-10">Do You Need Any Consulting Service?</span>
                    <h3 class="pb-40">Call Us Our Consultants Will Assist You</h3>
                    <a class="btn-call" href="tel:+1(123)456789"><i class="fas fa-phone-alt"></i>(800) 123-6789</a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="contact-thumb">
                    <img src="assets/images/aboutus/contact.png" alt="image here" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- service contact Ends -->

<!-- barnd section start  -->
<div class="brand-section section-padding" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="slider-5">
                <div class="brand-item">
                    <img src="assets/images/brand/1.png" alt="image here" class="img-fluid main-logo">
                    <img src="assets/images/brand/1.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="assets/images/brand/2.png" alt="image here" class="img-fluid main-logo">
                    <img src="assets/images/brand/2.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="assets/images/brand/3.png" alt="image here" class="img-fluid main-logo">
                    <img src="assets/images/brand/3.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="assets/images/brand/5.png" alt="image here" class="img-fluid main-logo">
                    <img src="assets/images/brand/5.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="assets/images/brand/6.png" alt="image here" class="img-fluid main-logo">
                    <img src="assets/images/brand/6.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="assets/images/brand/3.png" alt="image here" class="img-fluid main-logo">
                    <img src="assets/images/brand/3.png" alt="image here" class="img-fluid hover-logo">
                </div>
                <div class="brand-item">
                    <img src="assets/images/brand/1.png" alt="image here" class="img-fluid main-logo">
                    <img src="assets/images/brand/1.png" alt="image here" class="img-fluid hover-logo">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- footer-bottom -->
<?php require_once ("includes/view/footer.inc.php");?>
<!-- footer section Ends -->


<!-- js here-->
<?php require_once ("includes/view/foot.inc.php");?>


<!-- js here-->





</body>
</html>
