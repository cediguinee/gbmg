<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<!-- start: page -->
<section class="body-error error-outside">
    <div class="center-error">

        <div class="row">
            <div class="col-md-8">
                <div class="main-error mb-3">
                    <h2 class="error-code text-dark text-center font-weight-semibold m-0">404 <i class="fas fa-file"></i></h2>
                    <p class="error-explanation text-center">Erreur la page que vous demander n'existe pas.</p>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="text">Clicker sur autre lien</h4>
                <ul class="nav nav-list flex-column primary">
                    <li class="nav-item">
                        <a class="nav-link" href="<?=LINK.'tableau_de_bord'?>"><i class="fas fa-caret-right text-dark"></i> Tableau de bord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=LINK.'profil'?>""><i class="fas fa-caret-right text-dark"></i> Votre Profil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=LINK.'home'?>""><i class="fas fa-caret-right text-dark"></i> Site</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>