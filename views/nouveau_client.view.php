<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Nouveau Client</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Nouveau</span></li>

                        <li><span>Client</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(!empty($erreursM)):?>
                            <div class="alert alert-danger ">
                                <strong>Erreur : </strong> <?=$erreursM?>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($warningsM)):?>
                            <div class="alert alert-warning ">
                                <strong>Erreur : </strong> <?=$warningsM?>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($successM)):?>
                            <div class="alert alert-success ">
                                <strong>Erreur : </strong> <?=$successM?>
                            </div>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire de inscription client</h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données du client, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">
                                <div class="row form-group pb-3">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $matricule;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Matricule</label>
                                            <input type="text" class="form-control"  readonly value="<?=$matricule?>" name="matricule" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $sexe?>
                                            <label class="col-form-label" for="formGroupExampleInput">Civilité</label>
                                            <select name="sexe" id="" class="form-control">
                                                <option value="">Veuillez séléctionner la civilité</option>
                                                <option <?=($sexe=="M.")?'selected':''?> value="M.">M.</option>
                                                <option <?=($sexe=="Mme")?'selected':''?>  value="Mme">Mme</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $prenom?>
                                            <label class="col-form-label" for="formGroupExampleInput">Prénoms</label>
                                            <input type="text" class="form-control" value="<?=$prenom?>" name="prenom" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $nom?>
                                            <label class="col-form-label" for="formGroupExampleInput">Nom</label>
                                            <input type="text" class="form-control" value="<?=$nom?>" name="nom" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $tel?>
                                            <label class="col-form-label" for="formGroupExampleInput">Contact</label>
                                            <input type="text" class="form-control" value="<?=$tel?>" name="tel" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $adresse?>
                                            <label class="col-form-label" for="formGroupExampleInput">Adresse</label>
                                            <input type="text" class="form-control" value="<?=$adresse?>" name="adresse" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $datenaissance?>
                                            <label class="col-form-label" for="formGroupExampleInput">Date Naissance</label>
                                            <input type="date" class="form-control" value="<?=$datenaissance?>" name="datenaissance" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $profession?>
                                            <label class="col-form-label" for="formGroupExampleInput">Profession</label>
                                            <input type="text" class="form-control" value="<?=$profession?>" name="profession" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $residence;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Résidence</label>
                                            <input type="text" class="form-control" value="<?=$residence?>" name="residence" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $ville;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Ville</label>
                                            <input type="text" class="form-control" value="<?=$ville?>" name="ville" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $pays;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Pays</label>
                                            <input type="text" class="form-control" value="<?=$pays?>" name="pays" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $langue;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Langue Parlée</label>
                                            <input type="text" class="form-control" value="<?=$langue?>" name="langue" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $personneressource;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Par qui :</label>
                                            <select name="personneressource" id="personneressource" class="form-control">
                                                <option value="">Veuillez séléctionner sont canal d'arrivé</option>
                                                <option <?=($personneressource=='Partenaire')?'selected':'';?> value="Partenaire">Partenaire</option>
                                                <option <?=($personneressource=='Entreprise')?'selected':'';?>  value="Entreprise">Entreprise</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $partenaire;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Téléphone du Partenaire</label>
                                            <input type="number" class="form-control" value="<?=$partenaire?>" name="partenaire"  id="partenaire" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-form-label" for="formGroupExampleInput">Module</label>
                                            <?php global $idModule;?>
                                            <select name="idModule" value="<?=$idModule;?>" name="idModule" id="" data-plugin-selectTwo class="form-control populate" class="form-control">
                                                <option value="">Veuillez séléctionner le choix le module</option>
                                                <?php if(isset($getCategories) AND !empty($getCategories)):?>
                                                    <?php foreach ($getCategories as $category):?>
                                                        <option <?=($category->idCat==$idModule)?'selected':''?> value="<?=$category->idCat?>"><?=$category->nomCat?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-form-label" for="formGroupExampleInput">Formation</label>
                                            <?php global $idFormation;?>
                                            <select name="idFormation" value="<?=$idModule;?>" name="idModule" id="" data-plugin-selectTwo class="form-control populate" class="form-control">
                                                <option value="">Veuillez séléctionner le choix</option>
                                                <?php if(isset($getSousCategores) AND !empty($getSousCategores)):?>
                                                    <?php foreach ($getSousCategores as $getSousCategore):?>
                                                        <option <?=($getSousCategore->idSousCat==$idFormation)?'selected':''?> value="<?=$getSousCategore->idSousCat?>"><?=$getSousCategore->libelle?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $montant;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Montant</label>
                                            <input type="number" class="form-control" value="<?=$montant?>" name="montant" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $avance;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Avance</label>
                                            <input type="number" class="form-control" value="<?=$avance?>" name="avance" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button class="btn btn-primary">Valider</button>
                                        <button type="reset" class="btn btn-default">Annuler</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </form>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>
<script>
    $(document).ready(function (){
       let personneressource = document.querySelector("#personneressource");
       let partenaire = document.querySelector("#partenaire");
        personneressource.addEventListener("change",function(){
            if(personneressource.value=="Entreprise"){
                partenaire.value="623535956";
            }else{
                partenaire.value="";
            }
        });
    })
</script>

</body>
</html>



