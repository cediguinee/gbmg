<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Nouveau Client</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Payement</span></li>

                        <li><span>Fornisseur</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($succes) AND !empty($succes)):?>
                            <div class="alert alert-success ">
                                <strong>Information : </strong> <?=$succes?>
                            </div>
                        <?php endif;?>
                        <?php if(isset($warning) AND !empty($warning)):?>
                            <div class="alert alert-warning ">
                                <strong>Avertissemnt : </strong> <?=$warning?>
                            </div>
                        <?php endif;?>
                        <?php if(isset($erreur) AND !empty($erreur)):?>
                            <div class="alert alert-danger ">
                                <strong>Erreur : </strong> <?=$erreur?>
                            </div>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire de payement fournisseur</h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données de payement, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">
                                <div class="row form-group pb-3">

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $idFournisseurs?>
                                            <label class="col-form-label" for="formGroupExampleInput">Prénom et Nom</label>
                                            <select name="idFournisseurs" id="idFournisseurs" data-plugin-selectTwo class="form-control populate" class="form-control">
                                                <option value="">Veuillez séléctionner le fournisseur</option>
                                                <?php if(!empty($getFournisseurs)):?>
                                                    <?php foreach($getFournisseurs as $fournisseur):?>
                                                        <option <?=($idFournisseurs==$fournisseur->idFournisseurs)?'selected':'';?> value="<?=$fournisseur->idFournisseurs?>"><?=$fournisseur->telephoneFournisseurs.' | '.$fournisseur->prenomFournisseurs.' '.$fournisseur->nomFournisseurs.' | '.$fournisseur->adresseFournisseurs?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $solde;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Solde</label>
                                            <input id="restapayer" type="text" readonly class="form-control" value="<?=$solde?>" name="solde" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $montant;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Montant</label>
                                            <input id="montant" type="number" class="form-control" value="<?=$montant?>" name="montant" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $reste;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Reste</label>
                                            <input readonly id="reste" id="reste" type="text" class="form-control" value="<?=$reste?>" name="reste" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button class="btn btn-primary">Valider</button>
                                        <button type="reset" class="btn btn-default">Annuler</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </form>
                </div>
                <!-- col-lg-6 -->
            </div>
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                    </div>

                    <h2 class="card-title">Liste des Payements</h2>
                </header>
                <div class="card-body">
                    <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Clients</th>
                            <th>Montants</th>
                            <th>Dates</th>
                            <th>Actions(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $increment = 1;
                        if(isset($getReglements) AND !empty($getReglements)): ?>
                            <?php foreach ($getReglements as $item):?>
                                <tr data-item-id="44" role="row" class="odd">
                                    <td class="sorting_1"><?=$increment++ ?></td>
                                    <td><?=$item->prenomFournisseurs.' '.$item->nomFournisseurs?></td>
                                    <td><?=number_format($item->montantReglements).' GNF'?></td>
                                    <td><?=date_format(date_create($item->dateReglements),'d/m/Y')?></td>
                                    <td class="actions">
                                        <?php if(isset($_SESSION['gbmg']['role']) AND $_SESSION['gbmg']['role']=="Administrateur"):?>
                                            <a href="<?=LINK.'payement_fournisseur/'.$item->idReglements?>" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                        <?php endif;?>

                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </section>
            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>
<script src="<?=LINK?>assets/admin/ajax/paiementfournisseur.js?v=<?=uniqid()?>"></script>

</body>
</html>





