<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Modification Client</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Modification</span></li>

                        <li><span>Client</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire d'inscription client</h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données du client, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">
                                <div class="row form-group pb-3">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $prenom?>
                                            <label class="col-form-label" for="formGroupExampleInput">Prénoms</label>
                                            <input type="text" class="form-control" hidden value="<?=$idClient?>" name="idClient" id="formGroupExampleInput" placeholder="">
                                            <input type="text" class="form-control" value="<?=$prenom?>" name="prenom" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $nom?>
                                            <label class="col-form-label" for="formGroupExampleInput">Nom</label>
                                            <input type="text" class="form-control" value="<?=$nom?>" name="nom" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $tel?>
                                            <label class="col-form-label" for="formGroupExampleInput">Contact</label>
                                            <input type="text" class="form-control" value="<?=$tel?>" name="tel" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $adresse;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Adresse</label>
                                            <input type="text" class="form-control"  value="<?=$adresse?>" name="adresse" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button class="btn btn-primary">Valider</button>
                                        <button type="reset" class="btn btn-default">Annuler</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </form>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>





