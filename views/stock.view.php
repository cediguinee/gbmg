<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Liste du stock</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Liste</span></li>

                        <li><span>Stock</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                            </div>

                            <h2 class="card-title">Liste des produits en stock</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Libelle   </th>
                                    <th>Prix d'achat </th>
                                    <th>Prix de vente </th>
                                    <th>Quantité</th>
                                    <th>Montant</th>
                                    <th>Statut</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $increment = 1;
                                if(isset($getProduits) AND !empty($getProduits)): ?>
                                    <?php foreach ($getProduits as $items):?>
                                        <tr data-item-id="44" role="row" class="odd">
                                            <td class="sorting_1"><?=$increment++ ?></td>
                                            <td><?=ucfirst($items->libelleProduits)?></td>
                                            <td><?=number_format($items->prixAchatProduits).' GNF'?></td>
                                            <td><?=number_format($items->prixDeVenteProduits).' GNF'?></td>
                                            <td>
                                                <?php
                                                $quantite = 0;
                                                $getProduit = \models\Achats::countProduitsAchat($items->idProduits);
                                                $getProduitVentes = \models\Ventes::countProduitsVentes($items->idProduits);
                                                foreach ($getProduit as $item){
                                                    global $quantiteAchat;
                                                    $quantiteAchat = $item->quantite;
                                                }
                                                foreach ($getProduitVentes as $item){
                                                    global $quantiteVente;
                                                    $quantiteVente = $item->quantite;
                                                }
                                                $reste = $quantiteAchat-$quantiteVente;
                                                echo number_format($reste);
                                                ?>

                                            </td>
                                            <td>
                                                <?php
                                                $quantite = 0;
                                                $getProduit = \models\Achats::countProduitsAchat($items->idProduits);
                                                foreach ($getProduit as $item){
                                                    echo number_format($reste*$items->prixDeVenteProduits).' GNF';
                                                }
                                                ?>
                                            </td>
                                            <td class="actions">

                                                <?php
                                                if($reste <= 10){
                                                    echo '<span class="badge badge-danger">Rupture</span>';
                                                }elseif ($reste >= 100){
                                                    echo '<span class="badge badge-success">Disponible</span>';
                                                }else{
                                                    echo '<span class="badge badge-warning">Seuil</span>';
                                                }?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>





