<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="<?=LINK.'home'?>" class="logo float-left">
            <img src="<?=LINK?>assets/admin/img/logogbmg.png" height="70" alt="Porto Admin" />
        </a>

        <div class="panel card-sign">
            <div class="card-title-sign mt-3 text-end">
                <h2 class="title text-uppercase font-weight-bold m-0"><i class="bx bx-user-circle me-1 text-6 position-relative top-5"></i> Connexion</h2>
            </div>
            <div class="message">
                <?php if(isset($success) AND !empty($success)):?>
                    <?php foreach ($success as $info):?>
                        <div class="alert alert-success mt-2">
                            <strong>Information : </strong> <?=$info?>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
                <?php if(isset($warnings) AND !empty($warnings)):?>
                    <?php foreach ($warnings as $info):?>
                        <div class="alert alert-warning mt-2">
                            <strong>Avertissemnt : </strong> <?=$info?>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
                <?php if(isset($erreurs) AND !empty($erreurs)):?>
                    <?php foreach ($erreurs as $info):?>
                        <div class="alert alert-danger mt-2">
                            <strong>Erreur : </strong> <?=$info?>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="form-group mb-3">
                        <label>Identifiant</label>
                        <div class="input-group">
                            <?php global $email?>
                            <input name="email"  value="<?=$email?>" type="text" class="form-control form-control-lg" />
                            <span class="input-group-text">
										<i class="bx bx-user text-4"></i>
									</span>
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <div class="clearfix">
                            <label class="float-left">Mot de passe</label>
                            <a href="" class="float-end">Mot de passe oublié ?</a>
                        </div>
                        <div class="input-group">
                            <input name="password" type="password" class="form-control form-control-lg" />
                            <span class="input-group-text">
										<i class="bx bx-lock text-4"></i>
									</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input id="RememberMe" name="rememberme" type="checkbox"/>
                                <label for="RememberMe">Me rappeler</label>
                            </div>
                        </div>
                        <div class="col-sm-4 text-end">
                            <button type="submit" name="Seconnecter" value="Seconnecter" class="btn btn-primary mt-2">Connexion</button>
                        </div>
                        <div class="col-sm-4 text-end">
                            <a href="<?=LINK.'home'?>"><i class="fa fa-home"></i> Aller sur le site</a>
                        </div>
                    </div>


                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-3 mb-3">&copy; Copyright <?=date('Y')?>. All Rights Reserved.</p>
    </div>
</section>
<!-- end: page -->

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>