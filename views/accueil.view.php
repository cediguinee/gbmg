<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=SITE_NAME?> | <?=(!empty($page))?ucfirst($page):'Acceuil'?></title>
    <link rel="shortcut icon" href="<?= LINK ?>assets/logogbmg.jpg" type="image/png">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/smart-opl/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/smart-opl/assets/css/LineIcons.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/smart-opl/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/smart-opl/assets/css/slick.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/smart-opl/assets/css/animate.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/smart-opl/assets/css/default.css">
    <link rel="stylesheet" href="<?= LINK ?>assets/view/smart-opl/assets/css/style.css">
</head>

<body>

    <!--====== PRELOADER PART START ======-->

    <?php //require_once ("includes/view/loader.inc.php");?>
    <section class="header-area">
        <div class="navbar-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="#">
                                <img src="<?= LINK ?>assets/logogbmg.jpg" alt="Logo">
                            </a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarEight" aria-controls="navbarEight" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse sub-menu-bar" id="navbarEight">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item active">
                                        <a class="page-scroll" href="#home">ACCUEIL</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#about">A PROPOS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#services">SERVICES & FORMATIONS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#testimonial">NOTRE EQUIPE</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#client">NOS PARTENAIRES</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#contact">CONTACT</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="navbar-btn d-none mt-15 d-lg-inline-block">
                                <a class="menu-bar" href="#side-menu-right"><i class="lni-menu"></i></a>
                            </div>
                        </nav> <!-- navbar -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- navbar area -->

        <div id="home" class="slider-area">
            <div class="bd-example">
                <div id="carouselOne" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselOne" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselOne" data-slide-to="1"></li>
                        <li data-target="#carouselOne" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="carousel-item bg_cover active" style="background-image: url(views/slider/slider-5.jpg)">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-6 col-lg-7 col-sm-10">
                                            <h2 class="carousel-title">GBM GROUP SARLU</h2>
                                            <ul class="carousel-btn rounded-buttons">
                                                <p style="color:orange">Innove et Inspire, c'est le rendez-vous des compétences professionnelles et d'opportunités pour entreprendre, être autonome et indépendant pour s'épanouir durablement. Alors, merci de nous choisir et de nous faire confiance pour votre bien être personnel et professionnel..</p>
                                                <li><a class="main-btn rounded-three" href="#">En savoir plus</a></li>
                                            </ul>
                                        </div>
                                    </div> <!-- row -->
                                </div> <!-- container -->
                            </div> <!-- carousel caption -->
                        </div> <!-- carousel-item -->

                        <div class="carousel-item bg_cover" style="background-image: url(views/slider/slider-4.jpg)">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-6 col-lg-7 col-sm-10">
                                        <h2 class="carousel-title">GBM GROUP SARLU</h2>
                                            <ul class="carousel-btn rounded-buttons">
                                            <p style="color:white">Innove et Inspire, c'est le rendez-vous des compétences professionnelles et d'opportunités pour entreprendre, être autonome et indépendant pour s'épanouir durablement. Alors, merci de nous choisir et de nous faire confiance pour votre bien être personnel et professionnel..</p>
                                                <li><a class="main-btn rounded-three" href="#">En savoir plus</a></li>
                                            </ul>
                                        </div>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                         <!-- <div class="carousel-item bg_cover" style="background-image: url(views/slider/slider-2.jpg)">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-6 col-lg-7 col-sm-10">
                                            <h2 class="carousel-title">Based on Latest Bootstrap & HTML5</h2>
                                            <ul class="carousel-btn rounded-buttons">
                                                <li><a class="main-btn rounded-three" href="#">GET STARTED</a></li>
                                                <li><a class="main-btn rounded-one" href="#">DOWNLOAD</a></li>
                                            </ul>
                                        </div>
                                    </div> 
                                </div> 
                            </div> 
                        </div> -->
                    </div> 
                    <a class="carousel-control-prev" href="#carouselOne" role="button" data-slide="prev">
                        <i class="lni-arrow-left-circle"></i>
                    </a>
                    <a class="carousel-control-next" href="#carouselOne" role="button" data-slide="next">
                        <i class="lni-arrow-right-circle"></i>
                    </a>
                </div> <!-- carousel -->
            </div> <!-- bd-example -->
        </div>
    </section>
    <div class="sidebar-right" style="background-color: #030264;">
        <div class="sidebar-close">
            <a class="close" href="#close"><i class="lni-close"></i></a>
        </div>
        <div class="sidebar-content">
            <div class="sidebar-logo text-center">
                <a class="navbar-brand" href="#">
                    <img src="<?= LINK ?>assets/logogbmg.jpg" alt="Logo">
                </a>
               
            </div> <!-- logo -->
            <div class="sidebar-menu">
                <ul>
                    <li class="nav-item active">
                        <a class="page-scroll" href="#home">ACCUEIL</a>
                    </li>
                    <li class="nav-item">
                        <a class="page-scroll" href="#about">A PROPOS</a>
                    </li>
                    <li class="nav-item">
                        <a class="page-scroll" href="#services">SERVICES & FORMATIONS</a>
                    </li>
                    <li class="nav-item">
                        <a class="page-scroll" href="#testimonial">NOTRE EQUIPE</a>
                    </li>
                    <li class="nav-item">
                        <a class="page-scroll" href="#client">NOS PARTENAIRES</a>
                    </li>
                    <li class="nav-item">
                        <a class="page-scroll" href="#contact">CONTACT</a>
                    </li>
                </ul>
            </div> <!-- menu -->
            <!-- <div class="sidebar-social d-flex align-items-center justify-content-center">
                <span>FOLLOW US</span>
                <ul>
                    <li><a href="#"><i class="lni-twitter-original"></i></a></li>
                    <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                </ul>
            </div> sidebar social -->
        </div> <!-- content -->
    </div>
    <div class="bg-light mt-5">
        <div class="container my-5">
            <section id="about">
                <h4 class="title">A propos</h4>
                <div>
                    <div>
                        <p class="text">
                            Global Business Marketing Group Sarlu en abrégé GBM Group Sarlu est une entreprise agrée,
                            spécialisée dans les formations de développement des compétences professionnelles, de
                            gestion et de coaching.
                        </p>
                        <p>Dont le but est d’offrir des services de qualité en matière des
                            compétences pratiques et des connaissances stratégiques à ceux et celles qui veulent d’une
                            part embrasser et faire du succès dans l’entrepreneuriat, et d’autre part à ceux et celles qui
                            veulent être des personnes ressources, performantes et très épanouies dans leur carrière
                            professionnelle et personnelle.</p>
                        <p>
                            Au faite, GBM Group Sarlu c’est plus qu’une entreprise c’est
                            un espace d’opportunités à saisir pour le bien-être et s’épanouir durablement.
                        </p>
                        <p>D’où notre
                            slogan chez GBM Group Sarlu c’est le rendez-vous des compétences professionnelles et
                            d’opportunités pour entreprendre, être autonome et indépendant pour s’épanouir
                            durablement. </p>
                        <p>
                            Ensemble, nous transformons vos rêves en réalité, vos idées et projets en
                            business viable.
                            La promptitude, l’efficacité et le professionnalisme dans le travail font de nous le meilleur choix idéal dans les formations des compétences professionnelles. Alors, merci de nous choisir et de nous faire confiance pour votre bien être personnel et professionnel.
                        </p>
                    </div>
            </section>
        </div>
    </div>
    <hr>

    <section id="about">
        <div class="container">
            <div class="row justify-content-center" id="services">
                <div class="col-xl-12 col-lg-12">
                    <div class="about-image text-center wow fadeInUp" data-wow-duration="1.5s" data-wow-offset="100">
                        <!-- <img  src="<?= LINK ?>assets/view/smart-opl/assets/images/services.png" alt="services"> -->
                    </div>
                    <div class="section-title text-center mt-30 pb-40">
                        <h4 class="title wow fadeInUp">SERVICES & FORMATIONS </h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                        <div class="about-icon">
                            <img src="<?= LINK ?>assets/view/smart-opl/assets/images/icon-1.png" alt="Icon">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title">PÂTISSERIE PROFESSIONNELLE</h4>
                            <p class="text">
                                Un métier de formation professionnelle pouvant vous permettre d'avoir des compétences à la fois théorique et pratique pour la production de gâteaux d'anniversaire, Mariage, Madeleine, Yaourt,...
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                        <div class="about-icon">
                            <img src="<?= LINK ?>assets/view/smart-opl/assets/images/icon-1.png" alt="Icon">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title">CUISINE PROFESSIONNELLE</h4>
                            <p class="text">Une formation de métier professionnel à la technique d'art culinaire des recettes diverses (Pizza, Fattaya, Hamburger,...)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                        <div class="about-icon">
                            <img src="<?= LINK ?>assets/view/smart-opl/assets/images/icon-1.png" alt="Icon">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title">SAPONIFICATION AVANCÉE </h4>
                            <p class="text">Un métier de formation très stratégique pour entreprendre, à la fin de cette formation vous serez capable de produire des différentes gammes de produits cosmétiques notamment de savons ménage, toilette, shampoing gel douche, savon en poudre,...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                        <div class="about-icon">
                            <img src="<?= LINK ?>assets/view/smart-opl/assets/images/icon-1.png" alt="Icon">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title">AGROALIMENTAIRE </h4>
                            <p class="text">Une formation de métier professionnel taillée sur mesure pour la technique et la technologie de production du jus, des boissons et de confiture des fruits naturels notamment Mangue, Orange, Pastèque, Tomate, Pomme,...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                        <div class="about-icon">
                            <img src="<?= LINK ?>assets/view/smart-opl/assets/images/icon-1.png" alt="Icon">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title"> BOULANGERIE MODERNE </h4>
                            <p class="text">Un métier de formation professionnelle qui va vous permettre d'avoir des compétences simples et pratiques pour la technique de production des différents types des pains notamment Pain ordinaire, Pain chocolat, Pain Hamburger, Pain viennois,...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                        <div class="about-icon">
                            <img src="<?= LINK ?>assets/view/smart-opl/assets/images/icon-1.png" alt="Icon">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title"> HYGIÈNE ET SÉCURITÉ ALIMENTAIRE </h4>
                            <p class="text">Les bénéficiaires de cette formation vont maîtriser les bonnes pratiques d'hygiène alimentaire à tous les maillons de la chaîne de restauration collective.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>

    <section class="container mt-5">
        <ul class="list-group">
            <h3 class="title">AUTRES FORMATIONS ET SERVICES DISPONIBLES </h3>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Entrepreneuriat</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Développement Personnel</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Leadership Transformationnel</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Business Marketing</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Gestion des projets</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Gestion et Création d'entreprise</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Hygiène et Santé Sécurité au travail</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Communication en Public</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Vente des produits et matériels de saponification</li>
            <li class="list-group-item mt-3"> <span class="mr-3"><img height="30" width="30" src="<?= LINK ?>assets/logo.png" alt="" srcset=""></span> Vente Matériels et Équipements de pâtisserie et Cuisine,...</li>
        </ul>
    </section>


    <section id="portfolio" class="portfolio-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="section-title text-center pb-20">
                        <h4 class="title">Quelques images de nos activités</h4>
                        <hr>
                        <!-- <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p> -->
                    </div> <!-- row -->
                </div>
            </div> <!-- row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio-menu pt-30 text-center">

                    </div> <!-- portfolio menu -->
                </div>
            </div> <!-- row -->
            <div class="row grid">
                <?php if (isset($getAllAnnonces) and !empty($getAllAnnonces)) : ?>
                    <?php foreach ($getAllAnnonces as $allAnnonce) : ?>
                        <div class="col-lg-3 col-sm-6 branding-3 planning-3">
                            <div class="single-portfolio mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.2s">
                                <div class="portfolio-image">
                                    <img src="<?= LINK . "assets/photos/annonces/" . $allAnnonce->photoAnnonce ?>" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup" href="<?= LINK . "assets/photos/annonces/" . $allAnnonce->photoAnnonce ?>"><i class="lni-zoom-in"></i></a>
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-text">
                                    <h5 class="portfolio-titl"><a href="#"><?= $allAnnonce->libelle ?></a></h5>
                                    <p class="text"><?= $allAnnonce->nameAnnonce ?></p>
                                </div>
                            </div> <!-- single portfolio -->
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <section id="testimonial" class="testimonial-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center">
                        <h3 class="title">Notre équipe</h3>
                        <p class="text">Nous possedons une équipe qualifiée prête à vous servir</p>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="row testimonial-active">
                    <?php if(isset($getPersonnes) AND !empty($getPersonnes)):?>
                    <?php foreach($getPersonnes as $personne):?>
                        <div class="col-lg-4">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img height="100px" width="100px" class="img-circle" src="<?=LINK."assets/photos/avatars/".$personne->photoPersonnels?>" alt="Author">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text"><?=ucfirst($personne->professionPersonnels).' & '.ucfirst($personne->fonctionPersonnels)?></p>
                                    <h6 class="author-name"><?=ucfirst($personne->prenomPersonnels).' '.ucfirst($personne->nomPersonnels)?></h6>
                                    <span class="sub-title"><?=ucfirst($personne->fonctionPersonnels)?></span>
                                </div>
                            </div> <!-- single testimonial -->
                        </div>
                        <?php endforeach;?> 
                <?php endif;?>
                    </div> <!-- row -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <section id="client" class="client-logo-area">
        <div class="container">
            <div class="text-center">
                <h2 class="">Nos partenaires</h2>
            </div>
            <hr>

            <div class="row client-active">
                <?php if(isset($getPartenaires) AND !empty($getPartenaires)):?>
                    <?php foreach($getPartenaires as $item):?>
                        <div class="col-lg-3 col-md-4">
                            <div class="single-client text-center" style="width: 200px">
                                 <a title="<?=$item->entreprise?>" href="https://<?=$item->sites?>"><img src="<?= LINK ?>assets/photos/partenaires/<?=$item->photo?>" alt="Logo"></a>
                            </div> <!-- single client -->
                        </div>
                    <?php endforeach;?> 
                <?php endif;?>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== CLIENT LOGO PART ENDS ======-->

    <!--====== CONTACT TWO PART START ======-->
    <section id="inscription" class="contact-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-20">
                        <h3 class="title">Inscription</h3>
                        <!-- <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p> -->
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
            <div class="row">

                <div class="col-lg-12">
                    <div class="message">
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success mt-2">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning mt-2">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger mt-2">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <div class="contact-form form-style-one mt-35 wow fadeIn">
                        <form action="" method="post">
                            <div class="form-input mt-15">
                                <label>Prénoms</label>
                                <div class="input-items default">
                                    <input type="text" placeholder="Prénom" name="prenom">
                                    <i class="lni-user"></i>
                                </div>
                            </div> <!-- form input -->
                            <div class="form-input mt-15">
                                <label>Nom</label>
                                <div class="input-items default">
                                    <input type="text" placeholder="Nom" name="nom">
                                    <i class="lni-user"></i>
                                </div>
                            </div>
                            <div class="form-input mt-15">
                                <label>Numéro</label>
                                <div class="input-items default">
                                    <input type="text" placeholder="Téléphone" name="telephone">
                                    <i class="lni-network"></i>
                                </div>
                            </div>
                            <div class="form-input mt-15">
                                <label>Email</label>
                                <div class="input-items default">
                                    <input type="email" placeholder="Adresse email" name="email">
                                    <i class="lni-envelope"></i>
                                </div>
                            </div> <!-- form input -->
                            <div class="form-input mt-15">
                                <label>Formation</label>
                                <div class="input-items default">
                                    <select name="idFormation" class="form-control">
                                        <option value="">  Veuillez selectionner la formation</option>
                                        <?php if(!empty($getSousCategores)):?>
                                            <?php foreach ($getSousCategores as $category):?>
                                                <option value="<?=$category->idSousCat?>"><?=$category->libelle?></option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                    <i class="lni-underline"></i>
                                </div>
                            </div>
                            <div class="form-input mt-15">
                                <label>Module</label>
                                <div class="input-items default">
                                    <select name="idModule" class="form-control">
                                        <option value="">  Veuillez selectionner le module</option>
                                        <?php if(!empty($getCategories)):?>
                                            <?php foreach ($getCategories as $category):?>
                                                <option value="<?=$category->idCat?>"><?=$category->nomCat?></option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                    <i class="lni-underline"></i>
                                </div>
                            </div>
                            <!-- form input -->
                            <p class="form-message"></p>
                            <div class="form-input rounded-buttons mt-20">
                                <button type="submit" style="background-color: #0007e6 !important;" class="main-btn rounded-three btn-sm">S'inscrire</button>
                            </div> <!-- form input -->
                        </form>
                    </div> <!-- contact form -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
    <section id="contact" class="contact-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-20">
                        <h3 class="title">Nous contacter</h3>
                        <!-- <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p> -->
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
            <div class="row">

                <div class="col-lg-12">
                    <div class="contact-form form-style-one mt-35 wow fadeIn">
                        <form id="contact-form" action="assets/contact.php" method="post">
                            <div class="form-input mt-15">
                                <label>Prénom et nom</label>
                                <div class="input-items default">
                                    <input type="text" placeholder="Name" name="name">
                                    <i class="lni-user"></i>
                                </div>
                            </div> <!-- form input -->
                            <div class="form-input mt-15">
                                <label>Email</label>
                                <div class="input-items default">
                                    <input type="email" placeholder="Email" name="email">
                                    <i class="lni-envelope"></i>
                                </div>
                            </div> <!-- form input -->
                            <div class="form-input mt-15">
                                <label>Massage</label>
                                <div class="input-items default">
                                    <textarea placeholder="Massage" name="massage"></textarea>
                                    <i class="lni-pencil-alt"></i>
                                </div>
                            </div> <!-- form input -->
                            <p class="form-message"></p>
                            <div class="form-input rounded-buttons mt-20">
                                <button type="submit" style="background-color: #030264 !important;" class="main-btn rounded-three btn-sm">Envoyer</button>
                            </div> <!-- form input -->
                        </form>
                    </div> <!-- contact form -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== CONTACT TWO PART ENDS ======-->

    <!--====== FOOTER FOUR PART START ======-->

    <footer id="footer" class="footer-area">
        <div class="footer-copyright" style="background-color: #030264">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="copyright text-center text-lg-left mt-10">
                            <p class="text-white">Conception <a style="color: #f5f6f6" rel="nofollow" href="https://cediguinee.com">CEDIG-SARL</a> Copyright <a style="color: #f6f6f3" rel="nofollow" href="https://ayroui.com"><?=SITE_NAME?></a></p>
                        </div> <!--  copyright -->
                    </div>
                    <div class="col-lg-2">
                        <div class="footer-logo text-center mt-10">
                            <a href="index.html"><img src="<?= LINK ?>assets/logogbmg.jpg" alt="Logo"></a>
                        </div> <!-- footer logo -->
                    </div>
                    <div class="col-lg-5">
                        <ul class="social text-center text-lg-right mt-10">
                            <li><a href="https://www.facebook.com/gbmgroupsarlu/"><i class="lni-facebook-filled"></i></a></li>
                            <li><a href="https://twitter.com/GbmSarlu"><i class="lni-twitter-original"></i></a></li>
                            <li><a href="https://www.instagram.com/gbm_group_sarlu/"><i class="lni-instagram-original"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/gbmgroupsarlu"><i class="lni-linkedin-original"></i></a></li>
                            <li><a href="https://gbmgroupsarlu.com/connexion"><i class="lni-lock"></i></a></li>
                        </ul> <!-- social -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- footer copyright -->
    </footer>

    <a href="#" class="back-to-top"><i class="lni-chevron-up"></i></a>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/bootstrap.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/popper.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/slick.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/isotope.pkgd.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/scrolling-nav.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/jquery.easing.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/wow.min.js"></script>
    <script src="<?= LINK ?>assets/view/smart-opl/assets/js/main.js"></script>

</body>

</html>