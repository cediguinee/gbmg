<!DOCTYPE html>
<html lang="en">
<?php require_once ("includes/view/head.inc.php");?>
<body>
<?php require_once ("includes/view/header.inc.php");?>
<?php require_once ("includes/view/mobile.inc.php");?>
<!-- banner section start -->
<?php if(isset($getAllSliders) AND !empty($getAllSliders)):?>
<?php foreach($getAllSliders as $allSlider):?>
        <?php $background = LINK."assets/photos/sliders/".$allSlider->photo; ?>
        <style>
            .banner-section{
                background-image: url(<?=$background?>);
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                padding: 180px 0;
                position: relative;
                z-index: 0;
            }
        </style>
        <div class="banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-sm-10">
                        <div class="banner-content">
                            <h1 class="section-header text-white pb-20 aos-init aos-animate" data-aos="fade-right"><?=$allSlider->nom?></h1>
                            <p class="text-white aos-init aos-animate" data-aos="fade-right"><?=$allSlider->description?>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php endforeach;?>
<?php endif;?>
<!-- banner section Ends -->
<!--case study section start-->
<div class="case-study-section section-padding section-padding-mobile">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5 text-center pb-40">
                <span class="sub-heading bar">Nos Actiivités</span>
                <h3>Quelles images de nos activités</h3>
            </div>
        </div>
        <div class="row">
            <?php if(isset($getAllAnnonces) AND !empty($getAllAnnonces)):?>
            <?php foreach($getAllAnnonces as $allAnnonce):?>
            <div class="col-xl-4 col-lg-4 col-md-6 pos-relative mb-85">
                <div class="case-study-box">
                    <div class="case-thumb pos-relative">
                        <img height="100px" width="100px" src="<?=LINK."assets/photos/annonces/".$allAnnonce->photoAnnonce?>" alt="iamge here" class="img-fluid">
                    </div>
                    <div class="case-content case-content-style-1">
                        <p><?=$allAnnonce->nameAnnonce?></p>
                        <h4><a href=""><p><?=$allAnnonce->libelle?></p></a></h4>
                        <a href="" class="btn-view btn-view-theme-2"><i class="fas fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>
</div>
<!--case study section Ends-->
<div class="container team-section section-padding section-padding-mobile">
    <div class="row justify-content-center aos-init aos-animate" data-aos="fade-up">
        <div class="col-xl-6 text-center pb-40">
            <span class="sub-heading bar">Notre Equipe</span>
            <h3>Nos experts à votre service</h3>
        </div>
    </div>
    <div class="row aos-init aos-animate" data-aos="fade-up">
        <?php if(isset($getAllPersonnes) AND !empty($getAllPersonnes)):?>
            <?php foreach($getAllPersonnes as $personne):?>
                 <div class="col-xl-4 col-lg-6 col-md-6 p-0">
                <div class="team-single pos-relative">
                <div class="team-thumb fix pos-relative">
                    <img height="150px" width="150px" src="<?=LINK?>assets/photos/avatars/<?=$personne->photoPersonnels?>" alt="image" class="img-fluid">
                </div>
                <div class="team-info text-center pt-20">
                    <h4><?=ucfirst($personne->prenomPersonnels).' '.strtoupper($personne->nomPersonnels)?></h4>
                    <p class="pt-10"><?=strtoupper($personne->fonctionPersonnels)?></p>
                    <div class="team-social team-social-style-1">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram-square"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
            <?php endforeach;?>
        <?php endif;?>
    </div>
</div>

<!-- footer section start -->
<div class="contact-section top-spacing">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 text-center pb-50">
                <span class="sub-heading bar pb-10">Nos Partenaires</span>
                <h3>Des personnes qui nos sont portez confiance.</h3>
            </div>
        </div>
        <div class="brand-section section-padding aos-init aos-animate" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="slider-5 slick-initialized slick-slider">
                        <div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 3120px; transform: translate3d(-936px, 0px, 0px);"><div class="brand-item slick-slide slick-cloned" data-slick-index="-6" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-cloned" data-slick-index="-5" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-cloned" data-slick-index="-4" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-cloned" data-slick-index="-3" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-cloned" data-slick-index="-2" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-cloned" data-slick-index="-1" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="0" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-active" data-slick-index="1" aria-hidden="false" tabindex="0" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-active" data-slick-index="2" aria-hidden="false" tabindex="0" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-active" data-slick-index="3" aria-hidden="false" tabindex="0" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-active" data-slick-index="4" aria-hidden="false" tabindex="0" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-active" data-slick-index="5" aria-hidden="false" tabindex="0" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide" data-slick-index="6" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-cloned" data-slick-index="7" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-cloned" data-slick-index="8" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div>
                                <div class="brand-item slick-slide slick-cloned" data-slick-index="9" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div><div class="brand-item slick-slide slick-cloned" data-slick-index="10" id="" aria-hidden="true" tabindex="-1" style="width: 136px;">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid main-logo">
                                    <img src="<?=LINK?>assets/partenaires/cedig.jpeg" alt="image here" class="img-fluid hover-logo">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- footer-bottom -->
<?php require_once ("includes/view/footer.inc.php");?>

<?php require_once ("includes/view/foot.inc.php");?>


</body>
</html>