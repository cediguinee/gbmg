<?php
use models\Entreprises;
$entreprise = Entreprises::getEntreprise();
foreach ($entreprise as $e) {
    $nom = $e->nom;
    $nif = $e->nif;
    $rccm = $e->rccm;
    $compte = $e->compte;
    $numcompte = $e->numcompte;
    $tel = $e->tel;
    $email = $e->email;
    $adresse = $e->adresse;
}
?>
<html>
<head>
    <title>FACTURE</title>
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?=LINK?>assets/admin/vendor/bootstrap/css/bootstrap.css" />

    <!-- Invoice Print Style -->
    <link rel="stylesheet" href="<?=LINK?>assets/admin/css/invoice-print.css" />
</head>
<body>
<div class="invoice">
    <?php global  $facture;?>
    <?php if(isset($getAchat) AND !empty($getAchat)):?>
        <?php foreach($getAchat as $user):?>
            <?php $facture = $user->referenceVentes;?>
            <header class="clearfix">
                <div class="row">
                    <div class="col-sm-6 mt-3">
                        <h2 class="h2 mt-0 mb-1 text-dark font-weight-bold">FACTURE</h2>
                        <h4 class="h4 m-0 text-dark font-weight-bold">#<?=$user->referenceVentes?></h4>
                    </div>
                    <div class="col-sm-6 text-end mt-3 mb-3">

                        <div class="ib">
                            <img height="100px" width="200pxx" src="<?=LINK?>assets/admin/img/logogbmg.png" alt="">
                        </div>
                    </div>
                </div>
            </header>
            <div class="bill-info">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bill-to">
                            <p class="h5 mb-1 text-dark font-weight-semibold">INFO CLIENT:</p>
                            <address>
                                <?=ucfirst($user->prenomClient).' '.strtoupper($user->prenomClient)?>
                                <br>
                                <?=ucfirst($user->adresseClient)?>
                                <br>
                                <?=ucfirst($user->telephoneClient)?>
                                <br>
                            </address>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bill-data text-end">
                            <p class="mb-0">
                            <div class="text-center">
                                <address>
                                    Téléphone :<?= $tel ?>
                                    Addresse :<?= $adresse ?>
                                    Email :<?= $email ?>
                                </address>
                            </div>
                                <span class="text-dark">Date de vente :</span>
                                <span class="value"><?=date_format(date_create($user->dateVentes),'d/m/Y')?></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    <?php endif;?>
    <table class="table table-responsive-md invoice-items">
        <thead>
        <tr class="text-dark">
            <th id="cell-id" class="font-weight-semibold">#</th>
            <th id="cell-item" class="font-weight-semibold">Description</th>
            <th id="cell-price" class="text-center font-weight-semibold">Prix</th>
            <th id="cell-qty" class="text-center font-weight-semibold">Quantité</th>
            <th id="cell-total" class="text-center font-weight-semibold">Total</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $increment = 1;
        $total = 0;
        $paye = 0;
        $reste = 0;
        if(isset($getInfoFacture) AND !empty($getInfoFacture)):?>
            <?php foreach($getInfoFacture as $item):?>
                <?php $total += $item->totalVentes;
                $paye= $item->montantpaye;
                $reste= $item->restepaye;
                ?>
                <tr>
                    <td><?=$increment++?></td>
                    <td class="font-weight-semibold text-dark"><?=ucfirst($item->libelleProduits)?></td>
                    <td class="text-center"><?=number_format($item->prixVentes)?></td>
                    <td class="text-center"><?=number_format($item->quantiteVentes)?></td>
                    <td class="text-center"><?=number_format($item->totalVentes).' GNF'?></td>
                </tr>
            <?php endforeach;?>
        <?php endif;?>
        </tbody>
    </table>

    <div class="invoice-summary">
        <div class="row justify-content-end">
            <div class="col-sm-4">
                <table class="table h6 text-dark">
                    <tbody>
                    <tr class="h4">
                        <td colspan="2">Total</td>
                        <td class="text-left"><?=number_format($total).' GNF'?></td>
                    </tr>
                    <tr class="b-top-0">
                        <td colspan="2">Payé</td>
                        <td class="text-left"><?=number_format($paye).' GNF'?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Reste</td>
                        <td class="text-left"><?=number_format($reste).' GNF'?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <td style="text-align: center;" colspan="3">RCCM : <?=$rccm ?> NIF: <?=$nif ?> | N° Compte: <?=$numcompte?> UBA </td>
    </div>
</div>
<script>
    window.print();
</script>
</body>
</html>

