<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Détail de l'employé</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Détail</span></li>

                        <li><span>Employé</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>

                </div>
                <!-- col-lg-6 -->
            </div>
            <section class="card">
                <div class="card-body tab-content">
                    <div id="custom-log" class="tab-pane active">
                        <table class="table table-striped table-no-more table-bordered  mb-0">
                            <thead>
                            <tr>
                                <th colspan="2"><span class="font-weight-normal text-5">Information détail d'employé</span></th>
                            </tr>
                            </thead>
                            <tbody class="log-viewer">
                            <?php if(!empty($getAllEmploye)):?>
                                <?php foreach ($getAllEmploye as $item):?>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Matricule : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->matriculePersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">CIN : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->cinPersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Civilité : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->sexePersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Prénom : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->prenomPersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Nom : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->nomPersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Contact : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->telephonePersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Adresse : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->adressePersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Email : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=$item->emailPersonnels?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Date Naissance : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=$item->dateNaissancePersonnels?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Profession : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->professionPersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Résidence : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->residencePersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Pays : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->paysPersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Ville : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=ucfirst($item->villePersonnels)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Salaire : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?=number_format($item->salairePersonnels).' GNF'?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-title="Type" class="pt-3 pb-3">
                                            <i class="fas fa-info fa-fw text-info text-5 va-middle"></i>
                                            <span class="va-middle">Filiation : </span>
                                        </td>
                                        <td data-title="Date" class="pt-3 pb-3">
                                            <?="Fils de :" .$item->perePersonnels.' '.$item->nomPersonnels.' Et De :'.$item->merePersonnels?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  data-title="Type" class="pt-3 pb-3" colspan="2">
                                            <a href="<?=LINK.'liste_des_employes/'?>" class="on-default remove-row btn btn-dark"><i class="fa fa-backward"></i> Retour</a>
                                            <a href="<?=LINK.'tableau_de_bord/'?>" class="on-default remove-row btn btn-primary"><i class="fa fa-home"></i> Tableau de bord</a>

                                        </td>
                                    </tr>


                                <?php endforeach;?>
                            <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>







