<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Liste des Partenaires</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Liste</span></li>

                        <li><span>Partenaires</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                            </div>

                            <h2 class="card-title">Liste des Partenaires</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Matricule</th>
                                    <th>Prénoms et Nom</th>
                                    <th>Téléphone</th>
                                    <th>Adresse</th>
                                    <th>Langue</th>
                                    <th>Etat</th>
                                    <th>Actions(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $increment = 1;
                                if(isset($getClients) AND !empty($getClients)): ?>
                                    <?php foreach ($getClients as $item):?>
                                        <tr>
                                            <td><?=$increment++ ?></td>
                                            <td><?=ucfirst($item->matriculeParticipants)?></td>
                                            <td><?=$item->civiliteParticipants." ".ucfirst($item->prenomParticipants)." ".strtoupper($item->nomParticipants)?></td>
                                            <td><?=ucfirst($item->contactParticipants)?> </td>
                                            <td><?=ucfirst($item->adresseParticipants)?></td>
                                            <td><?=ucfirst($item->langueParticipants)?> </td>
                                            <td><?=($item->etatParticipants=="0")?'<button class="alert alert-info">Client</button>':'<button class="alert alert-dark">Partenaire</button>'?></td>

                                            <td>
                                                <a href="<?=LINK.'modification_participant/'.$item->idParticipants?> "><i class="fas fa-pencil-alt"></i></a>
                                                <?php if(isset($_SESSION['gbmg']['role']) AND $_SESSION['gbmg']['role']=="Administrateur"):?>
                                                    <a href="<?=LINK.'liste_des_clients/'.$item->idParticipants?>"><i class="far fa-trash-alt"></i></a>
                                                <?php endif;?>

                                                <a href="<?=LINK.'detail_client/'.$item->idParticipants?>" class="on-default remove-row"><i class="far fa-eye-slash"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>




