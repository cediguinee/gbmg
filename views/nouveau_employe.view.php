<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Nouveau Employé</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Nouveau</span></li>

                        <li><span>Employé</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post" enctype="multipart/form-data">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire de recrutement d'un employe</h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données de l'employe, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">
                                <div class="row form-group pb-3">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $matricule;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Matricule</label>
                                            <input type="text" class="form-control"  readonly value="<?=$matricule?>" name="matricule" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $sexe?>
                                            <label class="col-form-label" for="formGroupExampleInput">Civilité</label>
                                            <select name="sexe" id="" class="form-control">
                                                <option value="">Veuillez séléctionner la civilité</option>
                                                <option <?=($sexe=="M.")?'selected':''?> value="M.">M.</option>
                                                <option <?=($sexe=="Mme")?'selected':''?>  value="Mme">Mme</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $prenom?>
                                            <label class="col-form-label" for="formGroupExampleInput">Prénoms</label>
                                            <input type="text" class="form-control" value="<?=$prenom?>" name="prenom" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $nom?>
                                            <label class="col-form-label" for="formGroupExampleInput">Nom</label>
                                            <input type="text" class="form-control" value="<?=$nom?>" name="nom" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $tel?>
                                            <label class="col-form-label" for="formGroupExampleInput">Contact</label>
                                            <input type="text" class="form-control" value="<?=$tel?>" name="tel" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $adresse?>
                                            <label class="col-form-label" for="formGroupExampleInput">Adresse</label>
                                            <input type="text" class="form-control" value="<?=$adresse?>" name="adresse" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $datenaissance?>
                                            <label class="col-form-label" for="formGroupExampleInput">Date Naissance</label>
                                            <input type="date" class="form-control" value="<?=$datenaissance?>" name="datenaissance" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $lieu?>
                                            <label class="col-form-label" for="formGroupExampleInput">Lieu</label>
                                            <input type="text" class="form-control" value="<?=$lieu?>" name="lieu" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $profession?>
                                            <label class="col-form-label" for="formGroupExampleInput">Profession</label>
                                            <input type="text" class="form-control" value="<?=$profession?>" name="profession" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $fonction?>
                                            <label class="col-form-label" for="formGroupExampleInput">Fonction</label>
                                            <input type="text" class="form-control" value="<?=$fonction?>" name="fonction" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $residence;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Résidence</label>
                                            <input type="text" class="form-control" value="<?=$residence?>" name="residence" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $ville;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Ville</label>
                                            <input type="text" class="form-control" value="<?=$ville?>" name="ville" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $pays;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Pays</label>
                                            <input type="text" class="form-control" value="<?=$pays?>" name="pays" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $cin;?>
                                            <label class="col-form-label" for="formGroupExampleInput">CIN</label>
                                            <input type="text" class="form-control" value="<?=$cin?>" name="cin" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $parent;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Personne à Contacter</label>
                                            <input type="text" class="form-control" value="<?=$parent?>" name="parent" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $salaire;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Salaire de Base</label>
                                            <input type="number" class="form-control" value="<?=$salaire?>" name="salaire" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $papa;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Fils de </label>
                                            <input type="text" class="form-control" value="<?=$papa?>" name="papa" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $mere;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Et de </label>
                                            <input type="text" class="form-control" value="<?=$mere?>" name="mere" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $email;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Email </label>
                                            <input type="text" class="form-control" value="<?=$email?>" name="email" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $password;?>
                                            <label class="col-form-label" for="formGroupExampleInput">Mot de passe</label>
                                            <input type="text" class="form-control" value="<?=$password?>" name="password" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $role?>
                                            <label class="col-form-label" for="formGroupExampleInput">Rôle</label>
                                            <select name="role" class="form-control" id="">
                                                <option value="Administrateur">Administrateur</option>
                                                <option value="Comptable">Comptable</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $statut?>
                                            <label class="col-form-label" for="formGroupExampleInput">Statut</label>
                                            <select name="statut" class="form-control" id="">
                                                <option value="">Veuillez séléctionner le statut</option>
                                                <option value="Active" <?=($statut=="Active")?'selected':''?> >Activé</option>
                                                <option value="Desactive" <?=($statut=="Desactive")?'selected':''?>>Désactivé</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php global $identifiant?>
                                            <label class="col-form-label" for="formGroupExampleInput">Identifiant</label>
                                            <input type="text" class="form-control" value="<?=$identifiant?> " name="identifiant" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-form-label" for="formGroupExampleInput">Photo</label>
                                            <input type="file" class="form-control" name="photo" id="formGroupExampleInput" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button class="btn btn-primary">Valider</button>
                                        <button type="reset" class="btn btn-default">Annuler</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </form>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>




