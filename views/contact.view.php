<!DOCTYPE html>
<html lang="en">
<?php require_once ("includes/view/head.inc.php");?>
<body>

<?php require_once ("includes/view/header.inc.php");?>
<?php require_once ("includes/view/mobile.inc.php");?>

<!-- banner section start -->
<div class="banner-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 p-0 text-center">
                <div class="banner-content">
                    <h1 class="text-white">Contact</h1>
                    <span>Acceuil &#8250; Contact</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner section Ends -->

<!-- contact-section start -->
<div class="contact-section-1 section-padding section-padding-mobile">
    <div class="container">
        <div class="row justify-content-center pb-50">
            <div class="col-xl-4 text-center">
                <span class="sub-heading bar">Contactez-nous</span>
                <h3>Pour plus d'information contactez-nous</h3>
            </div>
        </div>
        <div class="row justify-content-between">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="contact-form">
                    <h4 id="warning" style="color: #FF5E15;" class="pb-10"></h4>
                    <form action="" name="contact-form" method="post">
                        <div class="mt-2">
                            <input class="form-control" name="full-name" type="text" placeholder="Votre nom complet" required>
                        </div>
                        <div class="mt-2">
                            <input class="form-control" name="number" type="number" placeholder="Votre numéro" required>
                        </div>
                       <div class="mt-2">
                           <input class="form-control" name="email-address" type="email" placeholder="Vote Addresse Email " required>
                       </div>
                       <div class="mt-2">
                           <input class="form-control" name="subject" type="text" placeholder="Subjet" required>
                       </div>
                       <div class="mt-2">
                           <textarea name="comments" cols="30" class="form-control" rows="7" placeholder="Message" required></textarea>
                       </div>

                        <button class="b-primary mt-30" name="submit-btn" type="submit">Envoyer le Message<i class="fas fa-long-arrow-alt-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- contact section Ends -->

<!-- footer-bottom -->
<?php require_once ("includes/view/footer.inc.php");?>
<!-- footer section Ends -->


<!-- js here-->
<?php require_once ("includes/view/foot.inc.php");?>


</body>
</html>
