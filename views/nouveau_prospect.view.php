<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Nouveau Prospect</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Nouveau</span></li>

                        <li><span>Prospect</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <form id="form" action="" class="form-horizontal" novalidate="novalidate" method="post">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                </div>

                                <h2 class="card-title">Formulaire de pré-inscription/prospection</h2>
                                <p class="card-subtitle">
                                    Veuillez saisir correctement les données de prospection, puis valider le formululaire.
                                </p>
                            </header>
                            <div class="card-body">

                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Prénom du Prospect <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $prenom;?>
                                        <input type="text" name="prenom" value="<?=$prenom;?>" class="form-control" placeholder="Ex.: Mariame" required="">
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Nom du Prospect <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $nom;?>
                                        <input type="text" name="nom" value="<?=$nom;?>" class="form-control" placeholder="Ex.: Diallo" required="">
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Téléphone du Prospect <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $telephone;?>
                                        <input type="text" name="telephone" value="<?=$telephone;?>" class="form-control" placeholder="Ex.: 622000000" required="">
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Email du Prospect <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $email;?>
                                        <input type="email" name="email" value="<?=$email;?>" class="form-control" placeholder="Ex.: gbmg@gmail.com" required="">
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Formation <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $idFormation;?>
                                        <select name="idFormation" value="idFormation" id=""  data-plugin-selectTwo class="form-control populate" class="form-control">
                                            <option value="">Veuillez séléctionner le choix du prospect</option>
                                            <?php if(isset($getSousCategores) AND !empty($getSousCategores)):?>
                                                <?php foreach ($getSousCategores as $getSousCategore):?>
                                                    <option <?=($getSousCategore->idSousCat==$idFormation)?'selected':''?> value="<?=$getSousCategore->idSousCat?>"><?=$getSousCategore->libelle?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row pb-3">
                                    <label class="col-sm-3 control-label text-sm-end pt-2">Module <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <?php global $idModule;?>
                                        <select name="idModule" value="<?=$idModule;?>" name="idModule"  data-plugin-selectTwo class="form-control populate" class="form-control">
                                            <option value="">Veuillez séléctionner le choix du prospect</option>
                                            <?php if(isset($getCategories) AND !empty($getCategories)):?>
                                                <?php foreach ($getCategories as $category):?>
                                                    <option <?=($category->idCat==$idModule)?'selected':''?> value="<?=$category->idCat?>"><?=$category->nomCat?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row justify-content-end">
                                    <div class="col-sm-9">
                                        <button class="btn btn-primary">Valider</button>
                                        <button type="reset" class="btn btn-default">Annuler</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </form>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>


