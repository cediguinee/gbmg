<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body card-margin">
            <header class="page-header">
                <h2>Liste des Achats</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Liste</span></li>

                        <li><span>Achats</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="message mb-2">
                        <br>
                        <?php if(isset($success) AND !empty($success)):?>
                            <?php foreach ($success as $info):?>
                                <div class="alert alert-success ">
                                    <strong>Information : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($warnings) AND !empty($warnings)):?>
                            <?php foreach ($warnings as $info):?>
                                <div class="alert alert-warning ">
                                    <strong>Avertissemnt : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if(isset($erreurs) AND !empty($erreurs)):?>
                            <?php foreach ($erreurs as $info):?>
                                <div class="alert alert-danger ">
                                    <strong>Erreur : </strong> <?=$info?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                            </div>

                            <h2 class="card-title">Liste des factures d'achat</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Libelle</th>
                                    <th>Montant de la vente</th>
                                    <th>Payé</th>
                                    <th>Reste</th>
                                    <th>Client</th>
                                    <th>Date</th>
                                    <th>Actions(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $increment = 1;
                                if(isset($getAllAchats) AND !empty($getAllAchats)): ?>
                                    <?php foreach ($getAllAchats as $item):?>
                                        <?php $getAchatsProduits = \models\Achats::getAchatAllByReference($item->referenceAchats);?>
                                        <?php foreach ($getAchatsProduits as $items):?>
                                            <tr data-item-id="44" role="row" class="odd">
                                                <td class="sorting_1"><?=$increment++ ?></td>
                                                <td><?=$items->referenceAchats?></td>
                                                <td><?=number_format($items->total).' GNF'?></td>
                                                <td><?=number_format($items->montantpaye).' GNF'?></td>
                                                <td><?=number_format($items->restepaye).' GNF'?></td>
                                                <td><?=ucfirst($items->prenomFournisseurs).' '.ucfirst($items->nomFournisseurs)?></td>
                                                <td><?=date_format(date_create($items->dateAchats),'d/m/Y')?></td>
                                                <td class="actions">
                                                    <a href="<?=LINK.'modification_achat/'.$items->referenceAchats?>" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="<?=LINK.'imprimer/'.$items->referenceAchats?>" class="on-default remove-row"><i class="fa fa-print"></i></a>
                                                    <?php if(isset($_SESSION['gbmg']['role']) AND $_SESSION['gbmg']['role']=="Administrateur"):?>
                                                        <a href="<?=LINK.'liste_des_achats/'.$items->referenceAchats?>" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                                    <?php endif;?>

                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
                <!-- col-lg-6 -->
            </div>

            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>




