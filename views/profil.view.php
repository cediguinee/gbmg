<!doctype html>
<html class="fixed">
<?php require_once ("includes/admin/head.inc.php");?>
<body>
<section class="body">

    <!-- start: header -->
    <?php require_once ("includes/admin/header.inc.php");?>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <?php require_once ("includes/admin/sidebar.inc.php");?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>Profil Utilisateur</h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="bx bx-home-alt"></i>
                            </a>
                        </li>

                        <li><span>Home</span></li>

                        <li><span>Profil Utilisateur</span></li>

                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->

            <div class="row">
                <div class="message mb-2">
                    <br>
                    <?php if(isset($success) AND !empty($success)):?>
                        <?php foreach ($success as $info):?>
                            <div class="alert alert-success ">
                                <strong>Information : </strong> <?=$info?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if(isset($warnings) AND !empty($warnings)):?>
                        <?php foreach ($warnings as $info):?>
                            <div class="alert alert-warning ">
                                <strong>Avertissemnt : </strong> <?=$info?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if(isset($erreurs) AND !empty($erreurs)):?>
                        <?php foreach ($erreurs as $info):?>
                            <div class="alert alert-danger ">
                                <strong>Erreur : </strong> <?=$info?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
                <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">

                    <section class="card">
                        <div class="card-body">
                            <div class="thumb-info mb-3">
                                <img src="<?=LINK."assets/photos/avatars/".$photo?>" class="rounded img-fluid" alt="John Doe">
                                <div class="thumb-info-title">
                                    <span class="thumb-info-inner"><?=$prenom.' '.$nom?></span>
                                    <span class="thumb-info-type"><?=$fonction?></span>
                                </div>
                            </div>

                            <div class="widget-toggle-expand mb-3">
                                <div class="widget-header">
                                    <h5 class="mb-2 font-weight-semibold text-dark">Profil Complet</h5>
                                    <div class="widget-toggle">+</div>
                                </div>
                                <div class="widget-content-collapsed">
                                    <div class="progress progress-xs light">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                            100%
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-expanded">
                                    <ul class="simple-todo-list mt-3">
                                        <li class="completed">Photo mise à jour</li>
                                        <li class="completed">Information à jour</li>
                                    </ul>
                                </div>
                            </div>

                            <hr class="dotted short">

                            <h5 class="mb-2 mt-3">A-propos</h5>
                            <p class="text-2">Vous pouvez changer votre mot de passe ici, merci.</p>
                        </div>
                    </section>

                </div>
                <div class="col-lg-8 col-xl-6">

                    <div class="tabs">
                        <div class="tab-content">
                            <div id="edit" class="tab-pane active">

                                <form class="p-3" method="post">
                                    <h4 class="mb-3 font-weight-semibold text-dark">Information Personnelle</h4>
                                    <div class="row row mb-4">
                                        <div class="form-group col">
                                            <label for="inputAddress">Prénoms et Nom</label>
                                            <input type="text" class="form-control" readonly id="inputAddress" value="<?=$prenom.' '.$nom ?>" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="form-group col">
                                            <label for="inputAddress2">Addresse</label>
                                            <input type="text" class="form-control" readonly id="inputAddress2" value="<?=$adresse.'/'.$telephone ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="inputCity">Email</label>
                                            <input type="text" class="form-control" readonly id="inputCity" value="<?=$email?>">
                                        </div>
                                        <div class="form-group col-md-6 border-top-0 pt-0">
                                            <label for="inputZip">Identifiant</label>
                                            <input type="text" class="form-control" readonly id="inputZip" name="login" value="<?=$login?>" >
                                        </div>
                                    </div>

                                    <hr class="dotted tall">

                                    <h4 class="mb-3 font-weight-semibold text-dark">Changer de mot de passe</h4>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword4">Nouveau</label>
                                            <input type="text" class="form-control" hidden  name="login" value="<?=$login?>" >
                                            <input type="password" class="form-control" id="inputPassword4" name="motdepasse1" placeholder="Mot de passe">
                                        </div>
                                        <div class="form-group col-md-6 border-top-0 pt-0">
                                            <label for="inputPassword5">Confirmation</label>
                                            <input type="password" class="form-control" id="inputPassword5" name="motdepasse2"  placeholder="Mot de passe">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-end mt-3">
                                            <button type="submit" class="btn btn-primary">Valider</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">

                    <h4 class="mb-3 mt-0 font-weight-semibold text-dark">Status</h4>
                    <ul class="simple-card-list mb-3">
                        <li class="success">
                            <h3>488</h3>
                            <p class="text-light">Dernière Connexion.</p>
                        </li>
                        <li class="warning">
                            <h3>20</h3>
                            <p class="text-light">Total Connexion.</p>
                        </li>
                        <li class="danger">
                            <h3>16</h3>
                            <p class="text-light">Connexion Echouée.</p>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- end: page -->
        </section>
    </div>

    <?php require_once ("includes/admin/third.inc.php");?>

</section>

<!-- Vendor -->
<?php require_once ("includes/admin/foot.inc.php");?>

</body>
</html>

