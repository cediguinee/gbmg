<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];

use models\Categorie;
use models\SousCategorie;
use models\Prospects;
if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)){
        $getInfo = Prospects::getProspectsById($id);
        foreach ($getInfo as $item):
            $id          = $item->idPospects;
            $nom         = $item->nomPospects;
            $prenom      = $item->prenomPospects;
            $telephone   = $item->contactPospects;
            $email       = $item->emailPospects;
            $idModule    = $item->idCategories ;
            $idFormation = $item->idSousCategories;
        endforeach;
    }
    endif;

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(isset($nom) AND empty($nom)):
        array_push($warnings,"Veuillez saisir le nom du prospect");
    endif;
    if(isset($prenom) AND empty($prenom)):
        array_push($warnings,"Veuillez saisir le prénom du prospect");
    endif;
    if(isset($telephone) AND empty($telephone)):
        array_push($warnings,"Veuillez saisir le numéro de téléphone du prospect");
    endif;
    /*
    if(isset($email) AND empty($email)):
        array_push($warnings,"Veuillez saisir l'email du prospect");
    endif;
    */
    if(isset($idFormation) AND empty($idFormation)):
        array_push($warnings,"Veuillez séléctionner la formation du prospect");
    endif;
    if(isset($idModule) AND empty($idModule)):
        array_push($warnings,"Veuillez séléctionner le module du prospect");
    endif;
    if(Prospects::verifyProspects($idModule,$idFormation,$telephone)>1):
        array_push($erreurs,"Cet prospect est déjà inscrit a cette formation");
    endif;
    if(numberGN($telephone)!=1){
        array_push($erreurs,"Cet numéro n'est pas Guinéens");
    }
    if(count($warnings)==0 AND count($erreurs)==0):
        Prospects::updateProspects($nom,$prenom,$telephone,$email,$idModule,$idFormation,$id);
        array_push($success,"Prospect modifié avec succès");
    endif;

endif;









$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();