<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\SousCategorie;
if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($module)):
        array_push($warnings,"Veuillez saisir le nom de la formation");
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):
        SousCategorie::addSousCategorie($module);
        array_push($success,"Formation {$module} crée avec succès");
    endif;
endif;
