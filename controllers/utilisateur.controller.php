<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];
use models\Personnels;
if (isset($_GET) and !empty($_GET)) {
    extract($_GET);
    if (!empty($id)) {
        $getIdPersonnels = Personnels::getAllPersonnelsById($id);
        if (count($getIdPersonnels) == 0) {
            array_push($warnings, "Cet personnel que vous voulez supprimer n'existe pas !");
        } else {
            Personnels::deletePersonnels($id);
            array_push($success, "Le personnel a été supprimer avec succes.");
        }
    }
}


$getAllEmployes=Personnels::getAllPersonnels();