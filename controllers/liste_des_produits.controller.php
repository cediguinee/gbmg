<?php
connected();
$success = [];
$warnings = [];
$erreurs = [];

use models\Produits;

if (isset($_GET) and !empty($_GET)):
    extract($_GET);

        if (isset($id) and !empty($id)):
            if (isset($_SESSION['gbmg']['role']) and $_SESSION['gbmg']['role'] == "Comptable"):
                array_push($warnings,"Seul l'administrateur a le droit de supprimer");
            else:
            Produits::deleteProduits($id);
            array_push($success, "Produit supprimer avec succès");
        endif;
    endif;

endif;

$getAllProduits = Produits::showAllProduits();