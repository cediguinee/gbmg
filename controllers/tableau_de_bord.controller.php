<?php

use models\Annonces;
use models\Categorie;
use models\Commentaire;
use models\Personnels;
use models\SousCategorie;
use models\Participants;
use models\PayementFormations;
use models\Ventes;
use models\Achats;


connected();

$prixFormationNet=0;
$montantPayements=0;
$restePayements=0;

$getParticipants = Participants::getAllParticipantsPayementsLimit();

$getPayements = PayementFormations::getAllPayementFormations();

foreach ($getPayements as $payement):
    $prixFormationNet = $prixFormationNet + $payement->prixFormationNet;
    $montantPayements = $montantPayements + $payement->montantPayements;
    //$restePayements = $restePayements + $payement->restePayements;
endforeach;
$restePayements =  $prixFormationNet-$montantPayements;
$getVentes = Ventes::getVentesAll();

$getVentesRefe = Ventes::VentesReference();

global $montantdesventes;
global $montantdesachats;

$montantdesventes = 0;
$montantdesachats = 0;

//Ventes
foreach($getVentes as $reference):
    $getVentesRefeTotal = Ventes::VentesReferenceTotal($reference->referenceventes);
    foreach ($getVentesRefeTotal as $total):
        $montantdesventes= $montantdesventes+$total->total;
    endforeach;
endforeach;
//Achats
$getAchats = Achats::getAchatsAll();
$getAchatsRefe = Achats::AchatsReference();

foreach($getAchats as $referenceAchats):
    $getAchatsRefeTotal = Achats::AchatsReferenceTotal($referenceAchats->referenceAchats);
    foreach ($getAchatsRefeTotal as $total):
        $montantdesachats= $montantdesachats+$total->total;
    endforeach;
endforeach;

//Solde difference achat et vente
$solde = (int)$montantdesachats-(int)$montantdesventes;
