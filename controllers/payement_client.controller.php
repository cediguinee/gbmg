<?php
connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];

$succes = "";
$warning = "";
$erreur = "";


use models\ReglementsClients;
use models\Client;



require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => OrangeSMSclientId,
    'clientSecret' => OrangeSMSclientSecret
);
$osms = new Osms($config);
if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        ReglementsClients::delReglementsClients($id);
        array_push($success,"Réglement supprimer avec succès");
    endif;
endif;
if (isset($_POST) and !empty($_POST)):
    extract($_POST);

    if (empty($idClients)):
            array_push($warnings, "Veuillez séléctionner le client");
    endif;
    if (empty($montant)):
        array_push($warnings, "Veuillez saisir le montant");
    endif;
    if ($reste != ($solde - $montant)):
        array_push($warnings, "Veuillez verifier le reste");
    endif;
    if (count($warnings) == 0 and count($errors) == 0):
        $numeroRecu = "TOUMA" . strtoupper(uniqid() . rand(1, 1000));
        ReglementsClients::addReglement($montant,$_SESSION['gbmg']['login'],$idClients,$numeroRecu);
        array_push($success, "Payement enregistrer avec succès");
        //redirect_whit_target(LINK . 'recu_inscription/' . $numeroRecu);
        //endInfo Message
        $getInfo = Client::getClientById($idClients);
        foreach ($getInfo as $get):
            $response = $osms->getTokenFromConsumerKey();
            if (!empty($response['access_token'])) {
                $senderAddress = "tel:+224622347827";
                $receiverAddress = "tel:+224{$get->telephoneClient}";
                $montant = number_format($montant) . ' GNF';
                $reste = number_format($reste) . ' GNF';
                $message = NAME.", vous remercie de votre paiement de : {$montant} il vous reste à payer : {$reste} NB : aucun remboursement n'est possible. Merci de nous choisir et de nous faire confiance.";
                $senderName = NAME;
                if ($osms->sendSMS($senderAddress, $receiverAddress, $message, $senderName)) {
                    $succes = "SMS envoyer avec succès à  {$get->prenomClient} {$get->nomClient}";
                } else {
                    $warning = "SMS non envoyer à {$get->prenomClient} {$get->nomClient}";
                }
            } else {
                $erreur = "Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
            }
        endforeach;
        unset($montant,$idClients,$numeroRecu);
    endif;
endif;
$getClients = Client::getAllClients();

$getReglements = ReglementsClients::getAllReglements();