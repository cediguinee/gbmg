<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];

use models\Participants;
use models\PayementFormations;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);

        if(isset($id) AND !empty($id)):
            if (isset($_SESSION['gbmg']['role']) and $_SESSION['gbmg']['role'] == "Comptable"):
                array_push($warnings,"Seul l'administrateur a le droit de supprimer");
            else:
            PayementFormations::deletePayementFormations($id);
            array_push($success,"Payement du participant supprimer avec succès");
        endif;
    endif;

endif;

$getClients = Participants::getAllParticipantsPayements();