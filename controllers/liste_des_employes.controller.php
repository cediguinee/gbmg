<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];


use models\Personnels;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);

        if(isset($id) AND !empty($id)):
            if (isset($_SESSION['gbmg']['role']) and $_SESSION['gbmg']['role'] == "Comptable"):
                array_push($warnings,"Seul l'administrateur a le droit de supprimer");
            else:
            Personnels::deletePersonnels($id);
            array_push($success,"Employé supprimer avec succès");
        endif;
    endif;
endif;
$getAllEmployes = Personnels::getAllPersonnels();