<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];
use models\Categorie;
use models\SousCategorie;
use models\Participants;
use models\PayementFormations;
if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    $getPaye = PayementFormations::getPayementFormationsById($id);
    foreach ($getPaye as $item):
        $idPayements = $item->idPayements;
        $matricule   = $item->matriculeParticipants;
        $idFormation = $item->idFormations;
        $idModule    = $item->idModules;
        $montant     = $item->prixFormationNet;
        $avance      = $item->montantPayements;
        $reste       = $item->restePayements;
        $numeroRecu  = $item->numeroRecu;
        endforeach;
    endif;

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($matricule)):
        array_push($warnings,"Veuillez séléctionner le client");
    endif;
    if(empty($idModule)):
        array_push($warnings,"Veuillez séléctionner le module");
    endif;
    if(empty($idFormation)):
        array_push($warnings,"Veuillez séléctionner la formation");
    endif;
    if(empty($montant)):
        array_push($warnings,"Veuillez saisir le montant");
    endif;
    if(empty($avance)):
        array_push($warnings,"Veuillez saisir l'avance");
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        $reste = $montant - $avance;
        PayementFormations::updatePayementFormations($matricule,$idFormation,$idModule,$montant,$avance,$reste,$_SESSION['gbmg']['login'],$idPayements);
        redirect_whit_target(LINK.'recu_inscription/'.$numeroRecu);
        array_push($success,"Payement modifier avec succès");
        //unset($matricule,$idFormation,$idModule,$montant,$avance,$reste);
    endif;
endif;
$getParticipants = Participants::getAllParticipants();
$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();
