<?php
connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];


$succes = "";
$warning = "";
$erreur = "";


use models\Users;
use models\Personnels;
if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        $getInfo = Personnels::getAllPersonnelsById($id);
        foreach ($getInfo as $item):
            $id = $item->idPersonnels;
            $nom = $item->nomPersonnels;
            $prenom = $item->prenomPersonnels;
            $sexe = $item->sexePersonnels;
            $identifiant = $item->identifientPersonnels;
            $photoName = $item->photoPersonnels;
            $adresse = $item->adressePersonnels;
            $fonction = $item->fonctionPersonnels;
            $profession = $item->professionPersonnels;
            $email = $item->emailPersonnels;
            $tel = $item->telephonePersonnels;
            $papa = $item->perePersonnels;
            $mere = $item->merePersonnels;
            $parent = $item->personneContactPersonnels;
            $datenaissance = $item->dateNaissancePersonnels;
            $lieu = $item->lieuNaissancePersonnels;
            $pays = $item->paysPersonnels;
            $residence = $item->residencePersonnels;
            $ville = $item->professionPersonnels;
            $salaire = $item->salairePersonnels;
            $cin = $item->cinPersonnels;
            $matricule = $item->matriculePersonnels;
        endforeach;
    endif;
endif;

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($matricule)):
        array_push($warnings,"Veuillez saisir le matricule");
    endif;
    if(empty($sexe)):
        array_push($warnings,"Veuillez séléctionner la civilité");
    endif;
    if(empty($prenom)):
        array_push($warnings,"Veuillez saisir le prénom");
    endif;
    if(empty($nom)):
        array_push($warnings,"Veuillez saisir le nom");
    endif;
    if(empty($tel)):
        array_push($warnings,"Veuillez saisir le numéro de téléphone");
    endif;
    if(empty($adresse)):
        array_push($warnings,"Veuillez saisir l'adresse");
    endif;
    if(empty($datenaissance)):
        array_push($warnings,"Veuillez séléctionner la date de naissance");
    endif;
    if(empty($statut)):
        array_push($warnings,"Veuillez séléctionner le statut");
    endif;
    if(empty($lieu)):
        array_push($warnings,"Veuillez saisir le lieu");
    endif;
    if(empty($profession)):
        array_push($warnings,"Veuillez saisir la profession");
    endif;
    if(empty($fonction)):
        array_push($warnings,"Veuillez saisir la fonction");
    endif;
    if(empty($residence)):
        array_push($warnings,"Veuillez saisir la residence");
    endif;
    if(empty($ville)):
        array_push($warnings,"Veuillez saisir la ville");
    endif;
    if(empty($pays)):
        array_push($warnings,"Veuillez saisir le pays");
    endif;
    if(empty($cin)):
        array_push($warnings,"Veuillez saisir le N° CIN");
    endif;
    if(empty($cin)):
        array_push($warnings,"Veuillez saisir le N° CIN");
    endif;
    if(empty($identifiant)):
        array_push($warnings,"Veuillez saisir l'identifiant");
    endif;
    if(empty($mere)):
        array_push($warnings,"Veuillez saisir le prénom et nom de la maman");
    endif;
    if(empty($papa)):
        array_push($warnings,"Veuillez saisir le prénom du papa");
    endif;
    if(empty($salaire)):
        array_push($warnings,"Veuillez saisir le salaire de base");
    endif;
    if(empty($email)):
        array_push($warnings,"Veuillez saisir l'adresse email");
    endif;
    if(empty($password)):
        array_push($warnings,"Veuillez saisir le mot de passe");
    endif;
    if(empty($role)):
        array_push($warnings,"Veuillez séléctionnioner le rôle");
    endif;
    if(empty($parent)):
        array_push($warnings,"Veuillez saisir le numéro de la personne à contacter");
    endif;
    if(Personnels::verifyNumeroPersonnel($tel)!=0):
        array_push($warnings,"Ce numéro est déjà utiliser par un autre employé");
    endif;
    if(Personnels::verifyIdentifientPersonnel($identifiant)!=0):
        array_push($warnings,"Ce identifiant est déjà utiliser par un autre employé");
    endif;
    if(Personnels::verifyEmailPersonnel($email)!=0):
        array_push($warnings,"Cet adresse email est déjà utiliser par un autre employé");
    endif;
    if(numberGN($tel)==0){
        array_push($warnings,"Veuillez saisir un numéro guinéens");
    }
    if (isset($_FILES) AND !empty($_FILES)) :
        extract($_FILES);
        $photoName        = $_FILES['photo']['name'];
        $photoSize        = $_FILES['photo']['size'];
        $photoError       = $_FILES['photo']['error'];
        $photoTmp         = $_FILES['photo']['tmp_name'];
        $photoExe         = pathinfo($photoName, PATHINFO_EXTENSION);

        $allow                  = ['jpg', 'png', 'gif', 'bmp', 'jpeg','JPG', 'PNG', 'GIF', 'BMP', 'JPEG'];


        if (empty($photoName)) {
            array_push($errors, 'Veillez sélectionner le logo de votre entreprise.');
        }elseif($photoSize > 5266515){
            array_push($errors,'Ce fichier est trop lourd');
        }elseif(!in_array($photoExe,$allow)){
            array_push($errors,'Ce fichier n\'est une image');
        }
        $photoName = strtolower(uniqid().$photoName);
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        $photoName = uniqid().$photoName;
        move_uploaded_file($photoTmp,"assets/photos/avatars/".$photoName);
        Personnels::addPersonnel($nom,$prenom,$sexe,$identifiant,$photoName,$adresse,$fonction,$profession,$email,$tel,$papa,$mere,$parent,$datenaissance,$lieu,$pays,$residence,$ville,$salaire,$cin,$matricule);
        Users::addUSers($identifiant,$email,PASSWORD,$role,$statut);
        unset($nom,$prenom,$sexe,$identifiant,$photoName,$adresse,$fonction,$profession,$email,$tel,$papa,$mere,$parent,$datenaissance,$lieu,$pays,$residence,$ville,$salaire,$cin);
        array_push($success,"Employer recrut avec succès");
    endif;

endif;