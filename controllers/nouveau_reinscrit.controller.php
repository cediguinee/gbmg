<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];

$succes = "";
$warning = "";
$erreur = "";


use models\Categorie;
use models\SousCategorie;
use models\Participants;
use models\PayementFormations;


require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => 'BJzT1Fl1U6clOWeneQ67gfx9R5YURlmH',
    'clientSecret' => 'kb4AxMDYX880SAdn'
);
$osms = new Osms($config);

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($matricule)):
        array_push($warnings,"Veuillez séléctionner le client");
    endif;
    if(empty($idModule)):
        array_push($warnings,"Veuillez séléctionner le module");
    endif;
    if(empty($idFormation)):
        array_push($warnings,"Veuillez séléctionner la formation");
    endif;
    if(empty($montant)):
        array_push($warnings,"Veuillez saisir le montant");
    endif;
    if(empty($avance)):
        array_push($warnings,"Veuillez saisir l'avance");
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        $reste = $montant - $avance;
        $numeroRecu =NAME.strtoupper(uniqid().rand(1,1000));
        PayementFormations::addPayementFormations($matricule,$idFormation,$idModule,$montant,$avance,$reste,$_SESSION['gbmg']['login'],$numeroRecu);
        array_push($success,"Payement enregistrer avec succès");
        $getInfo = Participants::getParticipantsByMatricule($matricule);
        $response = $osms->getTokenFromConsumerKey();
        foreach ($getInfo as $get):
            if (!empty($response['access_token'])) {
                $senderAddress = "tel:+224622347827";
                $receiverAddress = "tel:+224{$get->contactParticipants}";
                $message = NAME." vous remercie pour votre inscription à la formation, vous avez payer : {$avance} et le reste est : {$reste} ";
                $senderName = NAME;
                if ($osms->sendSMS($senderAddress,$receiverAddress, $message, $senderName)) {
                    $succes="SMS envoyer avec succès à  {$get->prenomParticipants} {$get->nomParticipants}";
                } else {
                    $warning="SMS non envoyer à {$get->prenomParticipants} {$get->nomParticipants}";
                }
            } else {
                $erreur="Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
            }
        endforeach;
        unset($matricule,$idFormation,$idModule,$montant,$avance,$reste);
    endif;
endif;
$getParticipants = Participants::getAllParticipants();
$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();
