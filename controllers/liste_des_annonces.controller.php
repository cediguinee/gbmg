<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Annonces;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);

        if(isset($id) AND !empty($id)):
            if (isset($_SESSION['gbmg']['role']) and $_SESSION['gbmg']['role'] == "Comptable"):
                array_push($warnings,"Seul l'administrateur a le droit de supprimer");
            else:
            $getImageDelete = Annonces::getAnnonceById($id);
            foreach ($getImageDelete as $item):
                if($item->photoAnnonce!=""):
                    unlink("assets/photos/annonces/{$item->photoAnnonce}");
                endif;
            endforeach;
            Annonces::deleteAnnonce($id);
            array_push($success,"Annonce supprimer avec succès");
        endif;
    endif;

endif;

$getAllAnnonce = Annonces::getAllAnnonce();