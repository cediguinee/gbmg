<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];

use models\Depenses;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);

        if(isset($id) AND !empty($id)):
            if (isset($_SESSION['gbmg']['role']) and $_SESSION['gbmg']['role'] == "Comptable"):
                array_push($warnings,"Seul l'administrateur a le droit de supprimer");
            else:
            Depenses::delDepenses($id);
            array_push($success,"Dépense supprimer avec succès");
        endif;
    endif;
endif;

$getAllDepenses = Depenses::getAllDepenses();