<?php
connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];

$succes = "";
$warning = "";
$erreur = "";


use models\ReglementFournisseurs;
use models\Fournisseurs;



require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => OrangeSMSclientId,
    'clientSecret' => OrangeSMSclientSecret
);
$osms = new Osms($config);
if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        ReglementFournisseurs::delReglementsFournisseur($id);
        array_push($success,"Réglement supprimer avec succès");
    endif;
endif;
if (isset($_POST) and !empty($_POST)):
    extract($_POST);

    if (empty($idFournisseurs)):
        array_push($warnings, "Veuillez séléctionner le client");
    endif;
    if (empty($montant)):
        array_push($warnings, "Veuillez saisir le montant");
    endif;
    if ($reste != ($solde - $montant)):
        array_push($warnings, "Veuillez verifier le reste");
    endif;
    if (count($warnings) == 0 and count($errors) == 0):
        $numeroRecu = "TOUMA" . strtoupper(uniqid() . rand(1, 1000));
        ReglementFournisseurs::addReglement($montant,$_SESSION['gbmg']['login'],$idFournisseurs,$numeroRecu);
        array_push($success, "Payement enregistrer avec succès");
        //redirect_whit_target(LINK . 'recu_inscription/' . $numeroRecu);
        //endInfo Message
        $getInfo = Fournisseurs::getFournisseursById($idFournisseurs);
        foreach ($getInfo as $get):
            $response = $osms->getTokenFromConsumerKey();
            if (!empty($response['access_token'])) {
                $senderAddress = "tel:+224622347827";
                $receiverAddress = "tel:+224{$get->telephoneFournisseurs}";
                $montant = number_format($montant) . ' GNF';
                $reste = number_format($reste) . ' GNF';
                $message = NAME.", vous remercie pour la confiance, paiement de : {$montant} il nous reste à payer : {$reste}.";
                $senderName = NAME;
                if ($osms->sendSMS($senderAddress, $receiverAddress, $message, $senderName)) {
                    $succes = "SMS envoyer avec succès à  {$get->prenomFournisseurs} {$get->nomFournisseurs}";
                } else {
                    $warning = "SMS non envoyer à {$get->prenomFournisseurs} {$get->nomFournisseurs}";
                }
            } else {
                $erreur = "Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
            }
        endforeach;
        unset($montant,$idFournisseurs,$numeroRecu);
    endif;
endif;
$getFournisseurs = Fournisseurs::getAllFournisseurs();

$getReglements = ReglementFournisseurs::getAllReglements();
