<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];

$succes = "";
$warning = "";
$erreur = "";


use models\Categorie;
use models\SousCategorie;
use models\Participants;
use models\PayementFormations;


require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => OrangeSMSclientId,
    'clientSecret' => OrangeSMSclientSecret
);
$osms = new Osms($config);

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($matricule)):
        array_push($warnings,"Veuillez séléctionner le client");
        endif;
    if(empty($idModule)):
        array_push($warnings,"Veuillez séléctionner le module");
    endif;
    if(empty($idFormation)):
        array_push($warnings,"Veuillez séléctionner la formation");
    endif;
    if(empty($montant)):
        array_push($warnings,"Veuillez saisir le montant");
    endif;
    if($reste!=($solde-$montant)):
        array_push($warnings,"Veuillez verifier le reste");
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        $numeroRecu = "TOUMA".strtoupper(uniqid()).date('dmYhms');
        PayementFormations::addPayementFormations($matricule,$idFormation,$idModule,0,$montant,$reste,$_SESSION['gbmg']['login'],$numeroRecu);
        redirect_whit_target(LINK.'recu_inscription/'.$numeroRecu);
        array_push($success,"Payement enregistrer avec succès");
        $getInfo = Participants::getParticipantsByMatricule($matricule);

        //Info Message
        $getFormation = SousCategorie::getSousIdCat($idFormation);
        $getModule = Categorie::getCatgorieById($idModule);
        $formation = "";
        $module   = "";
        foreach ($getFormation as $value):
            $formation = $value->libelle;
        endforeach;
        foreach ($getModule as $value):
            $module = $value->nomCat;
        endforeach;
       //endInfo Message
        $response = $osms->getTokenFromConsumerKey();
        foreach ($getInfo as $get):
        if (!empty($response['access_token'])) {
            $senderAddress = "tel:+224622347827";
            $receiverAddress = "tel:+224{$get->contactParticipants}";
            $montant = number_format($montant).' GNF';
            $reste = number_format($reste).' GNF';
            $message = NAME.", vous remercie de votre paiement de : {$montant} pour votre formation de : {$formation} Session : {$module} , à payer : {$reste} NB : aucun remboursement n'est possible. Merci de nous choisir et de nous faire confiance.";
            $senderName = NAME;
            if ($osms->sendSMS($senderAddress,$receiverAddress, $message, $senderName)) {
                $succes="SMS envoyer avec succès à  {$get->prenomParticipants} {$get->nomParticipants}";
            } else {
                $warning="SMS non envoyer à {$get->prenomParticipants} {$get->nomParticipants}";
            }
        } else {
            $erreur="Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
        }
        endforeach;
        unset($idFormation,$idModule,$montant,$avance,$reste,$solde);
    endif;
endif;
$getParticipants = Participants::getAllParticipants();
$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();

