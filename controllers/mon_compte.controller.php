<?php

connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];
use models\Users;
use models\Personnels;
if(isset($_POST) AND !empty($_POST)){
    extract($_POST);
    if(isset($prenom) AND empty($prenom)){
        array_push($warnings,"Veuillez saisir le prénom");
    }
    if(isset($nom) AND empty($nom)){
        array_push($warnings,"Veuillez saisir le nom");
    }
    if(isset($email) AND empty($email)){
        array_push($warnings,"Veuillez saisir l'email");
    }
    if(isset($adresse) AND empty($adresse)){
        array_push($warnings,"Veuillez saisir l'adresse");
    }
    if(isset($identifient) AND empty($identifient)){
        array_push($warnings,"Veuillez saisir l'identifient");
    }
    if(isset($password) AND empty($password)){
        array_push($warnings,"Veuillez saisir le mot de passe");
    }
    if(isset($confirmation) AND empty($confirmation)){
        array_push($warnings,"Veuillez saisir la confirmation du mot de passe");
    }
    if(isset($password) AND  isset($confirmation) AND !empty($confirmation) AND !empty($password) AND $password!=$confirmation){
        array_push($warnings,"Vos mot de passe ne sont pas identique");
    }
    if (isset($_FILES) AND !empty($_FILES)) {
        extract($_FILES);
        $photoName        = $_FILES['photo']['name'];
        $photoSize        = $_FILES['photo']['size'];
        $photoError       = $_FILES['photo']['error'];
        $photoTmp         = $_FILES['photo']['tmp_name'];
        $photoExe         = pathinfo($photoName, PATHINFO_EXTENSION);

        $allow                  = ['jpg', 'png', 'gif', 'bmp', 'jpeg','JPG', 'PNG', 'GIF', 'BMP', 'JPEG'];


        if (empty($photoName)) {
            array_push($errors, 'Veillez sélectionner votre photo.');
        }elseif($photoSize > 5266515){
            array_push($errors,'Ce fichier est trop lourd');
        }elseif(!in_array($photoExe,$allow)){
            array_push($errors,'Ce fichier n\'est une image');
        }
        $photo = uniqid().$photoName;
    }
    if(count($warnings)==0 AND count($exists)==0 AND count($errors)==0){
        Personnels::updatePersonnelsInfo($nom,$prenom,$photo,$adresse,$email,$_SESSION['bms']['login']);
        Users::getUsers($confirmation,$_SESSION['bms']['login']);
        move_uploaded_file($photoTmp,"assets/images/avatars/". $photo);
        array_push($success,"Compte mise à jour avec succès");
        if(isset($_POST) AND !empty($_POST)){
            unset($_POST);
            unset($confirmation);
        }
    }
}
$getMyInfo = Users::getUsers($_SESSION['bms']['login']);
