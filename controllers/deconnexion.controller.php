<?php

session_destroy();

if(isset($_SESSION['gbmg']) AND !empty($_SESSION['gbmg'])){
    unset($_SESSION['gbmg']['id']);
    unset($_SESSION['gbmg']['login']);
    unset($_SESSION['gbmg']['role']);
    unset($_SESSION['gbmg']['etat']);
    session_destroy();
    header('location:'.LINK.'tableau_de_bord');
}else{
    header('location:'.LINK.'connexion');
}
