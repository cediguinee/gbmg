<?php
connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];

use models\Fournisseurs;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
        if(isset($id) AND !empty($id)):
            $getFournisseur = Fournisseurs::getFournisseursById($id);
            foreach ($getFournisseur as $item):
                    $prenom = $item->prenomFournisseurs;
                    $nom = $item->nomFournisseurs;
                    $adresse = $item->adresseFournisseurs;
                    $tel = $item->telephoneFournisseurs;
                    $idFournisseur = $item->idFournisseurs;
                endforeach;
            endif;
    endif;

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(isset($prenom) AND empty($prenom)):
        array_push($warnings,"Veuillez saisir prénom du fournisseur");
    endif;
    if(isset($nom) AND empty($nom)):
        array_push($warnings,"Veuillez saisir nom du fournisseur");
    endif;
    if(isset($tel) AND empty($tel)):
        array_push($warnings,"Veuillez saisir téléphone du fournisseur");
    endif;
    if(isset($adresse) AND empty($adresse)):
        array_push($warnings,"Veuillez saisir adresse du fournisseur");
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        Fournisseurs::updateFournisseurs($prenom,$nom,$adresse,$tel,$idFournisseur);
        //unset($prenom,$nom,$adresse,$tel);
        array_push($success,"Fournisseur modifier avec succès");
    endif;

endif;