<?php
connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];

use models\Fournisseurs;



if(isset($_GET) AND !empty($_GET)):
    extract($_GET);

        if(isset($id) AND !empty($id)):
            if (isset($_SESSION['gbmg']['role']) and $_SESSION['gbmg']['role'] == "Comptable"):
                array_push($warnings,"Seul l'administrateur a le droit de supprimer");
            else:
            Fournisseurs::deleteFournisseurs($id);
            array_push($success,"Fournisseur supprimer avec succès");
        endif;
    endif;

endif;





$getAllFournisseurs = Fournisseurs::getAllFournisseurs();