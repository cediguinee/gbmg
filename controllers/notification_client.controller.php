<?php
connected();
$success = "";
$warnings = "";
$erreurs = "";

$successMessage = [];

use models\Categorie;
use models\SousCategorie;
use models\Prospects;
use models\Participants;

require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => OrangeSMSclientId,
    'clientSecret' => OrangeSMSclientSecret
);
$osms = new Osms($config);


if(isset($_POST) AND !empty($_POST)):
    extract($_POST);

    if(isset($description) AND empty($description)):
       $warnings = "Veuillez saisir la description du message";
    endif;
    if(isset($idFormation) AND empty($idFormation)):
        $warnings = "Veuillez séléctionner la formation";
    endif;
    if(isset($idModule) AND empty($idModule)):
        $warnings = "Veuillez séléctionner le module";
    endif;
    if($warnings=="" AND $erreurs==""):
        $response = $osms->getTokenFromConsumerKey();
        //$getProspects = Participants::getAllClientsNotification($idFormation,$idModule);
        $getProspects = Participants::getAllParticipantsParCategorie($idFormation,$idModule);
        $nparticipant = count($getProspects);
        $i=1;
        if($nparticipant!=0):
            foreach ($getProspects as $prospect):
                if (!empty($response['access_token'])) {
                    $senderAddress = "tel:+224622347827";
                    $receiverAddress = "tel:+224{$prospect->contactParticipants}";
                    $message = $description;
                    $senderName = NAME;
                    if ($osms->sendSMS($senderAddress,$receiverAddress, $message, $senderName)) {
                        $sms = $i.'/'.$nparticipant;
                        //$success="SMS {$sms} envoyer avec succès à  {$prospect->prenomParticipants} {$prospect->nomParticipants}";
                        array_push($successMessage,"SMS {$sms} envoyer avec succès à  {$prospect->prenomParticipants} {$prospect->nomParticipants}");
                    } else {
                        $warnings="SMS non envoyer à {$prospect->prenomParticipants} {$prospect->nomParticipants}";
                    }
                } else {
                    $errors="Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
                }
            endforeach;
            $success = "Message envoyer avec succès au {$nparticipant} participant(s)";
        else:
            $warnings = "Pas de clients dans cette liste de participant !";
        endif;

    endif;

endif;















$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();
