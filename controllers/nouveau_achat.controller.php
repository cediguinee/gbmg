<?php
connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];

use models\Produits;
use models\Panier;
use models\Fournisseurs;
use models\Increments;
use models\Achats;
if(isset($_GET) AND !empty($_GET)){
    extract($_GET);
    if(!empty($id)):
        Panier::deletePanier($id);
        array_push($success,"Produit supprimer avec succès");
    endif;
}
if(isset($_POST) AND !empty($_POST) AND isset($_POST['panier']) AND !empty($_POST['panier']) AND  $_POST['panier']=="panier"):
    extract($_POST);
    if(isset($produit) AND empty($produit)):
        array_push($warnings,"Veuillez séléctionner le produit");
    endif;
    if(isset($quantite) AND empty($quantite)):
        array_push($warnings,"Veuillez saisir la quantité");
    endif;
    if(isset($prixachat) AND empty($prixachat)):
        array_push($warnings,"Veuillez saisir le prix d'achat");
    endif;
    if(isset($prixvente) AND empty($prixvente)):
        array_push($warnings,"Veuillez saisir le prix de vente");
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        $getProduits = Panier::getPanierById($produit);
        if(count($getProduits)==1){
            foreach ($getProduits as $produits){
                $qte = $produits->quantite+$quantite;
                $total = (($produits->quantite+$quantite)*$produits->prix);
                Panier::updatePanierQuantite($qte,$produits->prix,$total,$_SESSION['gbmg']['login'],$produit);
            }
        }
        if(count($getProduits)==0){
            Panier::addPanier($produit,$quantite,$prixvente,($quantite*$prixvente),$_SESSION['gbmg']['login']);
        };

        array_push($success,"Produit ajouter avec succès");
    endif;
endif;

if(isset($_POST) AND !empty($_POST) AND isset($_POST['fournisseur']) AND !empty($_POST['fournisseur']) AND  $_POST['fournisseur']=="fournisseur"):
    extract($_POST);
    if(isset($prenom) AND empty($prenom)):
        array_push($warnings,"Veuillez saisir prénom du fournisseur");
    endif;
    if(isset($nom) AND empty($nom)):
        array_push($warnings,"Veuillez saisir nom du fournisseur");
    endif;
    if(isset($tel) AND empty($tel)):
        array_push($warnings,"Veuillez saisir téléphone du fournisseur");
    endif;
    if(isset($adresse) AND empty($adresse)):
        array_push($warnings,"Veuillez saisir adresse du fournisseur");
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        Client::addClient($prenom,$nom,$adresse,$tel);
        unset($prenom,$nom,$adresse,$tel);
        array_push($success,"Client enregistré avec succès");
    endif;

endif;

if(isset($_POST) AND !empty($_POST) AND isset($_POST['valider']) AND $_POST['valider']=="valider"):
    extract($_POST);
    if(isset($reference) AND empty($reference)):
        array_push($warnings,"Veuillez saisir la référence");
    endif;
    if(isset($fournisseur) AND empty($fournisseur)):
        array_push($warnings,"Veuillez séléctionner le client");
    endif;
    if(isset($nom) AND empty($nom)):
        array_push($warnings,"Veuillez saisir le nom du client");
    endif;
    if(isset($tel) AND empty($tel)):
        array_push($warnings,"Veuillez saisir le numéro de téléphone");
    endif;
    if(isset($adresse) AND empty($adresse)):
        array_push($warnings,"Veuillez saisir l'adresse");
    endif;
    if(isset($montant) AND empty($montant)):
        array_push($warnings,"Veuillez saisir le montant");
    endif;
    if(isset($date) AND empty($date)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(isset($totalPaye) AND empty($totalPaye)):
        array_push($warnings,"Veuillez ajouter des produits avant de valider");
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        $getPanier = Panier::getAllPanier($_SESSION['gbmg']['login']);
        foreach ($getPanier as $item):
            $produit = $item->idProduits;
            $quantite = $item->quantite;
            $prixachat = $item->prix;
            $total = $item->total;
            $reste = $totalPaye-$montant;
            Achats::addAchat($reference,$produit,$quantite,$prixachat,$total,$date,$_SESSION['gbmg']['login'],$montant,$reste,$fournisseur,$totalPaye);
        endforeach;
        array_push($success,"Achat éfféctué avec succès");
        Increments::addIncrement();
        Panier::delete($_SESSION['gbmg']['login']);
        unset($prenom,$nom,$adresse,$tel);
    endif;
endif;
//-----------incrementation---------------------
$count = count(Increments::getAllIncrement())+1;
$user = substr($_SESSION['gbmg']['login'], 0,4);
$reference = strtoupper("ACH".$user.$count);
//--------------fin---------------------------
$getProduits = Produits::showAllProduits();
$getPanier = Panier::getAllPanier($_SESSION['gbmg']['login']);
$getFournisseur = Fournisseurs::getAllFournisseurs();