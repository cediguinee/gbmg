<?php

$exists = [];
$success = [];
$errors = [];
$warnings = [];

use models\Participants;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);

        if(isset($id) AND !empty($id)):
            if (isset($_SESSION['gbmg']['role']) and $_SESSION['gbmg']['role'] == "Comptable"):
                array_push($warnings,"Seul l'administrateur a le droit de supprimer");
            else:
            Participants::deleteParticipants($id);
            array_push($success,"Inscription et payement du participant supprimer avec succès");
        endif;
    endif;

endif;

$getClients = Participants::getAllParticipants();