<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Types;
if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($type)):
        array_push($warnings,"Veuillez saisir le type de dépense");
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):
        Types::addTypes($type);
        array_push($success,"Type de dépense : {$type} crée avec succès");
    endif;
endif;

if(isset($_GET) AND !empty($_GET)){
    extract($_GET);
    if(isset($id) AND !empty($id)){
        Types::deleteTypes($id);
        array_push($success,"Type supprimer avec succès");
    }
}

$getAllTypes = Types::getAllTypes();