<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Depenses;
use models\Types;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        $info = Depenses::getDepensesById($id);
        foreach ($info as $item):
            $nom = $item->personnesDepenses;
            $date= $item->dateDepenses;
            $montant = $item->montantDepenses;
            $description = $item->descriptionDepenses;
            $idDepenses = $item->idDepenses;
            $quantite = $item->quantiteDepenses;
            $total= $item->totalDepenses;
            $type = $item->idTypes;
        endforeach;
    endif;
endif;


if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($nom)):
        array_push($warnings,"Veuillez saisir le nom de la personne");
    endif;
    if(empty($date)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(empty($montant)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(empty($idDepenses)):
        array_push($warnings,"Veuillez séléctionner une dépense");
    endif;
    if(empty($description)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;

    if(count($warnings)==0 AND count($erreurs)==0):
        Depenses::editDepenses($nom,$date,$montant,$description,$_SESSION['gbmg']['login'],$quantite,$total,$type,$idDepenses);
        //unset($nom,$date,$montant,$description);
        array_push($success,"Dépense modifiée avec succès");
    endif;

endif;
$getAllTypes = Types::getAllTypes();