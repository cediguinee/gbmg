<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Partenaires;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        Partenaires::deletePartenaires($id);
        array_push($success,"Partenaire supprimer avec succès");
    endif;
endif;

if(isset($_POST) AND !empty($_POST)){
    extract($_POST);
   
    if(empty($nom)):
        array_push($warnings,"Veuillez saisir le nom");
    endif;
   
    if (isset($_FILES) AND !empty($_FILES)) :
        extract($_FILES);
        $photoName        = $_FILES['photo']['name'];
        $photoSize        = $_FILES['photo']['size'];
        $photoError       = $_FILES['photo']['error'];
        $photoTmp         = $_FILES['photo']['tmp_name'];
        $photoExe         = pathinfo($photoName, PATHINFO_EXTENSION);

        $allow                  = ['jpg', 'png', 'gif', 'bmp', 'jpeg','JPG', 'PNG', 'GIF', 'BMP', 'JPEG'];


        if (empty($photoName)) {
            array_push($errors, 'Veillez sélectionner le logo de votre entreprise.');
        }elseif($photoSize > 5266515){
            array_push($errors,'Ce fichier est trop lourd');
        }elseif(!in_array($photoExe,$allow)){
            array_push($errors,'Ce fichier n\'est une image');
        }
        $photoName = strtolower(uniqid().$photoName);
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):
        $photoName = uniqid().$photoName;
        move_uploaded_file($photoTmp,"assets/photos/partenaires/".$photoName);
        Partenaires::addPartenaires($photoName,$nom,$sites);
    endif;
}

$getAll = Partenaires::getAllPartenaires();
