<?php
connected();
use models\Participants;
use models\Categorie;
use models\SousCategorie;
$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(isset($idModule) AND !empty($idModule) AND isset($idFormation) AND !empty($idFormation)):
        $getAllClients = Participants::getAllParticipantsParCategorie($idFormation,$idModule);
    else:
        $getAllClients  = Participants::getAllParticipants();
    endif;
else:
    $getAllClients  = Participants::getAllParticipants();
endif;

