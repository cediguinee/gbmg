<?php
require_once 'api/vendor/autoload.php';

use Dompdf\Dompdf;
use models\Entreprises;
use models\PayementFormations;
$entreprise = Entreprises::getEntreprise();
foreach ($entreprise as $e) {
    $nom = $e->nom;
    $nif = $e->nif;
    $rccm = $e->rccm;
    $compte = $e->compte;
    $numcompte = $e->numcompte;
    $tel = $e->tel;
    $email = $e->email;
    $adresse = $e->adresse;
}
if(isset($_GET) AND !empty($_GET)){
    extract($_GET);
    if(isset($id) AND !empty($id)){
        $getInfo = PayementFormations::getPayementFormationsByReference($id);
       
        foreach ($getInfo as $info){
            $reference = $info->numeroRecu;
            $nomcomplet = $info->civiliteParticipants.' : '.$info->prenomParticipants.' '.$info->nomParticipants;
            $formation = $info->libelle;
            $session = $info->nomCat;
            $payer = $info->montantPayements;
            $reste = $info->restePayements;
            $datePayements = $info->datePayements;
            $matricule = $info->matriculeParticipants;
        }
    }
}
$fmt = new NumberFormatter('fr', NumberFormatter::SPELLOUT);
setlocale(LC_TIME, 'fr');
$dompdf = new Dompdf();
// debug($getInfo);
// die();
ob_start(); ?>
<style type="text/css">
    .tableau {
        background-image: url("assets/admin/img/logogbmg.png");
        background-size: 90%;
        background-repeat: no-repeat;
        opacity: 0.1;
        padding-bottom: 0px;
        background-position-x: 20px;
    }

    .recu {
        border: 1px solid black;
        border: 1px solid black;
        border-radius: 20px;
        padding-right: 0px;
    }
</style>

<div class="recu">
    <table border="" class="tableau">
        <tr style="margin-bottom: 1px solid black;">
            <td style="width: 130px;padding-right:15px"> <img height="70" width="130" src="assets/admin/img/logogbmg.png" alt="" srcset=""> </td>
            <td style="border-bottom: 1px solid black;text-align:center;padding-bottom:10px;width:340px;padding-right:15px">
                <?= $nom; ?> <br>
               Tél: <?= $tel; ?> <br>
                <?= $adresse; ?><br>
                RCCM: <?= $rccm; ?><br>
                Email: <?= $email; ?>
            </td>
            <td style="text-align: left;width:180px;padding-left:5px">
                Date: <?=date_format(date_create($datePayements),'d/m/Y')?><br> <br>
                <div>Montant: <?=number_format($payer).' GNF'?> </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left;">
                RECU N°: <?=$reference?> <br><br>
                N° MATRICULE : <?=$matricule?> <br>
                Réçu de <?=$nomcomplet?> Formation :  <?=$formation?> / Session : <?=$session?> <br> <br>
                La somme de: <?=ucfirst($fmt->format($payer))?> Francs Guinéens <br><br>
                Reste à payer : <?=number_format($reste).' GNF'?>  <br><br>
                Le client
            </td>
            <td style="vertical-align: bottom;">La comptabilité</td>
        </tr>
        <tr><td style="height: 50px;border:1px solid white"></td></tr>
        <tr>
            <td style="text-align: center;" colspan="3">RCCM : <?=$rccm ?> NIF: <?=$nif ?> | N° Compte: <?=$numcompte?> BIG </td>
        </tr>
    </table>
</div>
<br><br>


<div class="recu">
    <table border="" class="tableau">
        <tr style="margin-bottom: 1px solid black;">
            <td style="width: 130px;padding-right:15px"> <img height="70" width="130" src="assets/admin/img/logogbmg.png" alt="" srcset=""> </td>
            <td style="border-bottom: 1px solid black;text-align:center;padding-bottom:10px;width:340px;padding-right:15px">
                <?= $nom; ?> <br>
               Tél: <?= $tel; ?> <br>
                <?= $adresse; ?><br>
                RCCM: <?= $rccm; ?><br>
                Email: <?= $email; ?>
            </td>
            <td style="text-align: left;width:180px;padding-left:5px">
                Date: <?=date_format(date_create($datePayements),'d/m/Y')?><br> <br>
                <div>Montant: <?=number_format($payer).' GNF'?> </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left;">
                RECU N°: <?=$reference?> <br><br>
                N° MATRICULE : <?=$matricule?> <br>
                Réçu de <?=$nomcomplet?> Formation :  <?=$formation?> / Session : <?=$session?> <br> <br>
                La somme de: <?=ucfirst($fmt->format($payer))?> Francs Guinéens <br><br>
                Reste à payer : <?=number_format($reste).' GNF'?>  <br><br>
                Le client
            </td>
            <td style="vertical-align: bottom;">La comptabilité</td>
        </tr>
        <tr><td style="height: 50px;border:1px solid white"></td></tr>
        <tr>
            <td style="text-align: center;" colspan="3">RCCM : <?=$rccm ?> NIF: <?=$nif ?> | N° Compte: <?=$numcompte?> BIG</td>
        </tr>
    </table>
</div>

<?php
$content = ob_get_contents();
ob_end_clean();
$dompdf->loadHtml($content);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$dompdf->stream("sample.pdf", array("Attachment" => 0));
