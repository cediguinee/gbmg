<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];
use models\Categorie;
use models\SousCategorie;
use models\Participants;
use models\PayementFormations;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        $getInfo = Participants::getParticipantsById($id);
        foreach ($getInfo as $item):
            $idParticipants = $item->idParticipants;
            $matricule      = $item->matriculeParticipants;
            $sexe           =$item->civiliteParticipants;
            $prenom         =$item->prenomParticipants;
            $nom            =$item->nomParticipants;
            $tel            =$item->contactParticipants;
            $adresse        =$item->adresseParticipants;
            $datenaissance  =$item->dateNaissanceParticipants;
            $profession     =$item->professionParticipants;
            $residence      =$item->residenceParticipants;
            $ville          =$item->villeParticipants;
            $pays           =$item->paysParticipants;
            $langue         =$item->langueParticipants;
            $personneressource =$item->parQuiParticipants;
            $partenaire     =$item->numeroPartenaireParticipants;
        endforeach;
        endif;
    endif;

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($matricule)):
        array_push($warnings,"Veuillez générer le matricule");
    endif;
    if(empty($sexe)):
        array_push($warnings,"Veuillez séléctionner la civilité");
    endif;
    if(empty($prenom)):
        array_push($warnings,"Veuillez saisir le prénom");
    endif;
    if(empty($nom)):
        array_push($warnings,"Veuillez saisir le nom");
    endif;
    if(empty($tel)):
        array_push($warnings,"Veuillez saisir le numéro");
    endif;
    /*
    if(empty($adresse)):
        array_push($warnings,"Veuillez saisir l'adresse");
    endif;
    if(empty($datenaissance)):
        array_push($warnings,"Veuillez séléctionner la date de naissance");
    endif;
    if(empty($profession)):
        array_push($warnings,"Veuillez saisir la profession");
    endif;
    if(empty($residence)):
        array_push($warnings,"Veuillez saisir la résidence");
    endif;
    if(empty($ville)):
        array_push($warnings,"Veuillez saisir la ville");
    endif;
    if(empty($pays)):
        array_push($warnings,"Veuillez saisir le pays");
    endif;
    if(empty($partenaire)):
        array_push($warnings,"Veuillez saisir le numéro du canal");
    endif;
    */
    if(empty($personneressource)):
        array_push($warnings,"Veuillez séléctionner la personne ressource");
    endif;
    if(empty($langue)):
        array_push($warnings,"Veuillez saisir la langue du participant");
    endif;
    if(numberGN($tel)==0):
        array_push($warnings,"Veuillez saisir un numéro guinéens");
    endif;

    if(count($warnings)==0 AND count($errors)==0):
        Participants::updateParticipants($matricule,$sexe,$prenom,$nom,$tel,$adresse,$datenaissance,$profession,$residence,$ville,$pays,$langue,$personneressource,$partenaire,$idParticipants);
        array_push($success,"Participant modifier avec succès");
        //unset($matricule,$sexe,$prenom,$nom,$tel,$adresse,$datenaissance,$profession,$residence,$ville,$pays,$langue,$personneressource,$partenaire);

    endif;

endif;

$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();
