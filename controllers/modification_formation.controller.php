<?php

connected();
$success = [];
$warnings = [];
$erreurs = [];

use models\SousCategorie;
if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        $getInfo = SousCategorie::getSousIdCat($id);
        foreach ($getInfo as $item):
            $id = $item->idSousCat;
            $module = $item->libelle;
        endforeach;
    endif;
    endif;
if (isset($_POST) and !empty($_POST)):
    extract($_POST);
    if (empty($module)):
        array_push($warnings, "Veuillez saisir le nom de la formation");
    endif;
    if (count($warnings) == 0 and count($erreurs) == 0):
        SousCategorie::updateSousCat($module,$id);
        array_push($success, "Formation {$module} modifier avec succès");
    endif;
endif;