<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];

use models\Payes;
use models\Personnels;


if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(isset($matricule) AND empty($matricule)):
       array_push($warnings,"Veuillez séléctionner l'employé");
    endif;
    if(isset($montant) AND empty($montant)):
        array_push($warnings,"Veuillez saisir le montant");
    endif;
    if(isset($date) AND empty($date)):
        array_push($warnings,"Veuillez séléctionner la date");
    else:
        $mois = date_format(date_create($date),'m');
        $annee = date_format(date_create($date),'Y');
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        Payes::addPayes($matricule,$montant,$_SESSION['gbmg']['login'],$date,$mois,$annee);
        array_push($success,"Employé payé avec succès");
    endif;
endif;
$getEmploye = Personnels::getAllPersonnels();