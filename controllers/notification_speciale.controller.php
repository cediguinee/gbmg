<?php
connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];


$succes = "";
$warning = "";
$erreur = "";



require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => OrangeSMSclientId,
    'clientSecret' => OrangeSMSclientSecret
);
$osms = new Osms($config);

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($description)):
        array_push($warnings,"Veuillez saisir la description");
    endif;
    if(empty($numero)):
        array_push($warnings,"Veuillez saisir le numéro");
    endif;
    if(numberGN($numero)==0):
        array_push($warnings,"Veuillez saisir un numéro guinéens");
    endif;

    if(count($warnings)==0 AND count($errors)==0):
        $response = $osms->getTokenFromConsumerKey();
        if (!empty($response['access_token'])) {
            $senderAddress = "tel:+224622347827";
            $receiverAddress = "tel:+224{$numero}";
            $message = $description;
            $senderName = "GBMG";
            if ($osms->sendSMS($senderAddress,$receiverAddress, $message, $senderName)) {
                $success="SMS envoyer avec succès au {$numero} ";
            } else {
                $warnings="SMS non envoyer au {$numero}  ";
            }
        } else {
            $errors="Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
        }
    endif;

endif;
