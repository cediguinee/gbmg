<?php
connected();
$exists = [];
$success = [];
$errors = [];
$warnings = [];

use models\Client;
if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(isset($prenom) AND empty($prenom)):
        array_push($warnings,"Veuillez saisir prénom du client");
    endif;
    if(isset($nom) AND empty($nom)):
        array_push($warnings,"Veuillez saisir nom du client");
    endif;
    if(isset($tel) AND empty($tel)):
        array_push($warnings,"Veuillez saisir téléphone du client");
    endif;
    if(isset($adresse) AND empty($adresse)):
        array_push($warnings,"Veuillez saisir adresse du client");
    endif;
    if(count($warnings)==0 AND count($errors)==0):
        Client::addClient($prenom,$nom,$adresse,$tel);
        unset($prenom,$nom,$adresse,$tel);
        array_push($success,"Client enregistré avec succès");
    endif;

endif;
