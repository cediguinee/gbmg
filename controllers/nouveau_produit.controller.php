<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];

use models\Produits;


if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($libelle)):
        array_push($warnings,"Veuillez saisir le nom du produit");
    endif;
    if(empty($prixvente)):
        array_push($warnings,"Veuillez saisir le prix de vente");
    endif;
    if(empty($prixachat)):
        array_push($warnings,"Veuillez saisir le prix d'achat");
    endif;
    if($prixachat > $prixvente):
        array_push($erreurs,"Le prix de vente {$prixvente} doit être supperieur au prix d'achat {$prixachat} ");
    endif;
    if(empty($date)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):
        Produits::addProduits($libelle,$prixachat,$prixvente,$date);
        array_push($success,"Produit ajouter avec succès");
        unset($libelle,$prixachat,$prixvente,$date);
    endif;

endif;