<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Categorie;
if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
        if(empty($module)):
            array_push($warnings,"Veuillez saisir le nom du module");
        endif;
        if(count($warnings)==0 AND count($erreurs)==0):
            Categorie::addCategorie($module);
            array_push($success,"Module {$module} crée avec succès");
        endif;
endif;

