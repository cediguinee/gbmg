<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];

use models\Produits;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        $produits = Produits::showProduitsById($id);
        foreach ($produits as $produit):
            $idProduits = $produit->idProduits;
            $libelle    = $produit->libelleProduits;
            $prixachat  = $produit->prixAchatProduits;
            $prixvente  = $produit->prixDeVenteProduits;
            $date       = $produit->dateProduits;
        endforeach;
    endif;
endif;

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($libelle)):
        array_push($warnings,"Veuillez saisir le nom du produit");
    endif;
    if(empty($idProduits)):
        array_push($warnings,"Veuillez séléctionner un produit");
    endif;
    if(empty($prixvente)):
        array_push($warnings,"Veuillez saisir le prix de vente");
    endif;
    if(empty($prixachat)):
        array_push($warnings,"Veuillez saisir le prix d'achat");
    endif;
    if($prixachat > $prixvente):
        array_push($erreurs,"Le prix de vente {$prixvente} doit être supperieur au prix d'achat {$prixachat} ");
    endif;
    if(empty($date)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):
        Produits::updateProduits($libelle,$prixachat,$prixvente,$date,$idProduits);
        array_push($success,"Produit ajouter avec succès");
        //unset($libelle,$prixachat,$prixvente,$date);
    endif;

endif;
