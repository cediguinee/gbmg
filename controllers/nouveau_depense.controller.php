<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Depenses;
use models\Types;
if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($nom)):
       array_push($warnings,"Veuillez saisir le nom de la personne");
    endif;
    if(empty($date)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(empty($montant)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(empty($quantite)):
        array_push($warnings,"Veuillez saissir la quantité");
    endif;
    if(empty($type)):
        array_push($warnings,"Veuillez séléctionner le type");
    endif;
    if(empty($description)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):
        Depenses::addDepenses($nom,$date,$montant,$description,$_SESSION['gbmg']['login'],$quantite,$total,$type);
        unset($nom,$date,$montant,$description,$quantite,$total,$type);
        array_push($success,"Dépense enregistrée avec succès");
    endif;

endif;

$getAllTypes = Types::getAllTypes();