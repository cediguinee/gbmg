<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];


$succes = "";
$warning = "";
$erreur = "";

use models\Personnels;

require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => OrangeSMSclientId,
    'clientSecret' => OrangeSMSclientSecret
);
$osms = new Osms($config);

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($description)):
        array_push($warnings,"Veuillez saisir la description");
    endif;
   $infoParticipant = Personnels::getAllPersonnels();
    if(count($warnings)==0 AND count($errors)==0):
        $response = $osms->getTokenFromConsumerKey();
        foreach ($infoParticipant as $item):
            if (!empty($response['access_token'])) {
                $senderAddress = "tel:+224622347827";
                $receiverAddress = "tel:+224{$item->telephonePersonnels}";
                $message = $description;
                $senderName = NAME;
                if ($osms->sendSMS($senderAddress,$receiverAddress, $message, $senderName)) {
                    $success="SMS envoyer avec succès";
                } else {
                    $warnings="SMS non envoyer";
                }
            } else {
                $errors="Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
            }
        endforeach;
    endif;

endif;

