<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Personnels;
use models\Login;

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(isset($motdepasse1) AND empty($motdepasse1) AND isset($motdepasse2) AND empty($motdepasse2) AND $motdepasse1!=$motdepasse2):
        array_push($warnings,"Les deux mot de passe ne sont pas identique");
    else:
        $motdepasse2 = hashpassword($motdepasse2);
        Login::updatePassword($motdepasse2,$login);
    array_push($success,"Mot de passe mise à jour avec succès");
    endif;
endif;
if(isset( $_SESSION['gbmg']['login']) AND !empty( $_SESSION['gbmg']['login'])):
    $getInnformation= Personnels::getUsersByLogin( $_SESSION['gbmg']['login']);
    foreach ($getInnformation as $personne):
        $prenom = $personne->prenomPersonnels;
        $nom = $personne->nomPersonnels;
        $adresse = $personne->adressePersonnels;
        $telephone = $personne->telephonePersonnels;
        $email = $personne->emailPersonnels;
        $login = $personne->identifientUsers;
        $pwd = $personne->passwordUsers;
        $fonction = $personne->fonctionPersonnels;
        $role = $personne->roleUsers;
        $photo = $personne->photoPersonnels;
    endforeach;
endif;