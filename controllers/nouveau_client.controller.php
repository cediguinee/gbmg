<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];

$successM = "";
$warningsM = "";
$erreursM = "";

connected();

use models\Categorie;
use models\SousCategorie;
use models\Participants;
use models\PayementFormations;



require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => OrangeSMSclientId,
    'clientSecret' => OrangeSMSclientSecret
);
$osms = new Osms($config);

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($matricule)):
        array_push($warnings,"Veuillez générer le matricule");
    endif;
    if(empty($sexe)):
        array_push($warnings,"Veuillez séléctionner la civilité");
    endif;
    if(empty($prenom)):
        array_push($warnings,"Veuillez saisir le prénom");
    endif;
    if(empty($nom)):
        array_push($warnings,"Veuillez saisir le nom");
    endif;
    if(empty($tel)):
        array_push($warnings,"Veuillez saisir le numéro");
    endif;
    /*if(empty($adresse)):
       array_push($warnings,"Veuillez saisir l'adresse");
   endif;

    if(empty($datenaissance)):
       array_push($warnings,"Veuillez séléctionner la date de naissance");
   endif;
   if(empty($profession)):
       array_push($warnings,"Veuillez saisir la profession");
   endif;
    if(empty($residence)):
       array_push($warnings,"Veuillez saisir la résidence");
   endif;
   if(empty($ville)):
       array_push($warnings,"Veuillez saisir la ville");
   endif;
   if(empty($pays)):
       array_push($warnings,"Veuillez saisir le pays");
   endif;
   */
    if(empty($avance)):
        array_push($warnings,"Veuillez saisir le montant d'avance");
    endif;
    if(empty($montant)):
        array_push($warnings,"Veuillez saisir le montant payé");
    endif;
    if(empty($idFormation)):
        array_push($warnings,"Veuillez séléctionner la formation");
    endif;
    if(empty($idModule)):
        array_push($warnings,"Veuillez séléctionner le module");
    endif;
    /*
    if(empty($partenaire)):
        array_push($warnings,"Veuillez saisir le numéro du canal");
    endif;
    if(empty($personneressource)):
        array_push($warnings,"Veuillez séléctionner la personne ressource");
    endif;
    if(empty($langue)):
        array_push($warnings,"Veuillez saisir la langue du participant");
    endif;
    */
    if(numberGN($tel)==0):
        array_push($warnings,"Veuillez saisir un numéro guinéens");
    endif;
    //Info Message
    $getFormation = SousCategorie::getSousIdCat($idFormation);
    $getModule = Categorie::getCatgorieById($idModule);
    $formation = "";
    $module   = "";
    foreach ($getFormation as $value):
        $formation = $value->libelle;
    endforeach;
    foreach ($getModule as $value):
        $module = $value->nomCat;
    endforeach;
    //endInfo Message
    if(count($warnings)==0 AND count($errors)==0):
        Participants::addParticipants($matricule,$sexe,$prenom,$nom,$tel,$adresse,$datenaissance,$profession,$residence,$ville,$pays,$langue,$personneressource,$partenaire);
        $reste = $montant-$avance;
        $numeroRecu = NAME.strtoupper(uniqid()).date('dmYhms');
        PayementFormations::addPayementFormations($matricule,$idFormation,$idModule,$montant,$avance,$reste,$_SESSION['gbmg']['login'],$numeroRecu);
        redirect_whit_target(LINK.'recu_inscription/'.$numeroRecu);
        $response = $osms->getTokenFromConsumerKey();
        if (!empty($response['access_token'])) {
            $montant = number_format($montant).' GNF';
            $reste = number_format($reste).' GNF';
            $avance = number_format($avance).' GNF';
            $senderAddress = "tel:+224622347827";
            $receiverAddress = "tel:+224{$tel}";
            $message = "Bonjour, vous êtes inscrit(e)s avec succès à la formation de {$formation}, session {$module}  . Montant à  payer {$montant} , Montant payé {$avance} reste à payer {$reste}. NB : aucun remboursement n'est possible. Nous vous remercions de nous choisir et de nous faire confiance pour votre bien être personnel et professionnel";
            $senderName = NAME;
            if ($osms->sendSMS($senderAddress,$receiverAddress, $message, $senderName)) {
                $successM="SMS envoyer avec succès à  {$prenom} {$nom}";
            } else {
                $warningsM="SMS non envoyer à {$prenom} {$nom}";
            }
        } else {
            $errorsM="Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
        }
        array_push($success,"Participant inscrit avec succès");
        unset($matricule,$sexe,$prenom,$nom,$tel,$adresse,$datenaissance,$profession,$residence,$ville,$pays,$langue,$personneressource,$partenaire);

    endif;

    endif;
$matricule = NAME.strtoupper(uniqid()).date('dmYhms');
$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();