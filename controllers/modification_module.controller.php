<?php

connected();
$success = [];
$warnings = [];
$erreurs = [];

use models\Categorie;
if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        $getInfo = Categorie::getCatgorieById($id);
        foreach ($getInfo as $item):
            $id = $item->idCat;
            $module = $item->nomCat;
            endforeach;
        endif;
    endif;
if (isset($_POST) and !empty($_POST)):
    extract($_POST);
    if (empty($module)):
        array_push($warnings, "Veuillez saisir le nom du module");
    endif;
    if (count($warnings) == 0 and count($erreurs) == 0):
        Categorie::updateCategorie($module,$id);
        array_push($success, "Module {$module} modifier avec succès");
    endif;
endif;