<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];

use models\Sliders;
if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    if(isset($id) AND !empty($id)):
        $data = Sliders::getSliderById($id);
            foreach ($data as $item):
                $idSliders = $item->id;
                $libelle = $item->nom;
                $description = $item->description;
                $photo2 = $item->photo;
            endforeach;
        endif;
    endif;
if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($libelle)):
        array_push($warnings,"Veuillez saisir le libelle");
    endif;
    if(empty($description)):
        array_push($warnings,"Veuillez saisir la description");
    endif;
    if (isset($_FILES) AND !empty($_FILES)) :
        extract($_FILES);
        if($_FILES['photo']['name']!=""):
            extract($_FILES);
            $photoName        = $_FILES['photo']['name'];
            $photoSize        = $_FILES['photo']['size'];
            $photoError       = $_FILES['photo']['error'];
            $photoTmp         = $_FILES['photo']['tmp_name'];
            $photoExe         = pathinfo($photoName, PATHINFO_EXTENSION);

            $allow                  = ['jpg', 'png', 'gif', 'bmp', 'jpeg','JPG', 'PNG', 'GIF', 'BMP', 'JPEG'];


            if (empty($photoName)) {
                array_push($errors, 'Veillez sélectionner le logo de votre entreprise.');
            }elseif($photoSize > 5266515){
                array_push($errors,'Ce fichier est trop lourd');
            }elseif(!in_array($photoExe,$allow)){
                array_push($errors,'Ce fichier n\'est une image');
            }
            $photo = strtolower(uniqid().$photoName);
            move_uploaded_file($photoTmp,"assets/photos/sliders/".$photo);
        else:
            $photo = $photo2;
        endif;
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):

        Sliders::editSlider($libelle,$description,$photo,1,$idSliders);
        array_push($success,"Slider enregistré avec succès");
    endif;
endif;

