<?php
connected();
$success = "";
$warnings = "";
$erreurs = "";

use models\Categorie;
use models\SousCategorie;
use models\Prospects;

require_once ("api/smsOrange/Osms.php");
use Osms\Osms;

$config = array(
    'clientId' => OrangeSMSclientId,
    'clientSecret' => OrangeSMSclientSecret
);
$osms = new Osms($config);


if(isset($_POST) AND !empty($_POST)):
    extract($_POST);

    if(isset($description) AND empty($description)):
        $warnings = "Veuillez saisir la description du message";
    endif;
    if(isset($idFormation) AND empty($idFormation)):
        $warnings = "Veuillez séléctionner la formation";
    endif;
    if(isset($idModule) AND empty($idModule)):
        $warnings = "Veuillez séléctionner le module";
    endif;
    if($warnings=="" AND $erreurs==""):
        $response = $osms->getTokenFromConsumerKey();
        $getProspects = Prospects::getAllProspectsNotification($idModule,$idFormation);
        foreach ($getProspects as $prospect):
            if (!empty($response['access_token'])) {
                $senderAddress = "tel:+224622347827";
                $receiverAddress = "tel:+224{$prospect->contactPospects}";
                $message = $description;
                $senderName = NAME;
                if ($osms->sendSMS($senderAddress,$receiverAddress, $message, $senderName)) {
                    $success="SMS envoyer avec succès à  {$prospect->prenomPospects} {$prospect->nomPospects}";
                } else {
                    $warnings="SMS non envoyer à {$prospect->prenomtPospects} {$prospect->nomPospects}";
                }
            } else {
                $errors="Erreur de connexion à l'API Orange, veuillez verifier votre SMS";
            }
        endforeach;
        $success = "Message envoyer avec succès";
    endif;

endif;















$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();

