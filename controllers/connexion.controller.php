<?php
use models\Login;
use models\Connexions;
$success =[];
$warnings = [];
$erreurs = [];
if(isset($_POST) AND !empty($_POST) AND $_POST['Seconnecter']=="Seconnecter" ){
    extract($_POST);
    if(isset($email) AND empty($email)){
        array_push($warnings,"Veuillez saisir l'adresse email");
    }
    if(isset($password) AND empty($password)){
        array_push($warnings,"Veuillez saisir le mot de passe");
    }
    if(count($warnings)==0 AND count($erreurs)==0){
        $password = hashpassword($password);
        $getUsersInfo = Login::veriflogin($email,$password);
        if(count($getUsersInfo)!='1'){
            unset($password);
            array_push($warnings,"Votre identifient ou mot de passe sont incorrect");
        }else{
            foreach ($getUsersInfo as $get){
                if(isset($get->etatUsers) AND !empty($getUsersInfo)){
                    if($get->etatUsers=="Active"){
                        $_SESSION['gbmg']['id'] =$get->idUsers;
                        $_SESSION['gbmg']['idperso'] =$get->idPersonnels;
                        $_SESSION['gbmg']['login'] =$get->identifientUsers;
                        $_SESSION['gbmg']['email'] =$get->emailUsers;
                        $_SESSION['gbmg']['role']=$get->roleUsers;
                        $_SESSION['gbmg']['etat']=$get->etatUsers;
                        Connexions::addConnexion($get->identifientUsers);
                        header('location:'.LINK.'tableau_de_bord');
                    }else{
                        array_push($warnings,"Votre compte est désactivé");
                    }
                }
            }
        }
    }
}