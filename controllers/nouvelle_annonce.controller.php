<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];

use models\Categorie;
use models\SousCategorie;
use models\Annonces;

if(isset($_POST) AND !empty($_POST)):
    extract($_POST);

    if(isset($libelle) AND empty($libelle)):
        array_push($warnings,"Veuillez saisir le libelle");
    endif;
     if(isset($description) AND empty($description)):
         array_push($warnings,"Veuillez saisir la description");
     endif;
    if(isset($date) AND empty($date)):
        array_push($warnings,"Veuillez séléctionner la date");
    endif;
    if(isset($idFormation) AND empty($idFormation)):
        array_push($warnings,"Veuillez séléctionner la formation du prospect");
    endif;
    if (isset($_FILES) AND !empty($_FILES)) :
        extract($_FILES);
        $photoName        = $_FILES['photo']['name'];
        $photoSize        = $_FILES['photo']['size'];
        $photoError       = $_FILES['photo']['error'];
        $photoTmp         = $_FILES['photo']['tmp_name'];
        $photoExe         = pathinfo($photoName, PATHINFO_EXTENSION);

        $allow                  = ['jpg', 'png', 'gif', 'bmp', 'jpeg','JPG', 'PNG', 'GIF', 'BMP', 'JPEG'];


        if (empty($photoName)) {
            array_push($errors, 'Veillez sélectionner le logo de votre entreprise.');
        }elseif($photoSize > 5266515){
            array_push($errors,'Ce fichier est trop lourd');
        }elseif(!in_array($photoExe,$allow)){
            array_push($errors,'Ce fichier n\'est une image');
        }
        $photoName = strtolower(uniqid().$photoName);
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):
        Annonces::addAnnonce($libelle,$date,$photoName,$idFormation,$description,slugify($libelle));
        move_uploaded_file($photoTmp,"assets/photos/annonces/".$photoName);
        array_push($success,"Annonce Publiée avec succès");
    endif;

endif;
$getCategories = Categorie::getAllCategorie();
$getSousCategores = SousCategorie::getAllSousCategorie();



