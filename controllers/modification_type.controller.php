<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Types;

if(isset($_GET) AND !empty($_GET)){
    extract($_GET);
    if(isset($id) AND !empty($id)){
        $getType = Types::getTypesById($id);
        foreach ($getType as $item){
            $type = $item->libelleTypes;
            $idType = $item->idTypes;
        }
    }
}
if(isset($_POST) AND !empty($_POST)):
    extract($_POST);
    if(empty($type)):
        array_push($warnings,"Veuillez saisir le type de dépense");
    endif;
    if(count($warnings)==0 AND count($erreurs)==0):
        Types::updateTypes($type,$idType);
        array_push($success,"Type de dépense : {$type} crée avec succès");
    endif;
endif;



