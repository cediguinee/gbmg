<?php
$exists = [];
$success = [];
$errors = [];
$warnings = [];


use models\Personnels;
use models\Users;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);
    $getUsers = Users::getUsersConnect($id);
    foreach ($getUsers as $getUser):
         if($getUser->etatUsers=="Active"):
             Users::updateUSersStatut("Desactive",$id);
             array_push($success,"Utilisateur désactivé avec succès");
         else:
             Users::updateUSersStatut("Active",$id);
             array_push($success,"Utilisateur activé avec succès");
         endif;
    endforeach;
endif;
$getAllEmployes = Personnels::getAllPersonnels();

