<?php
connected();
$success =[];
$warnings = [];
$erreurs = [];
use models\Categorie;

if(isset($_GET) AND !empty($_GET)):
    extract($_GET);

        if(isset($id) AND !empty($id)):
            if (isset($_SESSION['gbmg']['role']) and $_SESSION['gbmg']['role'] == "Comptable"):
                array_push($warnings,"Seul l'administrateur a le droit de supprimer");
            else:
            Categorie::deleteCategorie($id);
            array_push($success,"Module supprimer avec succès");
        endif;
    endif;

endif;

$getAllModule = Categorie::getAllCategorie();