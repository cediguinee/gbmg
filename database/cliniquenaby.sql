-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 06, 2020 at 11:53 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miseegn`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `idClients` int(11) NOT NULL,
  `nomClients` varchar(50) NOT NULL,
  `prenomClients` varchar(50) NOT NULL,
  `adresseClients` varchar(50) DEFAULT NULL,
  `telephoneClients` varchar(50) NOT NULL,
  `descriptionClients` text,
  `usersConnected` varchar(50) NOT NULL,
  `dateAjoutClients` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `telephonePrive` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`idClients`, `nomClients`, `prenomClients`, `adresseClients`, `telephoneClients`, `descriptionClients`, `usersConnected`, `dateAjoutClients`, `telephonePrive`) VALUES
(11, 'Bah', 'Ousmane mokoba', 'Pont miniere', '625982526', 'Argent toujours manquant', 'ousmane', '2020-06-24 12:19:39', '624002479'),
(12, 'Barry', 'Ibrahima Donghol madina', 'Madina mosquée', '620948582', 'Meilleur', 'ousmane', '2020-06-24 12:24:25', '622646292'),
(13, 'Diallo', 'Mamadou Saliou', 'Matam gare routière', '626456964', 'Bon', 'ousmane', '2020-06-24 12:27:39', '628095490'),
(14, 'Diallo', 'Mamadou', 'Aviation', '625455583', 'Sérieux et travailleur', 'ousmane', '2020-06-24 20:36:13', '621924195'),
(15, 'Barry', 'Mamadou Yero', 'Sonfonia gare', '622460098', 'Rendement faible', 'ousmane', '2020-06-24 20:39:48', '627028060'),
(16, 'Diallo', 'Mamadou Japon', 'Madina boussoura', '621987329', 'Assez bien', 'ousmane', '2020-06-24 20:43:51', '628868981'),
(17, 'Diallo', 'Alpha Madiou', 'Belle vue', '622460161', 'Espoir et sérieux', 'ousmane', '2020-06-24 20:48:00', '628524554'),
(18, 'Barry', 'Tanou', 'Madina', '620628615', 'Honnête', 'ousmane', '2020-06-24 20:51:00', '622661677'),
(19, 'Haïdara', 'Saliou', 'Sig madina', '629337622', 'Travailleur et sérieux', 'ousmane', '2020-06-24 20:56:40', '622306598'),
(20, 'Diallo', 'Souleymane Amadou', 'Camayenne CMIS', '624317894', 'Assez bien', 'ousmane', '2020-06-24 21:00:43', '622521214'),
(21, 'Balade', 'Aicha', 'Kaporo rails', '620948455', 'Brave femme', 'ousmane', '2020-06-24 21:03:00', '622209169'),
(22, 'Diallo', 'Ibrahim', 'Taouyah', '621627680', 'Capital faible', 'ousmane', '2020-06-24 21:06:19', '622202603'),
(23, 'Barry', 'Boubacar', 'Dabola', '621627739', 'Bon mais trop bavard', 'ousmane', '2020-06-24 21:09:38', '624550254'),
(24, 'Camara', 'Amadou', 'Dabola', '622460381', 'Bon mais distrait', 'ousmane', '2020-06-24 21:12:40', '622241862'),
(25, 'Barry', 'Alpha Mamadou', 'Enco5', '625244527', 'Faible', 'ousmane', '2020-06-24 21:14:44', '628453750'),
(26, 'Diallo', 'Thierno amadou', 'Kaporo', '621314239', 'Douteux', 'ousmane', '2020-06-24 21:19:06', '620842412'),
(27, 'Savane', 'Mamadou', 'Madina marche balaya', '621626683', 'Courageux', 'ousmane', '2020-06-24 21:25:11', '622667619'),
(28, 'Diaby', 'Youssouf falyko', 'Siguiri', '620948362', 'Faible et bavard', 'ousmane', '2020-06-24 21:28:47', '628838080'),
(29, 'Toure', 'Aboubacar', 'Sonfonia gare', '620948313', 'Nouveau', 'ousmane', '2020-06-24 22:57:23', '620967197'),
(30, 'Diallo', 'Fatoumata', 'Camayenne', '624002811', 'Pas mal', 'ousmane', '2020-06-24 22:59:53', '628886970'),
(31, 'Cisse', 'Youssouf', 'Kouroussa', '621313472', 'Bon', 'ousmane', '2020-06-24 23:02:14', '622336569'),
(32, 'Diallo', 'Alpha oumar bagou', 'Coronthie', '625518676', 'Bien', 'ousmane', '2020-06-24 23:06:52', '622846485'),
(33, 'Diallo', 'Lamarana', 'Madina', '622623515', 'Bon', 'ousmane', '2020-06-24 23:08:41', '664414238'),
(34, 'Diallo', 'Mamadou', 'Dinguiraye ( mataganya)', '626983740', 'Passable', 'ousmane', '2020-06-24 23:11:15', '624523352'),
(35, 'Diallo', 'Abdourahamane', 'Madina', '621873678', 'Paraisseux', 'ousmane', '2020-06-24 23:14:51', '625584444'),
(36, 'Barry', 'Mamadou bailo', 'Madina', '622455671', 'Courageux', 'ousmane', '2020-06-24 23:17:58', '622455671'),
(37, 'Sow', 'Abdoulaye', 'Bembeto', '622455788', 'Honnête', 'ousmane', '2020-06-24 23:19:52', '620605161'),
(38, 'Belgo', 'Baucoum malien', 'Camayenne', '621314090', 'Très fiable', 'ousmane', '2020-06-24 23:22:16', '623856473'),
(39, 'Barry', 'Saikou oumar', 'Enco 5', '622459347', 'Courageux', 'ousmane', '2020-06-24 23:24:12', '622459347'),
(40, 'Barry', 'Amadou', 'Dinguiraye (dialakro)', '621038331', 'Bon', 'ousmane', '2020-06-24 23:26:51', '622174690');

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE `configurations` (
  `idConfigurations` int(11) NOT NULL,
  `nomEntreprisesConfigurations` varchar(50) NOT NULL,
  `abreviationConfigurations` varchar(50) NOT NULL,
  `addresseConfigurations` varchar(50) NOT NULL,
  `telephonePDVConfigurations` varchar(50) NOT NULL,
  `telephoneConfigurations` varchar(50) NOT NULL,
  `descriptionConfigurations` text NOT NULL,
  `logoConfigurations` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`idConfigurations`, `nomEntreprisesConfigurations`, `abreviationConfigurations`, `addresseConfigurations`, `telephonePDVConfigurations`, `telephoneConfigurations`, `descriptionConfigurations`, `logoConfigurations`) VALUES
(1, 'Brother &amp; Frères Multiservices', 'BMS', 'Madina', '622459755', '622623515', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'BLAN.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `fonctions`
--

CREATE TABLE `fonctions` (
  `idFonctions` int(11) NOT NULL,
  `nameFonctions` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fonctions`
--

INSERT INTO `fonctions` (`idFonctions`, `nameFonctions`) VALUES
(14, 'Proprietaire'),
(29, 'Gerant');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `idLogins` int(11) NOT NULL,
  `users` varchar(50) NOT NULL,
  `datetimes` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`idLogins`, `users`, `datetimes`) VALUES
(1, 'habaansou', '2020-05-03 21:58:23'),
(2, 'habaansou', '2020-05-03 22:08:16'),
(3, 'habaansou', '2020-06-17 21:17:15'),
(4, 'habaansou', '2020-06-17 21:18:24'),
(5, 'habaansou', '2020-06-17 22:34:44'),
(6, 'habaansou', '2020-06-18 03:28:54'),
(7, 'habaansou', '2020-06-18 06:44:20'),
(8, 'habaansou', '2020-06-20 14:15:14'),
(9, 'habaansou', '2020-06-21 10:51:58'),
(10, 'habaansou', '2020-06-21 12:55:44'),
(11, 'habaansou', '2020-06-21 13:22:17'),
(12, 'habaansou', '2020-06-21 13:39:52'),
(13, 'habaansou', '2020-06-21 13:46:02'),
(14, 'habaansou', '2020-06-21 17:53:56'),
(15, 'habaansou', '2020-06-22 17:23:59'),
(16, 'ousmane', '2020-06-23 22:17:19'),
(17, 'ousmane', '2020-06-23 22:23:15'),
(18, 'ousmane', '2020-06-24 11:33:33'),
(19, 'ousmane', '2020-06-24 11:47:33'),
(20, 'ousmane', '2020-06-24 11:59:42'),
(21, 'ousmane', '2020-06-24 13:17:31'),
(22, 'ousmane', '2020-06-24 13:59:14'),
(23, 'ousmane', '2020-06-24 14:51:40'),
(24, 'ousmane', '2020-06-24 20:31:47'),
(25, 'ousmane', '2020-06-24 21:20:48'),
(26, 'ousmane', '2020-06-24 22:33:36'),
(27, 'ousmane', '2020-06-25 01:02:17'),
(28, 'ousmane', '2020-06-25 07:32:19'),
(29, 'ousmane', '2020-06-25 08:04:47'),
(30, 'ousmane', '2020-06-25 10:59:37'),
(31, 'ousmane', '2020-06-25 11:11:15'),
(32, 'ousmane', '2020-06-25 11:14:21'),
(33, 'ousmane', '2020-06-25 11:14:38'),
(34, 'ousmane', '2020-06-25 11:44:09'),
(35, 'ousmane', '2020-06-25 11:45:37'),
(36, 'ousmane', '2020-06-25 12:14:10'),
(37, 'ousmane', '2020-06-25 12:14:19'),
(38, 'ousmane', '2020-06-25 12:18:02'),
(39, 'ousmane', '2020-06-25 12:22:32'),
(40, 'ousmane', '2020-06-25 12:33:02'),
(41, 'ousmane', '2020-06-25 13:58:46'),
(42, 'ousmane', '2020-06-25 14:50:06'),
(43, 'ousmane', '2020-06-25 15:30:51'),
(44, 'ousmane', '2020-06-25 15:59:47'),
(45, 'ousmane', '2020-06-25 16:00:01'),
(46, 'ousmane', '2020-06-25 16:28:53'),
(47, 'ousmane', '2020-06-25 16:32:17'),
(48, 'ousmane', '2020-06-25 19:45:10'),
(49, 'ousmane', '2020-06-25 20:24:10'),
(50, 'ousmane', '2020-06-26 08:04:28'),
(51, 'ousmane', '2020-06-26 09:44:30'),
(52, 'ousmane', '2020-06-26 10:30:59'),
(53, 'ousmane', '2020-06-26 11:10:03'),
(54, 'ousmane', '2020-06-26 11:10:06'),
(55, 'ousmane', '2020-06-26 12:11:11'),
(56, 'ousmane', '2020-06-26 12:58:23'),
(57, 'ousmane', '2020-06-26 15:13:56'),
(58, 'ousmane', '2020-06-26 15:25:18'),
(59, 'ousmane', '2020-06-26 15:33:58'),
(60, 'ousmane', '2020-06-26 15:38:58'),
(61, 'ousmane', '2020-06-26 15:42:34'),
(62, 'ousmane', '2020-06-26 16:03:56'),
(63, 'ousmane', '2020-06-26 16:27:12'),
(64, 'ousmane', '2020-06-26 16:27:12'),
(65, 'ousmane', '2020-06-27 10:31:34'),
(66, 'ousmane', '2020-06-27 10:35:09'),
(67, 'ousmane', '2020-06-27 12:46:36'),
(68, 'ousmane', '2020-06-27 12:54:28'),
(69, 'ousmane', '2020-06-27 14:54:21'),
(70, 'ousmane', '2020-06-27 22:41:31'),
(71, 'ousmane', '2020-06-27 22:44:48'),
(72, 'ousmane', '2020-06-27 22:44:56'),
(73, 'ousmane', '2020-06-28 20:17:19'),
(74, 'ousmane', '2020-06-29 03:14:22'),
(75, 'ousmane', '2020-06-29 11:32:48'),
(76, 'ousmane', '2020-06-29 11:41:59'),
(77, 'ousmane', '2020-06-29 12:35:36'),
(78, 'ousmane', '2020-06-29 12:45:41'),
(79, 'ousmane', '2020-06-29 14:06:34'),
(80, 'ousmane', '2020-06-29 20:35:18'),
(81, 'ousmane', '2020-06-29 22:18:11'),
(82, 'ousmane', '2020-06-30 12:06:32'),
(83, 'ousmane', '2020-06-30 15:04:46'),
(84, 'ousmane', '2020-07-01 11:13:51'),
(85, 'ousmane', '2020-07-01 18:21:36'),
(86, 'ousmane', '2020-07-02 10:34:36'),
(87, 'ousmane', '2020-07-02 12:18:21'),
(88, 'ousmane', '2020-07-02 13:32:16'),
(89, 'ousmane', '2020-07-02 15:02:44'),
(90, 'ousmane', '2020-07-02 18:40:06'),
(91, 'ousmane', '2020-07-02 18:46:19'),
(92, 'ousmane', '2020-07-02 19:22:12'),
(93, 'ousmane', '2020-07-02 19:51:37'),
(94, 'ousmane', '2020-07-02 19:58:46'),
(95, 'ousmane', '2020-07-02 20:15:05'),
(96, 'ousmane', '2020-07-02 21:27:54'),
(97, 'ousmane', '2020-07-03 10:20:46'),
(98, 'ousmane', '2020-07-03 11:32:28'),
(99, 'ousmane', '2020-07-03 15:51:59'),
(100, 'ousmane', '2020-07-03 18:35:33'),
(101, 'ousmane', '2020-07-03 20:10:03'),
(102, 'ousmane', '2020-07-03 23:12:17'),
(103, 'ousmane', '2020-07-04 23:29:39'),
(104, 'habaansou', '2020-08-06 11:25:32'),
(105, 'habaansou', '2020-08-06 11:30:50'),
(106, 'habaansou', '2020-08-06 11:52:24');

-- --------------------------------------------------------

--
-- Table structure for table `personnels`
--

CREATE TABLE `personnels` (
  `idPersonnels` int(11) NOT NULL,
  `nomPersonnels` varchar(50) NOT NULL,
  `prenomPersonnels` varchar(50) NOT NULL,
  `sexePersonnels` varchar(50) NOT NULL,
  `identifientPersonnels` varchar(50) NOT NULL,
  `photoPersonnels` varchar(255) NOT NULL,
  `adressePersonnels` varchar(50) NOT NULL,
  `idFonctions` int(11) NOT NULL,
  `idProfessions` int(11) NOT NULL,
  `emailPersonnels` varchar(50) NOT NULL,
  `telephonePersonnels` varchar(50) NOT NULL,
  `dateDembausePersonnels` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descriptionPersonnels` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personnels`
--

INSERT INTO `personnels` (`idPersonnels`, `nomPersonnels`, `prenomPersonnels`, `sexePersonnels`, `identifientPersonnels`, `photoPersonnels`, `adressePersonnels`, `idFonctions`, `idProfessions`, `emailPersonnels`, `telephonePersonnels`, `dateDembausePersonnels`, `descriptionPersonnels`) VALUES
(40, 'HABA', 'David Ansoumane', 'Homme', 'habaansou', '5ef135bf1ec3fhabaansou.jpg', 'T6, Sonfonia', 14, 2, 'habaansou@gmail.com', '622347827', '2020-06-22 07:14:10', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(43, 'Diallo', 'Mamadou lamarana', 'Homme', 'ousmane', '5ef3d7c563e93image.jpg', 'hamdallaye', 14, 2, 'diallolamarana999@gmail.com', '657347827', '2020-06-22 21:52:50', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

-- --------------------------------------------------------

--
-- Table structure for table `professions`
--

CREATE TABLE `professions` (
  `idProfessions` int(11) NOT NULL,
  `nameProfessions` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `professions`
--

INSERT INTO `professions` (`idProfessions`, `nameProfessions`) VALUES
(2, 'Marchant'),
(3, 'Commercant');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL,
  `identifientUsers` varchar(50) NOT NULL,
  `emailUsers` varchar(50) NOT NULL,
  `passwordUsers` varchar(255) NOT NULL,
  `roleUsers` varchar(50) NOT NULL,
  `etatUsers` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUsers`, `identifientUsers`, `emailUsers`, `passwordUsers`, `roleUsers`, `etatUsers`) VALUES
(18, 'habaansou', 'habaansou@gmail.com', 'ce03b44e744432d8a65f37eb2dfea5a2f2bdf74d79d8052af1e8caa2a5526c6f', 'Administrateur', 'Active'),
(21, 'ousmane', 'diallolamarana999@gmail.com', 'ce03b44e744432d8a65f37eb2dfea5a2f2bdf74d79d8052af1e8caa2a5526c6f', 'Administrateur', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`idClients`),
  ADD KEY `telephoneClients` (`telephoneClients`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`idConfigurations`);

--
-- Indexes for table `fonctions`
--
ALTER TABLE `fonctions`
  ADD PRIMARY KEY (`idFonctions`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`idLogins`);

--
-- Indexes for table `personnels`
--
ALTER TABLE `personnels`
  ADD PRIMARY KEY (`idPersonnels`),
  ADD UNIQUE KEY `identifientPersonnels` (`identifientPersonnels`),
  ADD KEY `idProfessions` (`idProfessions`),
  ADD KEY `idFonctions` (`idFonctions`),
  ADD KEY `emailPersonnels` (`emailPersonnels`);

--
-- Indexes for table `professions`
--
ALTER TABLE `professions`
  ADD PRIMARY KEY (`idProfessions`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUsers`),
  ADD KEY `emailUsers` (`emailUsers`),
  ADD KEY `identifientUsers` (`identifientUsers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `idClients` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `configurations`
--
ALTER TABLE `configurations`
  MODIFY `idConfigurations` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fonctions`
--
ALTER TABLE `fonctions`
  MODIFY `idFonctions` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `idLogins` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `personnels`
--
ALTER TABLE `personnels`
  MODIFY `idPersonnels` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `professions`
--
ALTER TABLE `professions`
  MODIFY `idProfessions` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `personnels`
--
ALTER TABLE `personnels`
  ADD CONSTRAINT `personnels_idfonction_fk` FOREIGN KEY (`idFonctions`) REFERENCES `fonctions` (`idFonctions`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `personnels_idprofessions_fk` FOREIGN KEY (`idProfessions`) REFERENCES `professions` (`idProfessions`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `personnels_emailPersonnels_fk` FOREIGN KEY (`emailUsers`) REFERENCES `personnels` (`emailPersonnels`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `personnels_identifient_fk` FOREIGN KEY (`identifientUsers`) REFERENCES `personnels` (`identifientPersonnels`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
